**CONTENTS**

[[_TOC_]]


# **<div align="center">Linux</div>**
## **<div align="center">Distros</div>**
- Debian, Ubuntu, Mint etc.
- RHEL, Fedora and CentOS.
- Arch
- Alpine
- Kali


**Directory architecture:**<br>
/etc/ - CONFIG<br>
/bin/ - BINARY<br>
/sbin/ - CRITICAL BINARY<br>
/tmp/ - TEMPORARY<br>
/home/ - HOME<br>
/root/ - ROOT’S HOME<br>
/lib/ - LIBRARIES<br>
/lib64/ - LIBRARIES x86-64<br>
/boot/ - LINUX<br>
/dev/ - DEVICES<br>
/media/ - AUTOMOUNT (Ubuntu)<br>
/mnt/ - AUTOMOUNT<br>
/opt/ - OPTIONAL SOFTWARE<br>
/proc/ - PROCESS INFORMATION<br>
/usr/ - USER PROGRAM FILES AND LIBRARIES<br>
/var/ - VARIOUS<br>
*/var/log/ - Almost all system logs<br>


## **<div align="center">Troubleshooting</div>**
- Never rely on users opinion as a definitive source of truth;
- Start from basic stuff (free space, firewall, payments);
- Backup if possible before anything;
- Divide and conquer.


- **Identify the problem**: Make sure you understand the problem. What are the symptoms. What the exact ERROR MESSAGE (CODE)? What is the impact (in business terms)?
    - **Is there a problem at all?**: Is it plugged in? Is it on? Did you restart it? Gathering information from log files and error messages. Questioning users.Identifying symptoms;
    - **Determining recent changes**: When did the problem start (when did the user first become aware of the problem)? Are there any known changes that coincide with the problem starting? 
    - **Is the problem intermittent or persistent?**: Is it possible to reproduce? Duplicating the problem or approach multiple problems one at a time. Narrowing the scope of the problem. Establish a theory of probable cause.
- **Propose a solution and test it**: You shouldn't make changes until you are reasonably sure you have a solution you’re ready to implement.
    - Questioning the obvious to identify the cause of the problem;
    - Considering multiple approaches, including top-to-bottom or bottom-to-top for layered technologies (such as networks)
- **Establish a Plan of Action and Implement the Solution**: 
    - Backup if possible before anything;
    - Start from least invasive steps;
    - Make sure that things will not get worse. Prepare for the worst;
- **Verify full system functionality**: If applicable, implement preventive measures. Document findings, actions, outcomes and lessons learned.


**Commands to troubleshoot:**
`pwd`<br>
`uname -a`<br>
`hostnamectl`<br>
`ip -a`<br>
`df -ah`<br>
`du -sh <directory_name>`<br>
`service <service_name> status` / `systemctl status <service_name>`
`service --status-all`<br>
`journalctl _UID=<service_ID>`<br>
`ss -tunapl | egrep "(ESTAB|LISTEN)`<br>
`htop`<br>
`ps aux | grep <process_name>`<br>
`ps aux | grep '***'`<br>
`lsof -p <port_number>`<br>
`crontab -l`<br>
`ls /mnt`<br>
`/etc/fstab`<br>
`ls -alh <file_name>`<br>
`man`<br>
*Google it..*

**Clean disk in Ubuntu:**<br>
https://www.geeksforgeeks.org/methods-to-clean-temporary-files-from-ubuntu/