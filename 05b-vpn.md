**CONTENTS**

[[_TOC_]]


# <div align="center"> Wireguard</div>
[Angristan/WireGuard-Install](https://github.com/angristan/wireguard-install)<br>
`wget https://git.io/wireguard -O wireguard-install.sh && bash wireguard-install.sh`<br>
`chmod +x wireguard-install.sh`<br>
`./wireguard-install.sh`<br>
*To make it work in Linux Mint, just comment/delete `function checkOS()` function in the script.<br>


[ComplexOrganizations/WireGuard-Manager](https://github.com/complexorganizations/wireguard-manager)<br>
`curl https://raw.githubusercontent.com/complexorganizations/wireguard-manager/main/wireguard-manager.sh --create-dirs -o /usr/local/bin/wireguard-manager.sh`<br>
`chmod +x /usr/local/bin/wireguard-manager.sh`<br>
`wireguard-manager.s`<br>


[FileZone: WireGuard with WebUI (+https)](https://github.com/firezone/firezone)<br>
[Server install](https://www.firezone.dev/docs/deploy/docker)<br>


# <div align="center"> OpenVPN</div>




https://privacycanada.net/5-eyes-explained/