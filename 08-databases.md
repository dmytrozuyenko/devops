**CONTENTS**

[[_TOC_]]


# **<div align="center">Databases</div>**
**Database** is an organized collection of data, a collection of information that is organized so that it can easily be accessed, managed, and updated.<br>


**Database management system (DBMS)** is a special software application that interacts with the user, other applications, and the database itself to capture and analyze data.<br>


**DBMS purpose**:<br>
- To store data properly<br>
- To provide simultaneous access to the data for many users<br>
- To delimit the access to the data for different users<br>
- To prevent data from loss<br>


**SQL:**<br>
MySQL, MariaDB, Oracle, PostgreSQL, and Microsoft SQL Server.<br>


**NoSQL:**<br>
Redis, MongoDB.


**Differences:**<br>
- SQL databases are relational, NoSQL are non-relational.
- SQL databases use structured query language and have a predefined schema. NoSQL databases have dynamic schemas for unstructured data.
- SQL databases are vertically scalable, NoSQL databases are horizontally scalable.
- SQL databases are table based, while NoSQL databases are document, key-value, graph or wide-column stores.
- SQL databases are better for multi-row transactions, NoSQL are better for unstructured data like documents or JSON.


**SQL Database Engine** - software module that a database management system uses to create, read, update and delete data from a database.<br>


**Storage engines in MySQL:**<br>
- Transactional;
- Non-transactional.


**MySQL Database Engines:**<br>
**Current storage engine recommendations based on workload:**<br>
- Read-heavy workloads: Aria
- General purpose: InnoDB
- ACID: InnoDB
- Write-heavy workloads: MyRocks
- Compression: MyRocks
- Sharded: Spider
- Analytical workloads: MariaDB ColumnStore


**Default engines:**<br>
- MySQL <v5.5 - MyISAM
- MySQL >=v5.5 - InnoDB
- MariaDB <v5.5 - MyISAM
- MariaDB >v5.5 - Aria
- MariaDB >v10.2 - InnoDB


**InnoDB** is the most widely used storage engine with transaction support. It is an ACID compliant storage engine. It supports row-level lockingm crash recovery and multi-version concurrency control.<br>
It is the only engine which provides foreign key referential integrity constraint.<br>


**MyISAM** is the original storage engine. It is a fast storage engine. It doesn't support transactions. MyISAM provides table-level locking. It is used mostly in Web and data warehousing.<br>


**Memory** storage engine creates tables in memory. It is the fastest engine. It provides table-level locking. It doesn't support transactions.<br>


**There are three types of relationships between tables:**<br>
- `one-to-one` - In a one-to-one relationship, a row in the first table can have not more than one matching row in the second table. A one-to-one relationship is created if both of the related columns are primary keys or have unique constraints;
- `one to many`, `many to one` - In a one-to-many relationship, a row in the first table can have many matching rows in the second table, but a row in the second table can have only one matching row in the first table. Make a one-to-many relationship if only one of the related columns is a primary key or has a unique constraint;
- `many to many` - In a many-to-many relationship, a row in the first table can have many matching rows in the second one. 
You create such a relationship by defining a third table, called a junction table, whose primary key consists of the foreign keys from both first and second tables.

**Keys:**<br>
- **Primary key** constraint uniquely identifies each record in a database table. Primary keys must contain UNIQUE values. A primary key column cannot contain NULL values. Most tables should have a primary key, and each table can have only ONE primary key.<br>
- **Foreign key** is a column or combination of columns that is used to establish and enforce a link between the data in two tables to control the data that can be stored in the foreign key table.

## **<div align="center">Normalization</div>**
The process of organizing the columns and tables of a relational database to minimize data redundancy.
**Data redundancy** - the existence of data that is an additional copy of the actual data or a small part of them. Data redundancy leads to the modification anomalies.


**Modification anomalies:**<br>
- Insert anomalies.
- Update anomalies.
- Delete anomalies.


**Goals of the normalization:**<br>
- Free the DB of modification anomalies.
- Minimize redesign when extending the DB structure.
- Make the DB more informative.
- Make the DB suitable for querying.


**Normal forms:**<br>
- First normal form (1NF).
- Second normal form (2NF).
- Third normal form (3NF).
- Other normal forms.


**First normal form (1NF):**<br>
- No duplicate rows – **row uniqueness.**
- No repeating groups of columns – **column uniqueness.**
- Every row-and-column intersection contains exactly one value from the applicable domain – **data atomicity.**

1: Single Valued Attributes
2: Attribute Domain should not change
3: Unique name for Attributes/Columns
4: Order doesn't matters


**Row uniqueness:**<br>
- Duplicates of rows should be eliminated;
- In case when we really need the same values in different rows, we can use an additional field and nominate it as a primary key.


**Column uniqueness:**<br>
- Repeating groups of columns should be eliminated.


**Data atomicity:**<br>
- There is a single value in each field of each row;
- This value is from selected domain;
- NULL values could be also allowed.


**UNF->1NF:**<br>
- Eliminate duplicative columns from the same table.
- Create separate tables for each group of related data and identify each row with a unique column or set of columns (the primary key).


**Functional dependency:**<br>
- Describes relationship between columns in a table;
- If A and B are columns of the table R, B is functionally dependent on A (denoted A→B), if each value of A in R is associated with exactly one value of B in R.
`ID→LASTNAME`<br>
`ID→FIRSTNAME`<br>
~~`LASTNAME FIRSTNAME`~~<br>


**Second normal form (2NF):**<br>
- 1NF;
- No partial functional dependencies.


**1NF->2NF:**<br>
- Identify functional dependencies in the table;
- If partial dependencies exist on the primary key remove them by placing them in a new table along with copy of their determinant.


**Third normal form (3NF):**<br>
- 2NF;
- No transitive dependencies, i.e. no non-key field depends upon another. 


**2NF->3NF:**<br>
- Identify functional dependencies in the table;
- If transitive dependencies exist on the primary key remove them by placing them in a new table along with copy of their determinant.


**Boyce-Codd Normal Form (BCNF):**
- 3NF;
- For any dependency A → B, A should be a super key.


**Fourth normal form (4NF):**<br>
- BCNF;
- And, the table should not have any Multi-valued Dependency.


**Fifth normal form (5NF):**<br>
- 4NF;
- Won't have lossless decomposition into smaller tables.


**Denormalization** - denormalized databases fair well under heavy read-load and when the application is read intensive. But because the data is duplicated, the updates and inserts become complex and costly.<br>


## **<div align="center">Syntax</div>**
**Data Definition Language (DDL)** is used for creating and modifying database objects such as tables, indices, and users.<br>
`CREATE`<br>
`ALTER`<br>
`DROP`<br>


**Data Manipulation Language (DML)** is used for adding (inserting), deleting, and modifying (updating) data.<br>
`INSERT`<br>
`SELECT`<br>
`UPDATE`<br>
`DELETE`<br>


**Data types:**<br>
`INT` – integer numbers;<br>
`NUMERIC(m,n)` – fixed point real numbers;<br>
`FLOAT` – floating point real numbers;<br>
`CHAR(n)` – character string constant of length n characters;<br>
`VARCHAR(n)` - character string of variable length, maximum length n characters;<br>
`DATETIME`, `DATE`, `TIME` – date and time etc.<br>
`NULL` – for all types.<br>


`DATEPART(datepart, date)`<br>
`DAY(date)`<br>
`MONTH(date)`<br>
`YEAR(date)`<br>
`DATEDIFF(datepart, startdate, enddate)`<br>


**Create a table using the query:**<br>
`CREATE TABLE <table name>`<br>
`    (`<br>
`    <field_name 1> <type_field 1>,`<br>
`    <field_name 2> <type_field 2>, …`<br>
`    )`<br>
_Example:_<br>
`CREATE TABLE department`<br>
`    (`<br>
`    id INT NOT NULL,`<br> 
`    name VARCHAR(30),`<br>
`    city VARCHAR(30)`<br>
`    )`<br>


**Modify table structure:**<br>
`ALTER TABLE`<br>
`DROP TABLE`<br>


**Modify the structure of the table:**<br>
`ADD [COLUMN]` – add a new field<br>
`ALTER [COLUMN]` – modify the field<br>
`DROP [COLUMN]` – delete the field<br>
`ADD CONSTRAINT` – add a new constraint<br>
`DROP CONSTRAINT` – delete the constraint<br>


**Simple integrity constraint types:**<br>
**`PRIMARY KEY`**- The PRIMARY KEY constraint uniquely identifies each record in a database table. Primary keys must contain UNIQUE values. A primary key column cannot contain NULL values. Most tables should have a primary key, and each table can have only ONE primary key.<br>
- **`UNIQUE`**<br>
- **`NULL/NOT NULL`**<br>
- **`FOREIGN KEY/REFERENCE`**<br>
- **`CHECK`**<br>


The **`SELECT`** keyword is the main SQL commands to extract data from database tables:<br>
`SELECT <field_name1>, <field_name2>, …`<br>
`FROM <table_name>`<br>
_Example:_<br>
`SELECT city`<br>
`FROM department`<br>


The **`ORDER BY`** keyword is used for sorting the result set by one or more columns.<br>
The **`DESC`** keyword is used for sort the records in a descending order.<br>
`SELECT name`<br>
`FROM department`<br>
`ORDER BY name`<br>
`DESC`<br>


The **`DISTINCT`** keyword is used for avoid duplication of information we get from the database.<br>
`SELECT DISTINCT city`<br>
`FROM department `<br>


The **`WHERE`** clause is used to extract only those records that fulfill a specified criterion.<br>
`SELECT <LIST OF FIELDS>`<br>
`FROM <TABLE>`<br>
`WHERE <CRITERIA>`<br>


**Comparison operators:**<br>
`=` - equal to;<br>
`>` - greater than;<br>
`<` - less than;<br>
`>=` - greater than or equal to;<br>
`<=` - less than or equal to;<br>
`<>` - not equal to;<br>
`ALL`	- TRUE if all of the subquery values meet the condition;<br>	
`AND`	- TRUE if all the conditions separated by AND is TRUE;<br>
`ANY`	- TRUE if any of the subquery values meet the condition;<br>
`BETWEEN`	- TRUE if the operand is within the range of comparisons;<br>	
`EXISTS` - TRUE if the subquery returns one or more records;<br>	
`IN`	- TRUE if the operand is equal to one of a list of expressions;<br>
`LIKE` - TRUE if the operand matches a pattern;<br>
`NOT`	- Displays a record if the condition(s) is NOT TRUE;<br>
`OR` - TRUE if any of the conditions separated by OR is TRUE;<br>
`SOME` - TRUE if any of the subquery values meet the condition;<br>


The **`IN`** operator allows you to specify multiple values in a WHERE clause.<br>
`SELECT firstname, lastname`<br> 
`FROM employee`<br>
`WHERE position IN ('Manager', ‘Seller’)`<br>


The **`LIKE`** operator is used in a WHERE clause to search for a specified pattern in a column.<br>
Wildcard characters:<br>
`%` - a substitute for zero or more characters;<br>
`_` - a substitute for a single character.


**`NOT`** operator is used to negate a condition in a **`SELECT`**, **`INSERT`**, `UPDATE`, or `DELETE` statement.**<br>
`NOT <condition>`<br>
_Example:_<br>
`SELECT city`<br> 
`FROM department`<br> 
`WHERE NOT city LIKE 'Lviv'`<br>


**JOIN**<br>
`INNER JOIN`br>
`OUTER JOIN`<br>
`LEFT JOIN`<br>
`RIGHT JOIN`<br>
`FULL JOIN`<br>


**Aggregate Functions** - SQL aggregate functions return a single value, calculated from values in a field.<br>
`COUNT(*)`;<br>
`COUNT(<field_name>)`<br>
`SUM(<field_name>)`<br>
`AVG(<field_name>)`<br>
`МАХ(<field_name>)`<br>
`MIN(<field_name>)`<br>


The **`GROUP BY`** statement is used in conjunction with the aggregate functions to group the result by one or more fields.<br>
`SELECT <field_name>,`<br> 
`        aggregate function(<field_name>)`<br>
`FROM <table_name>`<br>
`WHERE <condition>`<br>
`GROUP BY <field_name>`<br>


The **`HAVING`** clause includes a predicate used to filter rows resulting from the **`GROUP BY`** clause. Because it acts on the results of the **`GROUP BY`** clause, aggregation functions can be used in the **`HAVING`** clause predicate.<br>
`SELECT <field_name>,`<br>
`        aggregate function(<field_name>)`<br>
`FROM <table_name>`<br>
`WHERE <condition>`<br>
`GROUP BY <field_name>`<br>
`HAVING <condition_on_aggregate_function>`<br>


**`SELECT`** syntax:<br>
`SELECT [DISTINCT | ALL]<field_name_or_expression> [[AS] <alias>] [,…]`<br>
`FROM <table_name> [[AS] <alias>] [,…]`<br>
`[WHERE <condition>]`<br>
`[GROUP BY <list_of_fields>]`<br>
`[HAVING <condition_on_aggregate_function>]`<br>
`[ORDER BY <list_of_fields>]`<br>


Order of procedure for handling of certain parts of the query **`SELECT`**:<br>
`FROM`>`WHERE`>`GROUP BY`>`HAVING`>`SELECT`>`ORDER BY`<br>


A **subquery** is a query that is nested inside a **`SELECT`**, **`INSERT`**, **`UPDATE`**, or **`DELETE`** statement, or inside another subquery. 
A subquery can be used anywhere an expression is allowed.
There are a few rules that subqueries must follow:
- Subqueries must be enclosed within parentheses.
- A subquery can have only one column in the **`SELECT`** clause, unless multiple columns are in the main query for the subquery to compare its selected columns.
- An **`ORDER BY`** cannot be used in a subquery.
- Subqueries that return more than one row can only be used with multiple value operators, such as the IN operator.
- The **`BETWEEN`** operator cannot be used with a subquery; however, the **`BETWEEN`** operator can be used within the subquery.


The **`IN`** operator in subqueries can be used with the following SQL statements along with the comparison operators like:<br>
`=`, `<`, `>`, `>=`, `<=` etc.<br>
Usually, a subquery should return only one record, but sometimes it can also return multiple records when used with operators **`IN`**, **`NOT IN`** in the where clause. <br>
The query syntax:<br>
`SELECT <list_of_fields>`<br>
`FROM <table_name>`<br>
`WHERE <field_name> IN (<list_of_values>)`<br>


Subquery in the **`FROM`** clause. Subqueries are legal in a **`SELECT`** statement's **`FROM`** clause.<br>
The syntax for the query:<br>
`SELECT ...`<br> 
`FROM (subquery) [AS] name ...`<br>
The [AS] name clause is mandatory, because each table in a **`FROM`** clause must have a name. Any columns in the subquery select list must have unique names.<br>


**Correlated subquery** is a subquery that uses values from the outer query. The subquery is evaluated once for each row processed by the outer query.
`SELECT id, lastname`<br>
`FROM employee AS emp`<br>
`WHERE rate > ( SELECT AVG(rate)`<br> 
`    FROM employee`<br>
`    WHERE`<br>
`        id_department= emp.id_department);`<br>


The SQL **`EXISTS`** condition is used in combination with a subquery and is considered to be met, if the subquery returns at least one row. It can be used in a **`SELECT`**, **`INSERT`**, **`UPDATE`**, or **`DELETE`** statement.<br>
`WHERE EXISTS ( <subquery> );`<br>


The **`UNION`** operator is used to combine the result-set of two or more **`SELECT`** statements.<br>
`SELECT <list_of_fields>`<br>
`FROM <table1>`<br>
`UNION [ALL]`<br>
`SELECT <list_of_fields>`<br> 
`FROM <table2>`<br>


The **`INSERT`** statement is used to insert new data into a table.<br>
_Syntax 1_:<br>
`INSERT INTO <table>`<br>
`    (<field_name1>, <field_name2>, ...)`<br>
`VALUES`<br>
`(<value1>, <value2>, ...)`<br>
_Example 1:_<br>
`INSERT INTO department`<br>
`    (id, name, city)`<br>
`VALUES (5, 'Pluto', 'Lviv')`<br>
_Example 2:_<br>
`INSERT INTO employee`<br>
`    (id, lastname, firstname, position, date_employment, id_department, id_boss, rate, bonus)`<br>
`VALUES`<br>
`    (25, 'Семенів', 'Анатолій', 'Seller', '10/07/2014', 5, 1, 3200, 400)`<br>

_Syntax 2_:<br>
`INSERT INTO <table>`<br>
`    (<column 1>, <column2>, ...)`<br>
`[SELECT <statement>]`<br>
_Example 1:_<br>
`INSERT INTO employee`<br>
`    (id, lastname, firstname, position, date_employment, id_department, id_boss, rate, bonus)`<br>
`SELECT`<br>
`    (26, 'Ткачук', 'Віктор',	position, date_employment, id_department, id_boss, rate, bonus)`<br>
`FROM employee WHERE id = 25`<br>


The **`UPDATE`** statement is used to change existing data in a table.<br>
`UPDATE <table>`<br>
`SET`<br>
`    <column1> = <value1>,`<br>
`    <column2> = <value2>,`<br>
`    ...`<br>
`[WHERE <condition>]`<br>
_Example:_<br>
`UPDATE employee`<br> 
`SET`<br>
`    rate = rate * 1.5`<br>
`WHERE id = 25`<br>

SQL **`DELETE`** Statement is used to delete some data from a table.<br>
`DELETE FROM <table>`<br>
`[WHERE <condition>]`<br>
_Example 1:_<br>
`DELETE FROM employee`<br>
`WHERE id = 25`<br>
_Example 2:_<br>
`DELETE FROM employee`<br> 
`WHERE id_department = 5`<br>
_Example 3:_<br>
`DELETE FROM employee`<br>
`WHERE id_department = (SELECT id`<br> 
`            FROM department`<br>
`            WHERE name = 'Pluto')`<br>


**_CAUTION!_**<br>
Execute an appropriate **`SELECT`** statement before executing an **`UPDATE`** or **`DELETE`** statement and verify the count of rows to be affected.<br>
Never use **`UPDATE`** and **`DELETE`** without **`WHERE`** clause, otherwise the whole table will be changed (emptied).<br>


**What is data integrity?**<br>
To preserve the data integrity means to ensure that any of the data does not contradict another data.<br>
- Accuracy.
- Consistency.


**Integrity constraints:**<br>
- `NOT NULL` and `DEFAULT`
- `UNIQUE`
- `CHECK`
- `PRIMARY KEY`
- `FOREIGN KEY`


**Views** - special DB objects aimed to simplify creation of complex SQL queries, by encapsulation of **`SELECT`** queries so that they could be accessed in another queries just as they would be regular tables.<br>
`CREATE view vw_employee`<br>
`AS`<br>
`SELECT`<br>
`	e1.id,`<br>
`	e1.firstname,`<br> 
`	e1.lastname,`<br>
`	d.name depname,`<br>
`	d.city,`<br>
`	e2.firstname boss_firstname,`<br>
`	e2.lastname boss_lastname`<br>
`FROM employee e11`<br>
`LEFT JOIN employee e2`<br>
`ON e1.id_boss = e2.id`<br>
`LEFT JOIN department d`<br>
`ON e1.id_department = d.id`<br>
_Example of a View:_<br>
`SELECT * FROM vw_employee`<br>


**Stored Procedures** - a stored procedure is a subroutine available to applications that access a relational database system. Usually is written in special language which is the extension of SQL. For MS SQL Server this language is called Transact-SQL.<br>


**Advantages of SP:**<br>
- Performance;
- Testing;
- Scalability;
- Maintainability;
- Security;
- etc.


_Example of a Stored Procedure:_<br>
`CREATE PROCEDURE spAvgRateByCity`<br>
`	@City VARCHAR(30),`<br>
`	@AvgRate NUMERIC(10, 5) OUTPUT`<br>
`AS`<br>
`SELECT @AvgRate = AVG(e.rate)`<br>
`FROM employee e`<br>
`JOIN department e`<br>
`ON  e.id_department = d.id`<br>
`WHERE d.city = @City`<br>


_Stored Procedures Execution:_<br>
`DECLARE @AvgRate NUMERIC(10, 5)`<br>
`EXEC spAvgRateByCity ‘Lviv',`<br>
`	@AvgRate OUTPUT`<br>
`SELECT @AvgRate`<br>


**Triggers** - procedural code that is automatically executed in response to certain events on a particular table or view in a database.  The trigger is mostly used for maintaining the integrity of the information on the database.<br>


Types of Triggers:<br>
- **`AFTER`** triggers are executed after the action of the `INSERT`, `UPDATE`, or `DELETE` statement is performed.<br>
- **`INSTEAD OF`** triggers override the standard actions of the triggering statement.<br>
The primary advantage of `INSTEAD OF` triggers is that they enable views that would not be updatable to support updates.


_Example of a Trigger:_<br>
`CREATE TABLE emp_audit (ID INT IDENTITY, username VARCHAR(MAX),`<br>    
`  action_type VARCHAR(1),`<br>
`  NOTE VARCHAR(MAX), action_time datetime);`<br>

`CREATE TRIGGER t_insert_employee`<br>
`ON employee AFTER INSERT`<br>
`AS`<br>
`BEGIN`<br>
`    INSERT INTO emp_audit`<br>
`    	(username, action_type, note, action_time)`<br>
`  SELECT`<br>
`    CURRENT_USER, 'i',`<br>
`    concat(i.id , '|' , i.lastname , '|' , i.firstname) NOTE,`<br>
`    CURRENT_TIMESTAMP`<br>
`  FROM INSERTED i`<br>
`END;`<br>


_Testing the Trigger_<br>
`INSERT INTO employee`<br>
`	(id, lastname, firstname, position, date_employment,`<br>
`id_department, id_boss, rate, bonus)`<br>
`VALUES`<br>
`	(25, 'Семенюк', ‘Макар', 'Seller', '12/06/2000', 5, 1, 3500, 	500);`<br>

`SELECT * FROM emp_audit`<br>


_Testing the Trigger (2)_<br>
`UPDATE employee`<br>
`SET lastname = 'Прокопів'`<br>
`WHERE ID = 26;`<br>

`SELECT * FROM emp_audit`<br>


**Transactions** is a transaction is a sequence of operations performed as a single logical unit of work . A transaction is a series of database operations either all occur, or nothing occurs.<br>


Commands for transaction control:<br>
`BEGIN TRAN` (or `BEGIN TRANSACTION`)<br>
`COMMIT TRAN` (or `COMMIT TRANSACTION`)<br>
`ROLLBACK TRAN` (or `ROLLBACK TRANSACTION`)<br>


**Key properties – ACID:**<br>
- **A**tomicity - “All or nothing": if one part of the transaction fails, the entire transaction fails, and the database state is left unchanged;
- **C**onsistency - Ensures that any transaction will bring the database from one valid state to another;
- **I**solation - Ensures that the concurrent execution of transactions results in a system state that would be obtained if transactions were executed serially, i.e., one after the other. In case when two concurrent transactions attempt to update the same data, one of them will be paused rolled back;
- **D**urability - Ensures that once a transaction has been committed, it will remain so, even in the event of power loss, crashes, or errors.


_Transaction example:_<br>
`BEGIN TRAN`<br>

`UPDATE employee`<br>
`SET rate = rate * 1.2`<br>
`WHERE position = 'Seller';`<br>

`UPDATE employee`<br>
`SET rate = rate * 1.3`<br>
`WHERE position = 'Consultant'`<br>
 
`IF @@ROWCOUNT <= 2`<br>
`  COMMIT TRAN`<br>
`ELSE ROLLBACK TRAN`<br>


Managing transactions: other approach<br>
There is a possibility to manage DB transaction from upper layers of application (various components and frameworks).<br>
There are some frameworks and patterns for high level programming languages which implement some features of transactions, i.e. ORM systems, the “unit of work” pattern etc.<br>


<details>
  <summary><i>References:</i></summary>
  [SQL Tutorial W3School](http://www.w3schools.com/sql)<br>
  [SQL Tutorial for beginners](http://beginner-sql-tutorial.com)<br>
  [The Basics of Database Normalization](http://databases.about.com/od/specificproducts/a/normalization.htm)<br>
  [Database Normalization](http://holowczak.com/database-normalization/)<br>
  [Database Normalization Explained in simple English](http://www.essentialsql.com/get-ready-to-learn-sql-database-normalization-explained-in-simple-english/)<br>
  [Normalization in DBMS](http://www.studytonight.com/dbms/database-normalization.php)<br>
  [SQL Server Technical documentation](https://technet.microsoft.com/en-us/library/aa214068%28v=sql.80%29.aspx)
</details>


https://www.linkedin.com/posts/brijpandeyji_master-the-core-sql-commands-that-drive-80-activity-7247931048532017154-GFL4

>>In progress