**CONTENTS**

[[_TOC_]]

# <div align="center"> Security</div>
**Key security factors:**
- Most common security threat is lack of executive discipline (or excessive human error);
- Security is always expensive and inconvenient;
- There is no safe, only safer.


**Security** consists of **prevention**, **detection** and **response**.<br>


**NIST CSF (Cybersecurity framework):**
~~~
> Identify > Protect > Detect > Respond > Recover >
~~~

**Secure by design**
* Security architect should design the system. Everything else will be done by engineers;
* Start from beginning. And not the other way around;
* Security begins from up to bottom, there should be system level of doing things.
~~~
> Risk analysis > Policies > Architecture > Implementation > Administration > Audit >
~~~


**Design principles:**
1. Defense in depth: Multi-layered security without single point of failure (SPOF);
2. Least privileges: Right users have right access to right data for right reasons. With yearly rights revision. In hardware settings everything needless should be disabled;
3. Separation in duties: There should be no concentration of power and no corruption (like conflict of interests);
4. KISS (keep it simple stupid): Otherwise users won't cooperate. Security by obscurity. Secret or complexity doesn't increase security of a system. In infrastructure configuration implement FOSS principles.


**General directions of cybersecurity (CIA triad):**
- **Confidentiality**: Access control and encryption;
- **Integrity**: Immutability by message authentication codes and digital signatures;
- **Availability**: DDoS and SYN flood.


**Top 5 things that reduce the cost of a data breach:**
- Artificial inteligence (AI);
- DevSecOps approach;
- Incident response (IR);
- Cryptography;
- Employee training.


## <div align="left"> Prevention:</div>
**IAM (directory):**
- **Administration:** Process of giving and revoking rights to users;
- **Authentification (who you're?):** Password, MFA, Single Sign-On system (SSO), behavior analysis;
- **Authorization (what you allowed to do?):** Privileged access management (PAM);
- **Audit:** User behavior analytics (UBA), user entity behavior analytics (UEBA);
- **Federation capability:** Ability to use the IAM system in other services.


**Endpoint device:**
- Holistic white list of supported hardware. Inventory, security policies, monitor usage, mandatory updates, control, encryption, remote wipe, rgequired software, location tracking, disposal.
- Limitations of use and accepted control over personal devices, policies and services.


**Network:**
- Multilayered firewalls;
- Transparent proxy;
- NAT;
- Segmentation (DMZ);
- VPN: PPTP/L2TP(2), IPSec(3), TLS/SSL(4), SSH(7).<br>


**App:**<br>
DevSecOps is system administrator and security engineer in application development process.<br>
~~~
> Code > Build > Test > Release > Deploy > Monitor > Plan >
~~~


**Software development requirements (by OWASP.org):**
- Coding practicing;
- Trusted libs;
- Standard archs;
- Software Bill of Materials (SBOM) is a comprehensive list of all the software components, dependencies, and metadata associated with an application.


**Static application security testing (SAST):**<br>
**"White" box testing** is a form of application testing that provides the tester with complete knowledge of the application being tested, including access to source code and design documents. Examples: SonarQube Code Coverage Analysis.<br>
{+ Finds vulnerabilities earlier. +}


**Dynamic application security testing (DAST):**<br>
**"Black" box testing** a form of testing that is performed with no knowledge of a system's internals, can be carried out to evaluate the functionality, security, performance, and other aspects of an application.<br>


**Data security:**
- **Govern:**
  - Policy: What's sensitive;
  - Classification: Categories of sensitive data and how it is going to be secure;
  - Catalog: Structure of storing data;
  - Resilience: Plan how to restore lost data.
- **Discover:**
  - DB structure: How to look for data;
  - File unstructured: How to find unstructured data;
  - Network: How to find data that was send somewhere;
  - Data loss protection (DLP): How to discover lost data.
- **Protect:**
  - Encryption: Static and dynamic, at rest and in motion;
  - Key management: Frequent, complex and dynamic, but safe;
  - Quantum Safe Crypto (QSC);
  - Access control: Secure auth method;
  - Backup: Ransomware safe.
- **Comply:**
  - Report: Regulatory requirements (like GDPR);
  - Retain: How long we should store some types of data.
- **Detect:**
  - Monitor: Data flow;
  - UAB: Analyze changes in user behavior;
  - Alert: That will lead taking an action and opening a case;
- **Respond:**
  - Cases: To investigate an issue;
  - Dynamic playbook: Guide to take particular steps and based on results of that take another steps;
  - Orchistration;
  - Automation.


## <div align="left"> Detection:</div>
**Monitoring approaches:**
- **Security Information and Event Management system (SIEM)**: log management and network behavior anomaly detection;
  - **collect**: events, alarms and flow data;
  - **correlate**: make smaller more managable substance;
  - **analize**: rules policies, look for anomalies (AI/ML/UBA), trends (reports).<br>
 
 {- Works down-to-up -}

 
- **Extended Detection Response (XDR)**: federated search (particular conditions). There is no need to have a big database prefetched and there won't be huge traffic flow into the server.<br>

{+ Works up-to-down +}.<br> 

**Hunt:**<br>
Investigation is Reaction.
Threat hunting is Proaction:
~~~
> Experience + Instinct;
    > Hypothesis + Tools (SIEM+XDR);
        > Early detection.
~~~


[MyDLP: paid data loss prevention tool](https://mydlp.com)<br>
[OpenDLP: FOSS data loss prevention tool](https://code.google.com/archive/p/opendlp)<br>
[StaffCounter: time management tool and user activity monitoring software](https://staffcounter.net)<br>
[Kickidler employee monitoring software](https://www.kickidler.com)<br>
[СпрутМонитор](https://sprutmonitor.ru)<br>
[Stealthbits](https://stealthbits.com/free-trial)<br>



>wazuh


## <div align="left"> Response:</div>
**Incident response (IR):**
- Manual: Doesn't scale or truly repeatable;
- Triage: Real attack? Is it serious?;
- Remediate: fix, block, shutdown, patch apply.


**Security Orchestration and Automation Response (SOAR):**
Security Operation Center (SOC) operator use a dynamic playbook (consistent repeatable way of figuring out what the problems are). Dynamic playbooks consist of events, scripts and procedures.<br>


**Breach notification:**
- Types of data (name, SS#, CC#);
- Geography where it was compromised (nation, state);
- Regulatory requirements to follow (GDPR).


# <div align="center"> Vulnerabilities</div>
**Common Vulnerabilities and Exposures (CVE)**<br>
The mission of the CVE is to identify, define, and catalog publicly disclosed cybersecurity vulnerabilities.<br>


CVE is not a vulnerability database. CVE enables the correlation of vulnerability data across tools, databases, and people. This enables two or more people or tools to refer to a vulnerability and know they are referring to the same issue.<br>


CVE is restricted to publicly known vulnerabilities.
For a variety of reasons, sharing information is more difficult within the cybersecurity community than it is for hackers.
It takes much more work for an organization to protect its networks and fix all possible holes than it takes for a hacker to find a single vulnerability, exploit it, and compromise the network.
<br>


CVE helps because it enables rapid data correlation regarding a vulnerability across multiple information sources that are compatible with CVE. For example, if you own a security tool whose reports contain references to CVE IDs, you may then access fix information in a separate database that is compatible with CVE. CVE also provides you with a baseline for evaluating the coverage of your tools. With CVE’s common identifiers, you’ll know exactly what each tool covers allowing you to determine which tools are most effective and appropriate for your organization’s needs.<br>


In addition, if the security advisories your organization receives include CVEs, you can see if your vulnerability scanners check for this threat and then determine whether your intrusion detection system has the appropriate attack signatures to identify attempts to exploit particular vulnerabilities. If you build or maintain systems for customers, the inclusion of CVEs in advisories will help you to directly identify any fixes from the vendors of the commercial software products in those systems (if the vendor fix site is compatible with CVE).<br>


## <div align="left"> MITRE ATT&CK</div>
**MITRE ATT&CK** - framework is a knowledge base of tactics and techniques designed for threat hunters, defenders and red teams to help classify attacks, identify attack attribution and objectives, and assess an organization's risk.<br>
[MITTRE ATT&CK Navigator (Matrix):](https://mitre-attack.github.io/attack-navigator/)<br>


**Key attack methods:**
- Defense evasion;
- Exfiltration;
- Privilege escalation;
- Command and control (C2).


Reconnaissance: active scanning;
Initial Access: fishing compain by endpoints, email, DNS, humans;
Credential Access: use unsecured creadentials to login to another system;
Privilege escalation: use more valid account;
Collection: exfiltration data to himself;
Impact: destroying the data.


**Kill chain:**
1. Reconnaissance;
2. Weaponization;
3. Delivery;
4. Exploitation;
5. Installation;
6. Command and Control;
7. Actions on Objective.


**Command & control (C2):** at is core is being able to execute commands on a system that you can control. Usually by using SSH, Telnet, RDP, WinRM or some [C2 agents](https://thec2matrix.com). That is how most BotNets are created.<br>


Listeners (reverse shell) Trojan/backdoors types:
- Long stealth: to create BotNet for DDOS attack in the future;
- Short grunt: to be a part of some actual attack on particular system.<br>


C2C consists if technics that adversaries may use to communicate with systems under their control within a victim network.
Adversaries commonly attempt to mimic a normal, expected trafic to avoid detection.<br>


## <div align="center"> Linux:</div>
### <div align="center"> Basic security settings:</div>
#### <div align="left"> SSH:</div>
**Set up server authentication by authorized keys only:** <br>
Copy keys from the server at `~/.ssh/authorized_keys` and check that they are working:<br>
At `/etc/ssh/sshd_config` set parameter ***`PasswordAuthentication`*** to ***`no`***<br>
Restart SSH server:<br>
`sudo systemctl restart sshd.service`<br>
*For more safety look to Fail2ban, Crowdsec.<br>


#### <div align="left"> Firewall:</div>
*for Ubuntu:<br>
Add allowed ports, everything else will be blocked.<br>
`ufw allow <port>`<br>
_Examples:_ <br>
`ufw allow 80`<br>
`ufw allow 443`<br>
Restart, update settings and add to autostart.<br>
`ufw disable`<br>
`ufw enable`<br>


### <div align="center"> CVEs:</div>
#### <div align="left"> ASLR (Address Space Layout Randomization):</div>
~~~
sysctl -a –pattern 'randomize'
kernel.randomize_va_space = 2

#0 - Disabled
#1 - Conservative randomization
#2 - Full randomization
~~~


[SUID, SGUID](https://atom.hackstreetboys.ph/linux-privilege-escalation-suid-sgid-executables/)<br>

[Kernel Exploits](https://github.com/xairy/linux-kernel-exploitation)<br>


### <div align="center"> nCVEs:</div>
>in progress


## <div align="center"> Windows:</div>
### <div align="center"> CVEs:</div>


**Active Directory:**
NTLM:
[Top 10 active directory attack methods](https://www.lepide.com/blog/top-10-active-directory-attack-methods/)<br>


### <div align="center"> nCVEs:</div>

>розуміння архітектури, ключових сервісів та типових вразливостей середовища Active Directory (включаючи, але не обмежуючи NTLM, Kerberos, LDAP, LAPS, relay attacks, security descriptors, delegations, trusts, pass-the-hash, UAC bypass тощо)
<br>


# <div align="center"> Tools:</div>
**Any.run**<br>
[Any.run](https://any.run)<br>


**Virustotal.com**<br>
[Virustotal.com](https://virustotal.com)<br>


**Intezer.com**<br>
[Intezer.com](https://intezer.com)<br>


## <div align="left"> Security-exchanced Linux (SELinux):</div>
A system offering mandatory access control. Performance drop is less than 5%. <br>
SELinux {+ allows everything +} on OS level.<br>


Discretionary Access Control (DAC) is identity-based access control. DAC mechanisms will be controlled by user identification such as username and password. DAC is discretionary because the owners can transfer objects or any authenticated information to other users. In simple words, the owner can determine the access privileges.<br>


Mandatory Access Control
The operating system in MAC will provide access to the user based on their identities and data. For gaining access, the user has to submit their personal information. It is very secure because the rules and restrictions are imposed by the admin and will be strictly followed. MAC settings and policy management will be established in a secure network and are limited to system administrators.<br> 


MAC will stop even if DAC is allowing


**SELinux modes:**
- **Enforcing** mode the loaded security policy on the entire system
- **Permissive** mode acts as if SELinux is enforcing the loaded security policy, including labeling objects and emitting access denial entries in the logs, but it does not actually deny any operations;
- **Disabled** mode not only does avoid enforcing the policy, it also avoids labeling any persistent objects such as files, making it difficult to enable SELinux in the future. Is strongly discouraged.

Config file location:<br>
`/etc/selinux/config`<br>
This file controls the state of SELinux on the system.<br>
`SELINUX=` can take one of these three values:<br>
- `enforcing` - SELinux security policy is enforced;<br>
- `permissive` - SELinux prints warnings instead of enforcing;<br>
- `disabled` - No SELinux policy is loaded.<br>

Example, `SELINUX=enforcing`<br>


`SELINUXTYPE=` can take one of three values:<br>
- `targeted` - the default policy:<br>
  * Only targeted processes (there are hundreds) are protected by SELinux;
  * Everything else is unconfirmed.
- `minimum` - modification of targeted policy.<br>
  * Only selected processes are protected;<br>
- `mls` - multi-level/category security custom policies:
  * Can be very complex;
  * Typically used in TLA government organizations.

Example, `SELINUXTYPE=targeted`<br>


**Check SELinux service status:**<br>
`sestatus`<br>
**Search for logs changes:**<br>
`sudo ausearch -m AVC, USER_AVC -ts recent`<br>
**Set SELinux service mode to enforce:**<br>
`sudo setenforce 1`<br>
**Set SELinux service to permissive mode:**<br>
`sudo setenforce 0`<br>


**SELinux workds by labeling:**
- For **files and directories:** these labels are stored as extended attributes on the filesystem, user:role:type:level(optional);
- For **processors, ports, booleans and etc:** the kernel manages thes labels.


<details>
  <summary><i>Tips:</i></summary>
  How to deal with labels:<br>
  - `ls -Z`
  - `id -Z`
  - `ps -Z`
  - `netstat -Z`
</details>


Get SELinux boolean value(s):<br>
`getsebool -a`<br>
Find booleans with "httpd":<br>
`sudo semanage boolean -l | grep httpd`<br>
Change boolean parameter:<br>
`sudo semanage boolean -m --off httpd_ssi_exec`<br>
List the locally customized booleans by adding the -C option:<br>
`sudo semanage boolean -l -C`<br>
List file context definitions and add more:<br>
`sudo semanage fcontext -l | grep sshd`<br>
To store the sshd host keys in a separate subdirectory:<br>
`sudo semanage fcontext -a -t sshd_key_t '/etc/ssh/keys(/.*)?'`<br>
`sudo restorecon -r /etc/ssh/keys`<br>
View any locally-customized file contexts by adding the -C option:<br>
`sudo semanage fcontext -l -C`<br>
View the port contexts with:<br>
`sudo semanage port -l | grep http`<br>
Add a port definition with:<br>
`sudo semanage port -a -t http_cache_port_t -p tcp 8010`<br>
List any domains currently in permissive mode use:<br>
`sudo semanage permissive -l`<br>
Place a domain into permissive mode use:<br>
`sudo semanage permissive -a squid_t`<br>
All of the `semanage` commands that add or modify the targeted policy configuration store information in `*local` files under the `/etc/selinux/targeted` directory tree.<br>


**Troubleshooting:**<br>
- Install `setroubleshoot` and `setroubleshoot-server`: bunch of tools to diagnose and fix SELinux issues. Reboot after install.
- To see everything logged since last reboot:<br>
`journalcontrol -b -0`<br>


<details>
  <summary><i>References:</i></summary>
    [SEManage commands](https://www.redhat.com/sysadmin/semanage-keep-selinux-enforcing)<br>
    [SELinux Overview](https://youtu.be/_WOKRaM-HI4?si=25doBw8BgNci2Pkn)
</details>

~~~
                          SELinux architecture
+-------------------------------------------------+
|  +----------------+     +-----------------+     |
|  | SELinux Policy |  ←  |  Access Vector  |     |
|  | Database       |  →  |  Cache (AVC)    |     |
|  +----------------+     +-----------------+     |
|                                 ↑ ↓             |
|                        +--------------------+   |
|                        | Policy Enforcement |   |    +----------+
|                        | Server (PES)       |-------→| Log file |
|                        +--------------------+   |    +----------+
|                                 ↑ ↓             |  
|                        +---------------------+  |
|  +-----------+         | SELinux Abstraction |  | 
|  | SELinuxFS |         |    & Hook Logic     |  |
|  +-----------+         +---------------------+  | 
|                                 ↑ ↓             |
|                          +-----------------+    |
+--------------------------|      Linux      |----+
Events flow     +-----+    |     security    |
----------------| DAC |---→|      module     |--------→
                +-----+    +-----------------+
~~~

## <div align="left"> Linux AppArmor</div>
Is a Linux Security Module implementation of name-based mandatory access controls. Performance drop is around 0-2%.<br>
AppArmor {- denies everything -} on app level.<br>


**AppArmor components:**<br>
- **Server analyzer:** scans ports and determines which applications are listening. This component also detects if an application doesn't have a profile and if the server needs to confine it;
- **Profile generator** analyzes an application to create a profile template;
- **Optimizer** logs and gathers events.


To install the apparmor-profiles package from a terminal prompt:<br>
`sudo apt install apparmor-profiles`


**AppArmor modes of execution:**<br>
- **Complaining/Learning:** profile violations are permitted and logged. Useful for testing and developing new profiles;
- **Enforced/Confined:** enforces profile policy as well as logging the violation.<br>


**AppArmor types of rules in a profile:**<br>
- **Paths** determine which files an app or process can access;
- **Capabilities** specify the privilege that a confined process can use.


View the current status of AppArmor profiles.<br>
`sudo apparmor_status`
Places a profile into complain mode.<br>
`sudo aa-complain /bin/ping`
Places a profile into enforce mode.<br>
`sudo aa-enforce /bin/ping`


Directory is where the AppArmor profiles are located. It can be used to manipulate the mode of all profiles.<br>
`/etc/apparmor.d`<br>
The files are named after the full path to the executable they profile replacing the '/' with '.'<br>


Place all profiles into complain mode.<br>
`sudo aa-complain /etc/apparmor.d/*`
Place all profiles in enforce mode.<br>
`sudo aa-enforce /etc/apparmor.d/*`
Reload disabled profile.<br>
`sudo apparmor_parser -r /etc/apparmor.d/bin.ping`
Reload all profiles.<br>
`sudo systemctl reload apparmor.service`<br>
To disable a profile.<br>
`sudo ln -s /etc/apparmor.d/bin.ping /etc/apparmor.d/disable/`<br>
`sudo apparmor_parser -R /etc/apparmor.d/bin.ping`
Re-enable a disabled profile:<br>
`sudo rm /etc/apparmor.d/disable/bin.ping`<br>
`cat /etc/apparmor.d/bin.ping | sudo apparmor_parser -a`


**Two type of rules used in profiles:**
- **Path entries:** detail which files an application can access in the file system;
- **Capability entries:** determine what privileges a confined process is allowed to use.


`cat /etc/apparmor.d/bin.ping:`<br>
~~~
#include <tunables/global>
/bin/ping flags=(complain) {
  #include <abstractions/base>
  #include <abstractions/consoles>
  #include <abstractions/nameservice>

  capability net_raw,
  capability setuid,
  network inet raw,
  
  /bin/ping mixr,
  /etc/modules.conf r,
}
~~~
`#include <tunables/global>`: include statements from other files. This allows statements pertaining to multiple applications to be placed in a common file.<br>
`/bin/ping flags=(complain)`: path to the profiled program, also setting the mode to complain.<br>
`capability net_raw`: allows the application access to the CAP_NET_RAW Posix.1e capability.<br>
`/bin/ping mixr`: allows the application read and execute access to the file.


>Note: After editing a profile file the profile must be reloaded. 


Creating a profile:<br>
`sudo aa-genprof ping`

`sudo aa-logprof`



Confidentiality is controlling who can see the data.  Records openly accessible and open the the public are breaches of confidentiality.
Integrity is protecting against unauthorized changes.  Wrong or incorrect data entered by an unauthorized attacker would be an attack against integrity.
Availability is having systems and data accessible to authorized users.  Missing, offline, or unavailable information represent an attack against availability.


onfidentiality: Keeping sensitive information secret and disclosing it only to those who are authorized. This is akin to patient-doctor confidentiality but on a digital scale, employing encryption, access controls, and secure communication channels to maintain secrecy.
Integrity: Ensuring that information remains accurate and uncorrupted throughout its life cycle. Just as a patient's health record must be precise and up-to-date, InfoSec practices like checksumschecksums
A checksum is a value used to verify the integrity of a file or a data transfer. It is typically generated by an algorithm that processes the contents of a file or data packet and produces a short, fixed-size value (the checksum) that represents the content.
, hashinghashing
Hashing is a process that transforms input data (or 'message') of any size into a fixed-size string of characters. The output (the hash value) usually appears as a seemingly random sequence of characters.  This transformation is performed by a hash function.
, and audit trailsaudit trails
An audit trail is a record, sometimes called a log file, that shows who has accessed a computer system and what operations they performed during a given period of time. Audit trails are useful both for maintaining security and for recovering lost transactions.
 help preserve the trustworthiness of data.
Availability: Making sure that information is accessible to authorized users whenever it is needed. In healthcare, this could mean the difference between life and death, so systems must be resilient to attacks and failures, with backups and disaster recovery plans ready to activate.


Risk is a measure of impact and likelihood.
Impact refers to the potential consequences or extent of damage that could occur if a particular threat or vulnerability is realized.
This can include a variety of negative outcomes such as:
Financial Loss: Costs associated with data breaches, system downtime, or recovery efforts
Reputational Damage: Loss of customer trust and damage to the organization's brand
Operational Disruption: Interruption of business processes and services
Legal and Regulatory Consequences: Fines, penalties, and legal actions resulting from non-compliance with laws and regulations
Loss of Data Integrity: Unauthorized alterations to data, leading to incorrect or misleading information
Loss of Data Availability: Inability to access critical data when needed, affecting business operations
Breach of data confidentiality: Unauthorized access to sensitive information
Impact is a critical component of risk assessment as it helps organizations understand the severity of potential threats and prioritize their security measures accordingly.
Likelihood refers to the probability or chance that a specific threat or vulnerability will be realized. Likelihood is primarily determined by threats and vulnerabilities.
Threats: The presence and activity level of potential threats, such as hackers, malware, or natural disasters
Vulnerabilities: Weaknesses in the system, network, or processes that could be exploited by threats
Likelihood is a key component of risk assessment because it helps organizations gauge the probability of different risk events, further refining priorities.

https://www.linkedin.com/posts/cyberpress-org_cybersecurity-ecosystem-credits-francis-activity-7248649102425501696-TlxC
> in progress..


<details>
  <summary><i>References:</i></summary>
  [Cybersecurity Architecture Series (video, EN)](https://youtube.com/playlist?list=PLOspHqNVtKADkWLFt9OcziQF7EatuANSY&si=xpGEu8ytRzRbu4vY)
</details>



https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html