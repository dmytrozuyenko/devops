# **<div align="center">Git</div>**
**Git** is officially defined as a distributed version control system.


**Git Workflow:**
~~~
Master
  Hot fix
    Release
      Develop
        Feature
          Feature
~~~

Initialize the current directory as a git repository<br>
git init .<br>
See changes to tracked files, untracked files, etc.<br>
git status<br>
Add just that file or directory to next commit<br>
git add *directory_or_file_name*<br>
Commit files into repository<br>
git commit -m "commit message"<br>
Check all changes<br>
git log<br>
Move to specific commit state or branch<br>
git checkout *commit_ID_or_branch_name*<br>
Create branch<br>
git branch *branch_name*<br>
Merge branche with current one<br>
git merge *branch_name*<br>
Add remote repository<br>
git remote add origin https://gitlab.com/*username*/*reposiroty_name*.git<br>
Fetch from and integrate with another repository or a local branch<br>
git pull<br>
Transfer commits from your local repository to a remote repo<br>
git push -uf origin *branch_name*<br>
Show various types of objects<br>
git show<br>


https://stackoverflow.com/questions/2427238/what-is-the-difference-between-merge-squash-and-rebase

https://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-locally-and-remotely

https://graphite.dev/guides/git-remove-committed-files


Git
https://youtu.be/wpISo9TNjfU


https://www.geeksforgeeks.org/chattr-command-in-linux-with-examples/


**Create a new branch with given name**:
`git checkout -b <branch_name>`
**Check status: choosen branch, state of file changes**:
`git status`
**After making changes, add them to commit**:
`git add *`
**Commit change with a given message**:
`git commit -m "<ccommit_message>"`
**Push commited changes to the given branch in the repo**:
`git push --set-upstream origin <branch_name>`

**Change commit message in the last not pushed yet commit**:
`git commit --amend -m "new_commit_message"`


**Change 'main'/'master' branch to another branch**:
`git remote rename origin source`
`git remote add origin git@<github_host>:<user>/<repo_name>.git`
`git push origin <branch_name>`

**Reverse all commits in PR**:
`git diff main > <branch_name>.diff`
`git apply --reverse <branch_name>.diff`

**Reverse commits one by one in PR (affective immidietly)**:
`git reset --hard HEAD^`<br>
`git push --set-upstream origin <branch_name> --force`

**Change release tag**:
`git push --delete origin <tag_name>`

**Clone git and then choose release version**:
`git checkout tags/v<tag_version>`


**Forcefully deletes given local branch from local Git repository**:
`git branch -D feature-x`

**Condensed view of the commit history**:
`git log --outline`

**Delete added to git file**:
`git reset <file_path>`

**To modify a specific commit, you need to start an interactive rebase session.**:<br>
`git rebase -i <commit-hash>^`

   o---o---o---o---o  main
        \
         o---o---o---o---o  featureA
              \
               o---o---o  featureB

git rebase --onto main featureA featureB

                      o---o---o  featureB
                     /
    o---o---o---o---o  main
     \
      o---o---o---o---o  featureA

https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase

# **<div align="center">Message Brokers</div>**
Message brokers are an inter-application communication technology to help build a common integration mechanism to support cloud native, microservices-based, serverless and hybrid cloud architectures.<br>
Message brokers, unlike API, enable asynchronous communications between services so that the sending service need not wait for the receiving service’s reply. This improves fault tolerance and resiliency in the systems in which they’re employed.<br>
~~~
Producer > Message broker > Consumer
Message broker = Exchange > Message queue
~~~


## **<div align="center">Apache Kafka</div>**
The messages are not in separate queues. On the other hand, consumers determine where they stay on message queues with their own pointers. After the consumer processes a message, it moves its pointer. The messages are not deleted from the queues because another consumer can also process this message. It acts as if there are more than one queue by using pointers.<br>


**Dumb broker / smart consumer model** does not try to track which messages are read by consumers and only keeps unread messages. Kafka keeps all messages for a set period of time.<br>


## **<div align="center">RabbitMQ</div>**
Each consumer has its own queue. Samples of messages sent on exchange are stored in these queues. When the consumer finishes the transaction, the message is removed from consumer’s queue. Because another consumer does not read a message through the queue that belongs to someone else.<br>


**Smart broker / dumb consumer model** — consistent delivery of messages to consumers, at around the same speed as the broker monitors the consumer state.<br>

