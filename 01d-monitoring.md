**CONTENTS**

[[_TOC_]]


# **<div align="center"> Monitoring tools</div>**


## **<div align="left"> Prometheus</div>**
**Prometheus** is an open-source systems monitoring and alerting toolkit. Most Prometheus components are written in Go, making them easy to build and deploy as static binaries.

Node Exporter (/metrics) > Prometheus > Grafana

Prometheus
Retrieval
Storage
HTTP Server

Target
Unit
Metric = target + unit

Metric
Counter
Gauge
Histogram

Exporter

https://youtu.be/h4Sl21AKiDg?si=QduQRKG3Ao7eS-Br


## **<div align="left"> Grafana</div>**
**Grafana** is a multi-platform open source analytics and interactive visualization web application. It provides charts, graphs, and alerts for the web when connected to supported data sources.<br>


## **<div align="left"> ZABBIX</div>**
**ZABBIX**<br>
Zabbix Agent or Agentless (via SNMP).