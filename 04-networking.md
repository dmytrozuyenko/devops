**CONTENTS**

[[_TOC_]]


# **<div align="center">Networking</div>**
More and more of it is being abstracted.


**Model OSI:**
- 7. Application layer -  HTTPS, SSH, DHCP, DNS, FTP, SNMP, POP3, IMAP
- 6. Presentation layer - SSL/TLS
- 5. Session layer - sockets (SOCKS, NetBIOS, PPTP, RTP)
- 4. Transport layer - TCP/UDP
- 3. Network layer - packets and ports (IP, IPSec, ICMP)
- 2. Data Link Layer - MAC and LLC (ARP, RARP, NDP)
- 1. Physical Layer - transmission media


**TCP/IP:**
- 5. Application layer -  HTTPS, SSL/TLS, SSH, DHCP, DNS, FTP, SNMP, POP3, IMAP
- 4. Transport layer - TCP, UDP
- 3. Internet layer - packets and ports (IP, IPSec, ICMP)
- 2. Data Link Layer - MAC and LLC (ARP, RARP, NDP)
- 1. Physical Layer - transmission media

## **<div align="center">Application layer:</div>**


## **<div align="center">Network layer::</div>**

- **Domain:** A specific part of a network;
- **Bandwidth:** The amount of data that can be carried on a link in a given time period;
- **Unicast data:** Data sent to one device;
- **Multicast data:** Data sent to a group of devices;
- **Broadcast data:** Data sent to all devices;
- **Collision domain:** Includes all devices that share the same bandwidth; collision domains are separated by switches;
- **Broadcast domain:** Includes all devices that receive broadcast messages; broadcast domains are separated by routers.


**The Internet Control Message Protocol (ICMP)** is a network layer protocol used by network devices to diagnose network communication issues. ICMP is mainly used to determine whether or not data is reaching its intended destination in a timely manner. Commonly, the ICMP protocol is used on network devices, such as routers. IMP is crucial for error reporting and testing, but it can also be used in distributed denial-of-service (DDoS) attacks


## **<div align="center">Transport layer:</div>**
*After DNS lookup.*
**TCP handshake:**
- **Step 1 (SYN)**: In the first step, the client wants to establish a connection with a server, so it sends TCP SYNchronize packet with a segment with SYN(Synchronize Sequence Number) which informs the server that the client is likely to start communication and with what sequence number it starts segments with;
- **Step 2 (SYN + ACK)**: Server responds to the client request with SYN-ACK signal bits set. Acknowledgement(ACK) signifies the response of the segment it received and SYN signifies with what sequence number it is likely to start the segments with;
- **Step 3 (ACK)**: In the final part client acknowledges the response of the server and they both establish a reliable connection with which they will start the actual data transfer.


The initiator, generally the browser, sends a TCP SYNchronize packet to the other host, generally the server.
The server receives the SYN and sends back a SYNchronize-ACKnowledgement.
The initiator receives the server's SYN-ACK and sends an ACKnowledge. The server receives ACK and the TCP socket connection is established.


**Termination of TCP connection**:
The initiator sends a FIN packet to the other host.
The other host sends an ACK packet back to the initiator.
Now, the connection is half-closed, and the other host can still send data. (For example, the server can finish off sending data to the client when the client has closed its connection to the server.)
The other host sends a FIN packet to the initiator.
The initiator sends an ACK packet back to the other host.

*Three Way Handshake: TCP 3-way handshake, also known as a 3-way handshake, is a protocol for establishing a connection between a server and a client in a TCP/IP network. Before the real data communication process begins, both the client and server must exchange synchronization and acknowledgment packets.*


**Ports:**
- **FTP:** TCP port 20 (data) and 21 (control)
- **SSH:** TCP port 22
- **SMTP:** TCP port 25 and 465 (SSL/TLS)
- **DHCP:** UDP port 67, 68
- **TFTP:** UDP port 69
- **HTTP:** TCP port 80 and 443 (SSL/TLS)
- **POP3:** TCP port 110 and 995 (SSL/TLS)
- **IMAP:** TCP port 143 and 993 (SSL/TLS)
- **SNMP:** UDP port 161



**FTP (File Transfer Protocol)** is a network protocol for transmitting files between computers over Transmission Control Protocol/Internet Protocol (TCP/IP) connections. Within the TCP/IP suite, FTP is considered an application layer protocol. In an FTP transaction, the end user's computer is typically called the local host. The second computer involved in FTP is a remote host, which is usually a server. Both computers need to be connected via a network and configured properly to transfer files via FTP.


**The Simple Mail Transfer Protocol (SMTP)** is a technical standard for transmitting electronic mail (email) over a network.
Like other networking protocols, SMTP allows computers and servers to exchange data regardless of their underlying hardware or software. Just as the use of a standardized form of addressing an envelope allows the postal service to operate, SMTP standardizes the way email travels from sender to recipient, making widespread email delivery possible.


**IPv4 Packet header**:
The header is the beginning or front part of a packet. Any processing or receiving device, such as a router or a switch, sees the header first. The following 13 fields are included in an IPv4 protocol header:
- **Version**: field indicates the format of the internet header.
- **Internet header length (IHL)**: is the length of the internet header in 32-bit words that points to the beginning of the data.
- **Type of service**: indicates the abstract parameters of the quality of service desired.
- **Total length**: is the length of the datagram measured in octets that includes the internet header and data. This field allows the length of a datagram to be up to 65,535 octets.
- **Identification**: the sender assigns an identifying value to aid in assembling the fragments of a datagram.
Flags. These are various control flags.
- **Fragment offset**: field indicates where in the datagram this fragment belongs. The fragment offset is measured in units of eight octets, or 64 bits. The first fragment has offset zero.
- **Time to live (TTL)**: field indicates the maximum time the datagram is allowed to remain in the internet system. If this field contains the value of zero, then the datagram must be destroyed.
- **Protocol**: field indicates the next-level protocol used in the data portion of the internet datagram.
- **Header checksum**: checksum detects corruption in the header of the IPv4 packets.
- **Source address**: is the 32-bit source IP address.
- **Destination address**: is the 32-bit destination IP address.
- **Options**: field is optional, and its length can be variable. A source route option is one example, where the sender requests a certain routing path. If an option is not 32 bits in length, it uses padding options in the remaining bits to make the header an integral number of 4-byte blocks.
- **Payload**: is the actual data information the packet carries to its destination. The IPv4 payload is padded with zero bits to ensure that the packet ends on a 32-bit boundary.
- **Trailer**: sometimes, certain network protocols also attach an end part or trailer to the packet. An IP packet doesn't contain trailers, but Ethernet frames do.



Packet structure:
Level 2 - Level 3 - Level 4 - Level 5-7 
MAC     - IP      - TCP/UDP - HTTP, DNS, FTP

                                                                App Data
                                                App Header -    App Data
                                TCP Header -    App Header -    App Data
                    IP Header - TCP Header -    App Header -    App Data
Ethernet Header -   IP Header - TCP Header -    App Header -    App Data - Ethernet Trailer





## **<div align="center">Data link layer:</div>**

**Address Resolution Protocol (ARP)** is a procedure for mapping a dynamic IP address to a permanent physical machine address in a local area network (LAN). The physical machine address is also known as a media access control (MAC) address. The job of ARP is essentially to translate 32-bit addresses to 48-bit addresses and vice versa. This is necessary because IP addresses in IP version 4 (IPV4) are 32 bits, but MAC addresses are 48 bits.


**Ping (Packet Internet Groper)** is a method for determining communication latency between two networks. Simply put, ping is a method of determining latency or the amount of time it takes for data to travel between two devices or across a network. As communication latency decreases, communication effectiveness improves.

## **<div align="center">Encapsulation/Decapsulation:</div>**
**Encapsulation** is the process of adding additional information when data is traveling in TCP/IP model. When data moves from upper layer to lower layer of TCP/IP protocol stack, during an outgoing transmission, each layer includes a bundle of relevant information called "header" along with the actual data.


**Decapsulation** is the process of opening up encapsulated data that are usually sent in the form of packets over a communication network. It can be literally defined as the process of opening a capsule, which, in this case, refers to encapsulated or wrapped-up data.

**TCP/IP:**
- 5. Application layer:    Data
- 4. Transport layer:      Segments
- 3. Internet layer:       Packets
- 2. Data Link Layer:      Frames
- 1. Physical Layer:       Bites


**Data**


**Segments**
A process is divided into Segments. The chunks that a program is divided into which are not necessarily all of the same sizes are called segments. Segmentation gives user's view of the process which paging does not give.


**Packets**

**Frames:**
A frame is a unit of data. A frame works to help identify data packets used in networking and telecommunications structures. Frames also help to determine how data receivers interpret a stream of data from a source.


**Bits**
A bit (binary digit) is the smallest unit of data that a computer can process and store. A bit is always in one of two physical states, similar to an on/off light switch. The state is represented by a single binary value, usually a 0 or 1. However, the state might also be represented by yes/no, on/off or true/false. Bits are stored in memory through the use of capacitors.


IPV4
IPv4 or Internet Protocol version 4, address is a 32-bit string of numbers separated by periods. It uniquely identifies a network interface in a device.IPv4 is the first non-experimental Internet Protocol.

IP/6
IPv6 is the latest version of the Internet Protocol, which identifies devices across the internet so they can be located. Every device that uses the internet is identified through its own IP address in order for internet communication to work.

Dual Stacking
Dual stack means that devices are able to run IPv4 and IPv6 in parallel. It allows hosts to simultaneously reach IPv4 and IPv6 content, so it offers a very flexible coexistence strategy.

Tunneling
Tunnelling is a protocol for transferring data securely from one network to another. Using a method known as encapsulation, Tunnelling allows private network communications to be sent across a public network, such as the Internet.
Encapsulation enables data packets to appear general to a public network when they are private data packets, allowing them to pass unnoticed.

Switches
A network switch forwards data packets between devices. Switches send packets directly to devices, rather than sending them to networks like a router does. Switches can be hardware devices that manage physical networks, as well as software-based virtual devices.

Private IP
A private IP address is a range of non-internet facing IP addresses used in an internal network. Private IP addresses are provided by network devices, such as routers, using network address translation. If you want to find the IP of a device you're connected to, you can use the "netstat -an" command in the terminal.

Public IP
A public IP address is an IP address that your home or business router receives from your ISP and it's used when you access the internet. Public IP addresses are required for any publicly accessible network hardware such as a home router and the servers that host websites. You can find you public IP address by typing what is my IP on google.

IP
Stands for Internet Protocol. This network protocol outlines how almost all machine-to-machine communications should happen in the world. Other protocols like TCP, UDP and HTTP are built on top of IP.

HTTP
The HyperText Transfer Protocol is a very common network protocol implemented on top of TCP. Clients make HTTP requests, and servers respond with a response.
Requests typically have the following schema:
host: string (example: algoexpert.io)
port: integer (example: 80 or 443)
method: string (example: GET, PUT, POST, DELETE, OPTIONS or PATCH) headers: pair list (example: "Content-Type" => "application/json") body: opaque sequence of bytes


## **<div align="center">DNS</div>**
A DNS service is a globally distributed service that translates human-readable names into numeric IP addresses. DNS servers translate requests for names into IP addresses. They control which server an end user will reach when they type a domain name into their web browser.

DNS vs. DDNS
Dynamic DNS (DDNS) is an extension of DNS that automatically updates IP addresses associated with domain names in real time. It expands the capabilities of DNS. With DDNS, organizations and individuals can maintain connectivity and accessibility even in dynamic IP address environments.

DNS is universally supported by all DNS servers and used globally to resolve domain names to IP addresses.

DDNS, on the other hand, requires the support of specific DDNS providers. Organizations must subscribe to DDNS services and configure their devices or routers to work with the chosen DDNS provider.


**There are 4 DNS servers involved in loading a webpage**:
- **DNS recursor** - The recursor can be thought of as a librarian who is asked to go find a particular book somewhere in a library. The DNS recursor is a server designed to receive queries from client machines through applications such as web browsers. Typically the recursor is then responsible for making additional requests in order to satisfy the client’s DNS query;
- **Root nameserver** - The root server is the first step in translating (resolving) human readable host names into IP addresses. It can be thought of like an index in a library that points to different racks of books - typically it serves as a reference to other more specific locations;
- **TLD nameserver** - The top level domain server (TLD) can be thought of as a specific rack of books in a library. This nameserver is the next step in the search for a specific IP address, and it hosts the last portion of a hostname (In example.com, the TLD server is “com”);
- **Authoritative nameserver** - This final nameserver can be thought of as a dictionary on a rack of books, in which a specific name can be translated into its definition. The authoritative nameserver is the last stop in the nameserver query. If the authoritative name server has access to the requested record, it will return the IP address for the requested hostname back to the DNS Recursor (the librarian) that made the initial request.


[How DNS works](https://howdns.works/)
What happens afte domain name entered into browser address bar:
1. Checking Web browser cache (DNS Cache in Chrome);
2. Checking Operating system cache (hosts);
3. Checking DNS Resolver server cache, usually Internet Service Provider's (ISP) caching DNS server;
4. Checking root server cache for location of Top-Level Domain (TLD), for example, .COM;
5. Reciveing authoritative name servers (NS) IP addresses from TLD server;
6. Recieving the IP address of the domain from Name Server (NS).
*Saving in caches on the way back.*

Check https://www.youtube.com/watch?v=BkH3V3538YI

https://www.linkedin.com/posts/xmodulo_cybersecurity-infosec-devops-activity-7250473511087935488-AskV

https://github.com/alex/what-happens-when




>Dyn DNS

## **<div align="center">Routing protocols</div>**
**Border Gateway Protocol (BGP)** is a standardized exterior gateway protocol designed to exchange routing and reachability information among autonomous systems (AS) on the Internet.


**Open Shortest Path First (OSPF)** is a routing protocol for Internet Protocol (IP) networks. It uses a link state routing (LSR) algorithm and falls into the group of interior gateway protocols (IGPs), operating within a single autonomous system (AS). OSPF gathers link state information from available routers and constructs a topology map of the network. The topology is presented as a routing table to the internet layer for routing packets by their destination IP address. 


**Enhanced Interior Gateway Routing Protocol (EIGRP)** is an advanced distance-vector routing protocol that is used on a computer network for automating routing decisions and configuration. The protocol was designed by Cisco Systems as a proprietary protocol, available only on Cisco routers.


**The Routing Information Protocol (RIP)** is one of the oldest distance-vector routing protocols which employs the hop count as a routing metric.


## **<div align="center">Wireless</div>**


## **<div align="center">Security</div>**
### **<div align="center">SSL/TLS handshake</div>**
**SYMMETRIC ENCRYPTION**
Both sides have the same key, that capable to encrypt and decrypt the message.

**ASYMETRIC ENCRYPTION**
Two different but methematically related key. What encrypted by one can be only decrypted by another one, and vice versa.


**TLS handshake** is the process that kicks off a secure communication session that uses TLS (Transport Layer Security). <br>
During a TLS handshake, the two communicating sides exchange messages to acknowledge each other, verify each other, establish the cryptographic algorithms they will use, and agree on session keys. <br>
*SSL was the original security protocol developed for HTTP. SSL replaced by TLS and becomes legacy, but "SSL" name is still in wide use.* <br>
*TLS handshakes are a foundational part of how HTTPS works.* <br>
*TLS handshakes occur after a TCP connection has been opened via a TCP handshake.*


**The steps of a TLS handshake**:
- The **'client hello'** message: The client initiates the handshake by sending a "hello" message to the server. The message will include which TLS version the client supports, the cipher suites supported, and a string of random bytes known as the "client random.";
- The **'server hello'** message: In reply to the client hello message, the server sends a message containing the server's SSL certificate, the server's chosen cipher suite, and the "server random," another random string of bytes that's generated by the server;
- **Authentication**: The client verifies the server's SSL certificate with the certificate authority that issued it. This confirms that the server is who it says it is, and that the client is interacting with the actual owner of the domain;
- **The premaster secret**: The client sends one more random string of bytes, the "premaster secret." The premaster secret is encrypted with the public key and can only be decrypted with the private key by the server. (The client gets the public key from the server's SSL certificate.)
- **Private key used**: The server decrypts the premaster secret;
- **Session keys created**: Both client and server generate session keys from the client random, the server random, and the premaster secret. They should arrive at the same results;
- **Client is ready**: The client sends a "finished" message that is encrypted with a session key;
- **Server is ready**: The server sends a "finished" message encrypted with a session key;
- **Secure symmetric encryption achieved**: The handshake is completed, and communication continues using the session keys.


**The steps of a TLS 1.3 handshake**:
- **Client hello**: The client sends a client hello message with the protocol version, the client random, and a list of cipher suites. Because support for insecure cipher suites has been removed from TLS 1.3, the number of possible cipher suites is vastly reduced. The client hello also includes the parameters that will be used for calculating the premaster secret;
- **Server generates master secret**: At this point, the server has received the client random and the client's parameters and cipher suites. It already has the server random, since it can generate that on its own. Therefore, the server can create the master secret;
- **Server hello and "Finished"**: The server hello includes the server’s certificate, digital signature, server random, and chosen cipher suite. Because it already has the master secret, it also sends a "Finished" message;
- **Final steps and client "Finished"**: Client verifies signature and certificate, generates master secret, and sends "Finished" message.
Secure symmetric encryption achieved.
*Essentially, the client is assuming that it knows the server’s preferred key exchange method (which, due to the simplified list of cipher suites, it probably does). This cuts down the overall length of the handshake — one of the important differences between TLS 1.3 handshakes and TLS 1.0, 1.1, and 1.2 handshakes.*



# **<div align="center">API</div>**



HTTP structure headers and etc

commands:

curl 
https://www.warp.dev/terminus/curl-headers

https://www.warp.dev/terminus/curl-post-request

https://www.google.com/search?q=curl+http+method&sca_esv=0baf5c9e671f57b4&udm=2&biw=1512&bih=772&ei=ZY9YZ5-6OvfT5NoPn_-FoQI&ved=0ahUKEwjfgNaQ8Z2KAxX3KVkFHZ9_ISQQ4dUDCBA&uact=5&oq=curl+http+method&gs_lp=EgNpbWciEGN1cmwgaHR0cCBtZXRob2QyBRAAGIAEMgYQABgHGB4yBhAAGAcYHjIGEAAYBxgeMgYQABgHGB4yBRAAGIAEMgUQABiABDIEEAAYHjIEEAAYHjIEEAAYHkjaEVCrC1j-D3ACeACQAQCYAVegAfkCqgEBNbgBA8gBAPgBAZgCB6ACmQPCAggQABgHGAoYHsICChAAGIAEGEMYigWYAwCIBgGSBwE3oAfCGg&sclient=img#vhid=Jl24sQSWg2cYHM&vssid=mosaic

JWT
https://www.youtube.com/watch?v=P2CPd9ynFLg


uuidgen



Basic Auth, APIKey, JWT




Sessions vs. tokens

**Sessions** (requires server-side storage):
- User submits login form (HTTP request on /login webpage);
- After validation of creds, server stores a session, and respons with session ID;
- Browser saves session ID in cookies, and uses it to communicate with the server;
- Server responds with content related to this particular session ID.
 *Drawback: Hard to store sessions data at servers that usually horizontaly scalable nowadays.*


**Token-based auth** (works on client's side):
- User submits login form (HTTP request on /login webpage);
- Server creates and signs JWT (JSON Web Token) with a secret key;
- Browser puts JWT in local storage;
- Signed JWT header validated on future requests. JWT is added on future requests as Bearer <token>;
- The server need to validate only signature of JWT.
*Advantage: More efficient with distributed system in the cloud.*
*Disadvantage: Revoking JWT could be challenging.*
*It's like a hotel room access card. You don't need ID documents, you only show them once when you recieved the card at the front desk, after entering the hotel.*


**Structure of a JWT:**
- **Header:** token and algorythm type; 
- **Payload:** statements about an entity, usually user info with some additional data. Contains three type of claims: Public, Private, and Registered. Registered claims are predefined: issuer, expiration time, and subject.
*Payload could be encrypted using JSON Web Encryption (JWE).*
- **Signature:** ensures JWT hasn't been tempered with:
    - Symmetrical algorythms (HMAC, HS256);
    - Asymmetrical algorythms (RSA, ES256, ECDSA).


**JWT signing algorythms:**
- **HMAC** quick and simple, but require to share private key ahead of time (monolithic architecture);
- **RSA** or **ECDSA** allows verification of the creator without sharing private keys, but slower (microservice architecture).
*When the token expires, instead of requiring user to log in again, the client can send Refresh token to a special token endpoint on Auth Server. The server checks if the refresh token is valid and hasn't been revoked. And issues a new Access Token.*


HS256, HS384, HS512, RS256, RS384, RS512, ES256, ES384, ES512, PS256, PS384, PS512 or EdDSA.

RS*, ES*, PS* or EdDSA* the public key 

- **Refresh tokens** usually have expiration time set in day/weeks;
- **Short-lived access tokens** have expiration time set ~15 mins.





WEB
https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

https://www.howtogeek.com/119458/htg-explains-whats-a-browser-cookie/

