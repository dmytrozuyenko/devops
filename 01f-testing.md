**CONTENTS**

[[_TOC_]]


# **<div align="center">Testing:</div>**


## **<div align="center">Static application security testing (SAST)</div>**
###**<div align="left">SonarQube</div>**
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube







sudo sysctl -w vm.max_map_count=262144
sudo sysctl -w fs.file-max=65536
sudo ulimit -n 65536
sudo ulimit -u 4096


mkdir sonarqube
cd sonarqube
mkdir sonarqube_conf
mkdir sonarqube_extensions
mkdir sonarqube_logs
mkdir sonarqube_data

sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update

sudo apt install docker-ce
# sudo systemctl status docker
sudo usermod -aG docker ${USER}

sudo reboot

docker run -d -p 9000:9000 --name sonarqube -v sonarqube_conf:/opt/sonarqube/conf -v sonarqube_extensions:/opt/sonarqube/extensions -v sonarqube_logs:/opt/sonarqube/logs -v sonarqube_data:/opt/sonarqube/data sonarqube





https://egghead.io/lessons/jest-configuring-code-coverage-for-typescript-files-in-jest

## **<div align="center">Dynamic application security testing (DAST)</div>**





CI stage:
Code coverage;
Security scan;
Unit test execution;
Binary Repo;
Static code analysis;

CD stage:
Regression Test Run (Selenium)
UAT Test Run (Selenium)