**CONTENTS**

[[_TOC_]]


# <div align="center"> DevSecOps Engineer</div>
**DevSecOps Engineer** is SecOps that is working with a developer team in hot stage of development.<br>
Using DevSecOps methodology to securely develop, automate and support distributed cloud infrastructure, based on SLA, is called DevSecOps Engineer.<br> 
*DevSecOps is about Practice, not Tools.*


~~~

System Administrator
              \
              DevSecOps (SRE) > Technical Architect
              /                               \
Security Engineer                           Solution Architect
                                              /
                Software Architect (Application Architect)
                    /				
Developer > Tech Lead
                    \
           Team Lead > Project Manager (Product Manager) > Delivery Manager (CTO)

~~~


DevOps ensures that the results of programmers’ work reach the end user as quickly and efficiently as possible, so developers can use CI easily and intuitively, without being distracted by routine. While DevOps engineer automates the application life cycle, SRE is responsible for the stable operation of the system.<br>


**SRE (Site Reliability Engineer)** is what happens when you treat operations as a software problem and staff it with a bunch of software engineers.<br>

<details>
  <summary><i>SRE:)</i></summary>
  - Simply Restart Everything;<br>
  - Sysadmin (Really Expensive);<br>
  - Server Reboot Engineer;<br>
  - Sometimes Reliable Engineer.<br>
</details>


**Duties:**
- Version control and branching strategies;
- Continuous integration and continuous delivery (CI/CD) pipelines;
- Containers that standardize and isolate application runtime environments;
- Infrastructure as code (IAC), which enables scripting the infrastructure layer;
- Monitoring the devsecops pipelines and health of running applications.


**CI/CD**
~~~
> Plan > Code > Build > Test > Release > Deploy > Operations > Monitoring >
~~~
**Continuous security approach**
~~~
> Threat modeling > Scan > Analyze > Remediate > Monitor >
~~~


**Benefits:**
- Faster time to market and feedback;
- Increased customer satisfaction;
- Improving the ability to create the right solutions;
- Higher product quality;
- More reliable releases;
- Productivity increase;
- Reducing costs and risks;
- Traditional infrastructure & operations;
- System administration;
- End-to-end product responsibilities;
- Supporting development teams with a combination of release automation, deployment pipelines, and tooling;
- Building the awkward things that application developers don't want or need to care about: infrastructure, container fabrics, monitoring, and metrics;
- Encouraging and enabling DevOps practices across an organization.


## **<div align="center">Continuous integration/Continuous delivery (CI/CD):</div>**
**CI/CD** is a coding philosophy and related set of practices that drive development teams to implement small changes frequently, to identify defects and other software quality issues on smaller code differentials rather than larger ones developed over an extensive period of times. Most modern applications require developing code in different platforms and tools, so dev teams need a mechanism to integrate and validate their changes, by consistent and automated way to build, package, and test applications.


**Continuous integration (CI)** automatically builds, tests, and integrates code changes within a shared repository; then<br>
**Continuous delivery (CD)** automatically delivers code changes to production-ready environments for approval; or<br>
**Continuous deployment (CD)** automatically deploys code changes to customers directly.


**Continuous delivery (CD) stages:**
- Retrieving code from source control and executing a build;
- Configuring infrastructure automated through an infrastructure-as-code approach;
- Copying the code to the target environment;
- Setting environment variables for the target environment;
- Deploying application components (web servers, API services, databases);
- Performing additional actions, such as restarting services or calling services, required for new changes to work;
- Executing tests and rolling back environment changes if tests fail;
- Logging and sending notifications about the delivery status.

~~~
Code > Commit > Build > Unit testing > Deploy to Dev > More testing / Human approval > Deploy to Prod
~~~

**The build stage** is a transform that converts a code repo into an executable bundle known as a build. Using a version of the code at a commit specified by the deployment process, the build stage fetches vendors dependencies and compiles binaries and assets.<br>
**The release stage** takes the build produced by the build stage and combines it with the deploy’s current config. The resulting release contains both the build and the config and is ready for immediate execution in the execution environment.<br>
**The run stage** (also known as “runtime”) runs the app in the execution environment, by launching some set of the app’s processes against a selected release.
~~~
Code > Build
              \
                Release > Runtime
              /
       Config
~~~


**Deployment strategies:**
- **Recreate deployment pattern**: Fully scale down the existing application version before you scale up the new application version.
- **Rolling update deployment pattern**: You update a subset of running application instances instead of simultaneously updating every application instance;
- **Blue/green deployment pattern**: In a blue/green deployment (also known as a red/black deployment), you perform two identical deployments of your application;
- **Canary** - release a new version to a subset of users, then proceed to a full rollout;
- **A/B test pattern**: With A/B testing, you test a hypothesis by using variant implementations. A/B testing is used to make business decisions (not only predictions) based on the results derived from data. When you perform an A/B test, you route a subset of users to new functionality based on routing rules.


## **<div align="center">Microservices</div>**
A **microservices architecture** consists of a collection of small, autonomous services. Each service is self-contained and should implement a single business capability within a bounded context. 


**What are microservices?**
- Microservices are small, independent, and loosely coupled. A single small team of developers can write and maintain a service;
- Each service is a separate codebase, which can be managed by a small development team;
- Services can be deployed independently. A team can update an existing service without rebuilding and redeploying the entire application;
- Services are responsible for persisting their own data or external state. This differs from the traditional model, where a separate data layer handles data persistence;
- Services communicate with each other by using well-defined APIs. Internal implementation details of each service are hidden from other services;
- Supports polyglot programming. For example, services don't need to share the same technology stack, libraries, or frameworks.


**Management/orchestration component** is responsible for placing services on nodes, identifying failures, rebalancing services across nodes, and so forth (Kubernetes).<br>


**The API gateway** is the entry point for clients. Instead of calling services directly, clients call the API gateway, which forwards the call to the appropriate services on the back end.<br>
~~~
Clients > API Gateway (WebApp) > Microserverces > Databases
~~~


**Benefits:**
- **Agility:** Because microservices are deployed independently, it's easier to manage bug fixes and feature releases. You can update a service without redeploying the entire application, and roll back an update if something goes wrong. In many traditional applications, if a bug is found in one part of the application, it can block the entire release process.
- **Small, focused teams:** A microservice should be small enough that a single feature team can build, test, and deploy it. Small team sizes promote greater agility.
- **Small code base:** In a monolithic application, there is a tendency over time for code dependencies to become tangled. Adding a new feature requires touching code in a lot of places. By not sharing code or data stores, a microservices architecture minimizes dependencies, and that makes it easier to add new features.
- **Mix of technologies:** Teams can pick the technology that best fits their service, using a mix of technology stacks as appropriate.
- **Fault isolation:** If an individual microservice becomes unavailable, it won't disrupt the entire application, as long as any upstream microservices are designed to handle faults correctly
- **Scalability:** Services can be scaled independently, letting you scale out subsystems that require more resources, without scaling out the entire application. Using an orchestrator such as Kubernetes you can pack a higher density of services onto a single host, which allows for more efficient utilization of resources.
- **Data isolation:** It is much easier to perform schema updates, because only a single microservice is affected. In a monolithic application, schema updates can become very challenging, because different parts of the application may all touch the same data, making any alterations to the schema risky.