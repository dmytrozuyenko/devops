**CONTENTS**

[[_TOC_]]


# <div align="center"> Methodologies</div>
**The software development lifecycle (SDLC)** is the cost-effective and time-efficient process that development teams use to design and build high-quality software. The goal of SDLC is to minimize project risks through forward planning so that software meets customer expectations during production and beyond. This methodology outlines a series of steps that divide the software development process into tasks you can assign, complete, and measure.


**SDLC phases**:
- **Plan**: cost-benefit analysis, scheduling, resource estimation, and allocation. The development team collects requirements from several stakeholders such as customers, internal and external experts, and managers to create a software requirement specification document. The document sets expectations and defines common goals that aid in project planning. The team estimates costs, creates a schedule, and has a detailed plan to achieve their goals;
- **Design**: software engineers analyze requirements and identify the best solutions to create the software. For example, they may consider integrating pre-existing modules, make technology choices, and identify development tools. They will look at how to best integrate the new software into any existing IT infrastructure the organization may have;
- **Implement**: the development team codes the product. They analyze the requirements to identify smaller coding tasks they can do daily to achieve the final result;
- **Test**: development team combines automation and manual testing to check the software for bugs. Quality analysis includes testing the software for errors and checking if it meets customer requirements. Because many teams immediately test the code they write, the testing phase often runs parallel to the development phase;
- **Deploy**: When teams develop software, they code and test on separate build and production environments ensures that customers can continue to use the software even while it is being changed or upgraded. The deployment phase includes several tasks to move the latest build copy to the production environment, such as packaging, environment configuration, and installation;
- **Maintain**: team fixes bugs, resolves customer issues, and manages software changes. In addition, the team monitors overall system performance, security, and user experience to identify new ways to improve the existing software.


**Application lifecycle management (ALM)** is the creation and maintenance of software applications until they are no longer required. SDLC describes the application development phase in greater detail. It is a part of ALM. ALM includes the entire lifecycle of the application and continues beyond SDLC. ALM can have multiple SDLCs during an application's lifecycle.


## <div align="center"> Agile:</div>
**Agile development**:
- is adaptive rather than predictive.
- is people-oriented rather than process-oriented.
~~~
Individuals and interactions  	OVER	Processes and tools
Working software                OVER	Comprehensive documentation
Customer collaboration          OVER	Contract negotiations
Responding to change            OVER	Following plan
~~~


1. Satisfy Customers Through Early & Continuous Delivery;
2. Welcome Changing Requirements Even Late in the Project;
3. Deliver Value Frequently;
4. Break the Silos of Your Project;
5. Build Projects Around Motivated Individuals;
6. The Most Effective Way of Communication is Face-to-face;
7. Working Software is the Primary Measure of Progress;
8. Maintain a Sustainable Working Pace;
9. Continuous Excellence Enhances Agility;
10. Simplicity is Essential;
11. Self-organizing Teams Generate Most Value;
12. Regularly Reflect and Adjust Your Way of Work to Boost Effectiveness.


{+ Agile advantage: +}<br>
Fail early, fail fast.
~~~
Practice of avoidance of doing pointless work:
=> Take most important, recent and urgent part of just developed product
  => Deliver it the fastest way possible to customer
    => Get fast and frequent feedbacks on the developing product
      => Minimizing work that shouldn't be done at all, as of misunderstanding client's request.
~~~


{- Waterfall disadvantage: -}<br>
Any client's requests will restart the development process cycle.
~~~
Problem examples:
"It's not it, redo everything from the scratch!";
"Cool project, but who is it for?";
"Yes, everything was made by technical specifications, but we can't integrate it in our system".
~~~


## <div align="center"> Scrum:</div>
**Scrum** is a lightweight framework that helps people, teams and organizations generate value through adaptive solutions for complex problems.<br>
**Transparency** is the emergent process and work must be visible to those performing the work as well as those receiving the work.<br>
**Inspection** is the Scrum artifacts and the progress toward agreed goals must be inspected frequently and diligently to detect potentially undesirable variances or problems.<br>
**Adaptation** is if any aspects of a process deviate outside acceptable limits or if the resulting product is unacceptable, the process being applied or the materials being produced must be adjusted.<br>


**The Sprint** is all the work necessary to achieve the Product Goal, including Sprint Planning, Daily Scrums, Sprint Review, and Sprint Retrospective.<br>
During the Sprint:
- No changes are made that would endanger the Sprint Goal;
- Quality does not decrease;
- The Product Backlog is refined as needed; and,
- Scope may be clarified and renegotiated with the Product Owner as more is learned.<br>
Sprints enable predictability by ensuring inspection and adaptation of progress toward a Product Goal at least every calendar month.<br>

**Sprint Planning** initiates the Sprint by laying out the work to be done for the Sprint.
- Why is this Sprint valuable?
- What can be Done this Sprint?
- How will the chosen work get done?<br>


Sprint Planning is timeboxed to a maximum of eight hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.<br>


**Daily Scrum**
The Sprint Backlog is the Sprint Goal, the Product Backlog items selected for the Sprint, plus the plan for delivering them. The purpose of the Daily Scrum is to inspect progress toward the Sprint Goal and adapt the Sprint Backlog as necessary, adjusting the upcoming planned work.<br>
- **The Product Backlog** is an emergent, ordered list of what's needed to improve the product, like new features, changes to existing features, bug fixes, infrastructure changes or other activities that a team may deliver in order to achieve a specific outcome.<br>
- **The Sprint Backlog** is composed of the Sprint Goal (why), the set of Product Backlog items selected for the Sprint (what), as well as an actionable plan for delivering the Increment (how).<br>


**Sprint Review** purpose is to inspect the outcome of the Sprint and determine future adaptations. The Scrum Team presents the results of their work to key stakeholders and progress toward the Product Goal is discussed. The Sprint Review is the second to last event of the Sprint and is timeboxed to a maximum of four hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.<br>


**Sprint Retrospective** purpose is to plan ways to increase quality and effectiveness. The Scrum Team identifies the most helpful changes to improve its effectiveness. The most impactful improvements are addressed as soon as possible. They may even be added to the Sprint Backlog for the next Sprint. The Sprint Retrospective concludes the Sprint. It is timeboxed to a maximum of three hours for a one-month Sprint. For shorter Sprints, the event is usually shorter.<br>


Being sure that you always have something on each iteration. Most valuable product (MVP) doesn't only mean that faster is better, even if product isn't ready. There is always a possibility that deadline with be changed to sooner date than was agreed previously. An incremental and interactive approach means that scrum isn't just a number of miniwaterfalls, there will be no rush at last days before release.<br>


<details>
  <summary><i>References:</i></summary>
  [SCRUM — метод управления проектами (video, RU)](https://www.youtube.com/watch?v=BHhr1aMgKPk)
</details>


## <div align="center"> Kanban:</div>
**Kanban** system in the most straightforward form is three basic columns – “Requested”, “In Progress” and “Done”. When constructed, managed, and functioning correctly, it serves as a real-time information repository, highlighting bottlenecks within the system and anything else that might interrupt smooth working practices.

