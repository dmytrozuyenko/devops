**CONTENTS**

[[_TOC_]]


# <div align="center"> Virtualization</div>
Virtualization is technology that you can use to create virtual representations of servers, storage, networks, and other physical machines.<br>


**Hypervisor types:**
- **Type 1 (bare metal)** hypervisor interacts directly with the underlying machine hardware.
- **Type 2 (hosted)** hypervisor interacts with the underlying host machine hardware through the host machine’s operating system.
- **Containerization** (of application) is a type of virtualization in which all the components of an application are bundled into a single container image and can be run in isolated user space on the same shared operating system. By using Linux Kernel features like namespaces, to isolate resources like process IDs, host-names, user IDs, file names, network namespaces, interprocess communication, and cgroups, to control and monitor these resources.

~~~
             Type 1                             Type 2              
       (machine isolation)                 (host isolation)
+---------------+---------------+  +-------------------------------+
| +-----------+ | +-----------+ |  | +-----------+ | +-----------+ |
| |  Process  | | |  Process  | |  | |  Process  | | |  Process  | |
| +-----------+ | +-----------+ |  | +-----------+ | +-----------+ |
| | Libs/Bins | | | Libs/Bins | |  | | Libs/Bins | | | Libs/Bins | |
| +-----------+ | +-----------+ |  | +-----------+ | +-----------+ |
| |  Guest OS | | |  Guest OS | |  | |  Guest OS | | |  Guest OS | |
| +-----------+ | +-----------+ |  | +-----------+ | +-----------+ |
|    Virtual    |    Virtual    |  |    Virtual    |    Virtual    |
|    machine    |    machine    |  |    machine    |    machine    |
+---------------+---------------+  +---------------+---------------+
|          Hypervisor           |  |          Hypervisor           |
+-------------------------------+  +-------------------------------+
|           Hardware            |  |     Host operating system     |
+-------------------------------+  +-------------------------------+
                                   |           Hardware            |
                                   +-------------------------------+
~~~

~~~
           Containers
       (process isolation)
+---------------+---------------+
| +-----------+ | +-----------+ |
| |  Process  | | |  Process  | |
| +-----------+ | +-----------+ |
| | Libs/Bins | | | Libs/Bins | |
| +-----------+ | +-----------+ |
| | Guest OS  | | | Guest OS  | |
| +-----------+ | +-----------+ |
|   Container   |   Container   |
+---------------+---------------+
|       Container engine        |  
+-------------------------------+  
|     Host operating system     |  
+-------------------------------+  
|           Hardware            |  
+-------------------------------+  
~~~

~~~

                 Containers on virtual machines
                  (containers engine isolation)
+-------------------------------+-------------------------------+
| +---------------------------+ | +---------------------------+ |
| | +-----------+-----------+ | | | +-----------+-----------+ | |
| | |  Process  |  Process  | | | | |  Process  |  Process  | | |
| | +-----------+-----------+ | | | +-----------+-----------+ | |
| | | Libs/Bins | Libs/Bins | | | | | Libs/Bins | Libs/Bins | | |
| | +-----------+-----------+ | | | +-----------+-----------+ | |
| | | Container | Container | | | | | Container | Container | | |
| | +-----------+-----------+ | | | +-----------+-----------+ | |
| |     Container engine      | | |     Container engine      | |
| +---------------------------+ | +---------------------------+ |
| |         Guest OS          | | |         Guest OS          | |
| +---------------------------+ | +---------------------------+ |  
|       Virtual machine         |       Virtual machine         |
+-------------------------------+-------------------------------+
|                          Hypervisor                           |
+---------------------------------------------------------------+
|                     Host operating system                     |
+---------------------------------------------------------------+
|                           Hardware                            |
+---------------------------------------------------------------+
~~~


# <div align="center"> Type 1 hypervisors:</div>
**KVM**<br>

**VMWare iESX**<br>

**Microsoft Hyper-V**<br>
>Legacy..

# <div align="center"> Type 2 hypervisors:</div>
**Oracle VirtualBox**<br>

**VMWare Workstation**<br>

**Gnome Boxes**<br>


# <div align="center"> Container engines:</div>
Containers share the same OS kernel, but have isolated Processes, Networks, and Mounts. <br>


Docker vs Virtual machine: each VM contains OS inside it. Utilization of which has much poorer performance and require a lot more of storage space.
Using VM instead of Docker gives possibility to have on one machine different versions of Linux Kernel and different OS's (like Microsoft Windows)

## <div align="center"> Low-Level Container Runtimes:</div>
**runC** — created by Docker and the OCI. It is now the de-facto standard low-level container runtime. runC is written in Go. It is maintained under moby—Docker’s open source project.<br>


**crun** — an OCI implementation led by Red Hat. Crun is written in C. It is designed to be lightweight and performant, and was among the first runtimes to support cgroups v2.<br>


**containerd** — an open-source daemon supported by Linux and Windows, which facilitates the management of container life cycles through API requests. The containerd API adds a layer of abstraction and enhances container portability.<br>
*ctr - CLI for containerD (unsupported);*
*nerdctl - provides a Docker-like CLI for containerD (supports newest features in containerD). Supports Docker compose;*



**rkt**


**CRI-O** — an open-source implementation of Kubernetes’ container runtime interface (CRI), offering a lightweight alternative to rkt and Docker. It allows you to run pods using OCI-compatible runtimes, providing support primarily for runC and Kata (though you can plug-in any OCI-compatible runtime).<br>


*crictl - provides a CLI for CRI compatible container runtime. Installed separately. Used to inspect and debug container runtimes, not create.*



>In progress..


## <div align="center"> High-Level Container Runtimes:</div>
### **<div align="center">Docker</div>**
**Docker** the leading container system, offering a full suite of features, with free or paid options. It is the default Kubernetes container runtime, providing image specifications, a command-line interface (CLI) and a container image-building service. <br>


**Image** - package or template of instuctions which is used to create containers. Packed image of default or custom container (like ISO).<br>
**Container** - running instance of an image (like VDI).<br>
**Dockerfile** - is a text document that contains all the commands a user could call on the command line to assemble an image (like VBOX file).<br>
**Volume** - shared directory between container and host or other containers (like shared folder).


Unlike VM, conteiners are not meant to host OS. Container only lives as long as a process inside it is alive. Once the task is complete container exits. Container with OS will be stopped immediately.<br>

~~~
/var/lib/docker/..
                  /aufs
                  /containers
                  /image
                  /volumes
~~~


**Architecture:**
- Layer 6. Container layer (Read Write)
<br>Image layers:<br>
- Layer 5. Update Entrypoint (Read only)
- Layer 4. Source code (Read only)
- Layer 3. Change in pip packages (Read only)
- Layer 2. Changes in apt packages (Read only)
- Layer 1. Base OS Layer (Read only)
The same image layers are shared between all containers built from the same image. And can not be modified without rebuilding an image.


#### **<div align="left">Commands:</div>**
https://docs.docker.com/engine/install/ubuntu/


**Install on Ubuntu 22.04:**<br>
~~~
#Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
~~~
Check that eveyting is installed correctly:<br>
`sudo docker run hello-world`<br>


Create and start the container:<br>
`docker create <image> [command]`<br>
Start (existing) the container:<br>
`docker run <image> [command]`<br>
Run (existing) the container:<br>
`docker start <container>`<br>
  `-a, -attach`: attach STDOUT/STDERR;<br>
  `-i, --interactive`: attach STDIN (interactive);<br>
  `-t, --tty`: pseudo-tty;<br>
  `--name <name>`: name image;<br>
  `-p, --publish 5000:5000`: port map;<br>
  `--expose 5432`: expose a port to linked containers;<br>
  `-P, --publish-all`: publish all ports;<br>
  `--link <container>:<alias>`: linking<br>
_Example:_

Execute a command in docker container:<br>
`docker exec <container> <command>`<br>
_Example:_<br>
`docker exec -it website /bin/bash`<br>

Stop the container:<br>
`docker stop <container>`<br>
Kill (SIGKILL) the container:<br>
`docker kill <container>`<br>
Stop and start the container:<br>
`docker restart <container>`<br>
Suspend the container:<br>
`docker pause <container>`<br>
Resume the container:<br>
`docker unpause <container>`<br>
Destroy the container:<br>
`docker rm <container>`<br>
Force remove a container, even if container is running:<br>
`docker rm -f <container>`<br>
Remove all containers:<br>
`docker rm $(docker ps -aq):`<br>
Logs and follow:<br>
`docker logs -f <container>`<br>
Create volume:<br>
`docker volume create <volume>`<br>
Map a directory inside docker container to use docker host’s directory:<br>
`docker run -v <directory_inside_container>:<directory_inside_host> <image>`<br>
Changes in /<path>/index.html would be presented as default nginx page:<br>
`docker run --name <container> -v $(pwd):/usr/share/nginx/html:ro -d -p 8080:80 nginx`<br>
Mount volumes that other container has (to clone or share beetween containers):<br>
`docker run --name <container> --volume-from <container> -d -p 8081:80 nginx`<br>
Limit container CPU usage to 50%:<br>
`docker run --cpus=.5 <container>`<br>
Limit RAM usage to 100 MB:<br>
`docker run --memory=100m <container>`<br>


Docker is for tasks (processes).<br>
_Example:_ `docker run ubuntu`<br>
This container will start and stop immidietly, because there is no processes.<br>
_Example:_ `docker run ubuntu sleep 5`<br>
Will stop after 5 sec, when process “sleep” will be stopped.<br>


Run custom registry:<br>
`docker run -d -p 5000:5000 --name registry registry:2`<br>
Let’s tail the container’s logs:<br>
`docker logs -f registry`<br>
`docker image tag <my_image> localhost:5000/<my_image>`<br>
`docker push localhost:5000/<my_image>`<br>
`docker pull localhost:5000/<my_image>`<br>


**Inspecting the container:**<br>
`docker ps -a`: list all containers<br>
`docker logs [-f] <container>` - show the container output (STDOUT+STDERR).<br>
List the process running inside the containers:<br>
`docker top <container> [ps options]`<br>
Show the differences with the image (modified files):<br>
`docker diff <container>`<br>
Show low-level infos (in json format):<br>
`docker inspect <container>`<br>


**Interacting with container:**<br>
Attach to a running container (STDIN/STDOUT/STDERR):<br>
`docker attach <container>`<br>
Copy files from the container copy files into the container:<br>
`docker cp <container:path> <hostpath><br> docker cp <hostpath> <container:path>`<br>
Export the content of the conainer (tar archive):<br>
`docker export <container>`<br>
Run a command in an existing container (useful for debugging):<br>  
`docker exec <container> <args>`<br>
  `-d, --detach`: run in background;<br>
  `-i, --interactive`: STDIN;<br>
  `-t, --tty: iteractive`.<br>
Wait until the container terminates and return the exit code:<br>
`docker wait <container>`<br>
Commit a new docker image (snapshot of the container):<br>
`docker commit <container> <image>`<br>


**Image management commands:**<br>
List all local images:<br>
`docker images`<br>
Show the image history (list of ancestors):<br>
`docker history <image>`<br>
Show low-level infos (in json format):<br>
`docker inspect <image>`<br>
Create an image (from a container):<br>
`docker tag <image> <tag>`<br>
Create an image (from a container):<br>
`docker commit <container> <image>`<br>
Create an image (from a tarball):<br>
`docker import <url>[-<tag>]`<br>
Delete images:<br>
`docker rmi <image>`<br>


**Image transfer commands:**<br>
Pull an image/repo from a registry:<br>
`docker pull <repo>[:<tag>]`<br>
Push an image/repo from a registry:<br>
`docker push <repo>[:<tag>]`<br>
Search an image on the offial registry:<br>
`docker search <text>`<br>
Login to a registry:<br>
`docker login`<br>
Logout from a registry:<br>
`docker logout`<br>


**Manual transfer commands:**<br>
Export an image/repo as a tarball:<br>
`docker save <repo>[:<tag>]`<br>
Load images from a tarball:<br>
`docker load`<br>
Proposed script to transfer images between two daemons over ssh:<br>
`docker-ssh`

Clean Docker:<br>
```
clean_docker(){
    docker rm $(docker ps --all --format {{.ID}})
    docker rmi "$(docker images --all --quiet)"
    docker system prune --all --force
    docker system prune --all --force --volumes
    docker builder prune --all --force
    docker volume prune --all --force
}
```

**Dockerfile:**<br>
`FROM <image>`<br>
_Example:_<br>
`FROM ruby2.2.2`<br>
Name of the maintainer (metadata):<br>
`MAINTAINER <email>`<br>
Copy `<path>` from the context into the container at location `<dst>`:<br>
`COPY <path_dst>`<br>
Same as COPY, but untar archives and accepts http urls:<br>
`ADD <scr> <dst>`<br>
Environment variables:<br>
`ENV <variables>`<br>
_Example:_<br>
`ENV APP_HOME /myapp`<br>
Run an arbitrary command inside the container:<br>
`RUN <arguments>`<br>
_Example:_<br>
`RUN mkdir $APP_HOME`<br>
Set the default username:<br> 
`USER <name>`<br>
Set the default working directory:<br>
`WORKDIR <path>`<br>
Set the default command:<br>
`CMD <arguments>`<br>
Set an environment value:<br>
`ENV <name_value>`<br>

RUN vs CMD vs ENTRYPOINT
RUN: While image builds, CMD: Can be overwritten. ENTRYPOINT: Sets the main process. CMD and ENTRYPOINT: Both executes when the container runs.

COPY vs ADD: Both do the same thing, but ADD supports URL's and decompression *.tar files.

`.dockerignore`
~~~
node_modules
Dockerfile
.git
*.js
folder/**
~~~


**Network:**<br>
**`BRIDGE`** - 172.17.0.0 (By default)<br>
**`HOST`** - localhost with one array of ports<br>
**`NONE`** - no network availible<br>
`docker run <container_name> --network==host`<br>
**Create custom network:**
`docker network create --driver bridge --subnet 182.18.0.0/16 custom-isolated-network`
Show all networks:<br>
`docker network ls`<br>
Default DNS server IP address: 127.0.0.11.


**Tips:**<br>
**Vertical containers view:**<br>
~~~
export FORMAT="ID\t{{.ID}}\nNAME\t{{.Names}}\nIMAGE\t{{.Image}}\nPORTS\t{{.Ports}}\nCOMMAND\t{{.Command}}\nCREATED\t{{.CreatedAt}}\nSTATUS\t{{.Status}}\n"
~~~
`docker ps --format="$FORMAT"`


<details>
  <summary><i>References:</i></summary>
  [Registry. Official images hub](https://hub.docker.com/)<br>
  Guids:<br>
  [A Full DevOps Course on How to Run Applications in Containers](https://www.youtube.com/watch?v=fqMOX6JJhGo)<br>
</details>





**Windows Containers and Hyper-V Containers** — two lightweight alternatives to Windows Virtual Machines (VMs), available on Windows Server. Windows Containers offer abstraction (similar to Docker) while Hyper-V provides virtualization. Hyper-V containers are easily portable, as they each have their own kernel, so you can run incompatible applications in your host system.<br> 


**Linux container Daemon (LXD)**<br>


# **<div align="center">Container orchestration tools:</div>**
**Docker Swarm**<br>
Create and join to a cluster:
at swarm manager: `docker swarm init`<br>
at worker: `docker swarm join --token *token*`<br>
at swarm manager: `docker service create --replicas=3 --network frontend -p 8080:80 <name_of_image>`


## **<div align="left">Kubernetes</div>**
**Kubernetes** is a portable, extensible, open-source platform for managing containerized workloads and services, that facilitates both declarative configuration and automation. Kubernetes uses different containerization technologies: Docker, CRI-O, containerd.<br>


**Kubernetes architecture:**
- **Kuberentes deployment** is composed of Kuberenetes clusters; 
- **Kubrentes cluster** contains two main components—a control plane and compute machines (also called “nodes”);
- **node** (minion) serves as a unique, Linux-based, environment, deployed as a physical or virtual machine (VM). The node runs applications and workloads using pods;
- **cluster** is a set of nodes grouped together.
- **master** is node with Kubernetes control panel components installed.
- **pod** is a set of Linux namespaces, cgroups, and potentially other facets of isolation - the same things that isolate a container. Can run single or multiple containers that need to work together (usually single);
- **container** code packaged together;
- **control plane** in charge of maintaining the state og a cluster, determining which applications should and which container each the application should use;
- **Compute**.

**Kubernetes Components:**
- **API server** - is the front-end of Kubernetes. The API server is a component of the Kubernetes control plane that exposes the Kubernetes API.
- **etcd** - Consistent and highly-available key value store used as Kubernetes' backing store for all cluster data.
- **Scheduler** - Control plane component that watches for newly created Pods with no assigned node, and selects a node for them to run on.
- **Controller** - Control plane component that runs controller processes.
- **Container runtime** - is the software that is responsible for running containers.
- **kubelet** - an agent that runs on each node in the cluster. It makes sure that containers are running in a Pod.


**Master Node:**<br>
- </>kube-apiserver;
- etcd;
- controller;
- scheduler.

**Worker Nodes:**<br>
- </>kuberlet;
- Container Runtime.


**Kube Control (kubectl):**<br>
`kubectl run <container_name>` - deploy an application on the cluster;<br>
`--image=<container_name>` - name image to use in container<br>
`kubectl cluster-info` - view information about the cluster;<br>
`kubectl get nodes -o wide`- detailed list of all the nodes of the cluster;<br>
`kubectl get podes -o wide`- detailed list of all the podes of the cluster;<br>
`kubectl describe pod <pod_name>`- detailed information of specified pod.<br>
`kubectl apply -f pod.yaml` - create pod from a specified YAML file.<br>
`kubectl delete <pod_name>` - delete pod;<br>
`kubectl edit pod <pod_name>` - edit YAML file of specified pod;<br>


`kubectl run --replicas=1000 <container_name>`<br>
`kubectl scale -replicas=2000 <container_name>`<br>
`kubectl rolling-update <container_name> --image=<image_name>:<tag>`<br>
`kubectl rolling-update <container_name> --rollback`



**YAML:**<br>
LIST vs DICTIONARY vs LIST of DICTIONARY

`kubectl create -f <file_name>` - create pod;<br>
`kubectl get podes -o wide`- detailed list of all the podes of the cluster;<br>
*pod-definition.yml*<br>
~~~
apiVersion: v1
kind: Pod
metadata:
  name: postgres
  labels:
    app: db
    tier: db-tier
    env: production
spec:
  containers:
    - name: postgres-container
      image: postgres
      env:
        - name: POSTGRES_PASSWORD
          value: mysecretpassword
~~~


`kubectl create -f <file_name>` - create replicationcontroller;<br>
`kubectl get replicationcontroller -o wide`- detailed list of all the replicationcontrollers of the cluster;<br>
*Example:*<br>
`kubectl create -f <rc-definition.yml>`

*rc-definition.yml*<br>
```
apiVersion: v1
kind: ReplicationController
metadata:
  name: myapp-rc
  labels:
    app: myapp
    type: front-end
spec:
  - template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
        - name: nginx-container
          image: nginx

replicas: 3
```


`kubectl create -f <file_name>` - create replicaset;<br>
`kubectl get replicaset -o wide` - detailed list of all the replicasets of the cluster;<br>
`kubectl delete replicaset <replicaset_name>`- deletes spicified replicaset, and all underlying PODs;<br>
`kubectl replace -f <file_name>` - update replicaset parameters;<br>
`kubectl scale --replicas=6 -f <file_name>` - update replicaset scale parameters;<br>
`kubectl scale --replicas=6 replicaset <replicaset_name>` - update replicaset scale parameters (YAML file will not be changed);<br>
*Example:*<br>
`kubectl create -f <replicaset-definition.yml>`

*replicaset-definition.yml*<br>
```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myapp-replicaset
  labels:
    app: myapp
    type: front-end
spec:
  - template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
        - name: nginx-container
          image: nginx

replicas: 3
selector: 
  matchLabels:
    type: front-end
```


Deploy - Update - Rolling updates - Rollback - Pause - Resume 

Kubernetes Deployment:


Strategies:
- Recreate (default): destroy everything and create new ones;
- Rolling: destroy and recreate one pod at the time.

kubectl create -f deployment-definition.yml - create a deployment from a file;

kubectl get deployments - get a list of deployments;

kubectl apply -f deployment-definition.yml - apply new parameters from file;
kubectl set image deployment/myapp-deployment \ nginx-container=nginx:1.9.1 - update the deployment without changing definition file.


kubectl rollout status deployment/myapp-deploy - status of rollout;
kubectl rollout history deployment/myapp-deployment - history of rollout.
*To record a history of deployment use kubectl create -f <file_name> --record=true* 



kubectl rollout undo deployment/myapp-deployment


Kubernetes networking:
Node has IP address (192.168.1.x). Each POD has its own internal IP address (10.244.x.x).
All containers

Kebernetes Services:
- NodePort
- ClusterIP
- LoadBalancer

NodePort
service-definition.yml
```
apiVersion: v1
kind: Service
metadata:
  name: myapp-service

spec:
  type: NodePort
  ports:
    - tartgetPort: 80
      port: 80
      nodePort: 30008
  selector:
    app: myapp
    type: front-end
```

kubectl create -f service-definition.yml - create a service from a file;

kubectl get services - get a list of services;


ClusterIP
service-definition.yml
```
apiVersion: v1
kind: Service
metadata:
  name: back-end

spec:
  type: ClusterIP
  ports:
    - targetPort: 80
      port: 80
  selector:
    app: myapp
    type: back-end
```

LoadBalancer
service-definition.yml
```
apiVersion: v1
kind: Service
metadata:
  name: myapp-service

spec:
  type: LoadBalancer
  ports:
    - targetPort: 80
      port: 80
      nodePort: 30008
```

**Container Runtime Interface (CRI)**: allowed any vendor to work as a container runtime for Kubernetes as long as they adhere to the **Open Container Initiative (OCI)**; which contains **imagespec**: define the specifications on how image should be build, and **runtimespec**: defines standards on how any container runtime should be developed. 


# **<div align="center">Google Kubernetes Engine:</div>**
- **Google Kubernetes Engine**;<br>
- **Azure Container Service (ACS)**;<br>
- **Amazon Elastic Container Service (ECS)**;<br>
- **Red Hat OpenShift**;<br>
- **VMware Tanzu Application Service**;<br>
- **SUSE CaaS Platform**;<br>
- **Rancher**.<br>


