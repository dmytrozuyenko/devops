**CONTENTS**

[[_TOC_]]


# **<div align="center">Cloud service models</div>**
**Software as a service (SaaS)** is a software distribution model in which a cloud provider hosts applications and makes them available to end users over the internet. For example, Google Workspace, Dropbox, Cisco WebEx, GitLab.<br>
- Completed product that is run and managed by the service provider.


**Platform as a service (PaaS)** is a cloud computing model where a third-party provider delivers hardware and software tools that are needed for application development. Usually specified for particular programing language or development framework. For example, Amazon Web Services (AWS) Elastic Beanstalk, Google Cloud Platform (GCP) App Engine, Microsoft Azure, Heroku, Apache Stratos, Red Hat OpenShift.<br>
- Removes the need for your organization to manage the underlying infrastructure;
- Focus on the deployment and management for your applications.


**Infrastructure as a service (IaaS)** is a form of cloud computing that provides virtualized computing resources such as storage, server and networking resources. For example, Amazon Web Services (AWS) Elastic Compute Cloud (EC2), Google Compute Engine (GCE), DigitalOcean, Linode, Rackspace, Cisco Metapod, Microsoft Azure.<br>
- Provide building blocks for cloud IT;
- Provides networking, computers, data storage space;
- Highest level of flexibility;
- Easy parallel with traditional on-premises IT.


**The five characteristics of cloud computing:**
- **On-demand self service**: Users can provision resources and use them without human interaction from the service provider;
- **Broad network access**: Resources availible over the network, and can be accessed by diverse client platforms;
- **Multi-tenancy and resource pooling**: 
    - Multiple customers can share the same infrastructure and applications with security and privacy.
    - Multi customers are serviced from the same physical resources;
- **Rapid elasticity and scalability**:
    - Automatically and quickly acquire and dispose resources when needed.
    - Quickly and easily scale based on demand;
- **Measure service**: Usage is measured, users pay correctly for what they have used.


