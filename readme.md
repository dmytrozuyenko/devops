
## <div align="center"> List of all chapters:</div>
* [ReadMe](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/readme.md)<br>
* [Methodologies](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/00-methodologies.md)<br>
* [DevSecOps](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01-devsecops.md)<br>
  * [Automation](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01a-automation.md)<br>
  * [Infrastructure as code](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01b-iac.md)<br>
  * [Virtualization](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01c-virtualization.md)<br>
  * [Monitoring](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01d-monitoring.md)<br>
  * [Tools](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01e-tools.md)<br>
  * [Testing](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/01f-testing.md)<br>
* [Clouds](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/02-clouds.md)<br>
  * [AWS](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/02a-aws.md)<br>
  * [GCP](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/02b-gcp.md)<br>
  * [Azure](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/02c-azure.md)<br> 
* [Linux](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/03-linux.md)<br>
  * [Tools](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/03a-commands.md)<br>
  * [Scripts](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/03b-scripts.md)<br>  
* [Networking](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/04-networking.md)<br>
* [Security](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/05-security.md)<br>
  * [Firewalls](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/05a-firewalls.md)<br>
  * [VPN](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/05b-vpn.md)<br>
  * [Encryption](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/05c-encryption.md)<br>
  * [OSINT](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/05d-osint.md)<br>
* [Windows](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/06-windows.md)<br>
* [Software development](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/07-software_development.md)<br>
  * [Python](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/07a-python.md)<br>
* [Databases](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/08-databases.md)<br>
* [Soft skills](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/09-soft_skills.md)<br>
* [AI](https://gitlab.com/dmytrozuienko/devsecops/-/blob/main/10-ai.md)<br>