**CONTENTS**

[[_TOC_]]


# <div align="center"> Regular Expressions</div>
**Regular expression (RegEx)** is a sequence of characters that specifies a search pattern.<br>
*The plural of "regex" is "regrets".*


## <div align="center"> Metacharacters</div>
**Special characters with specific meanings:**
- `.` : Matches any single character (except newline);
- `/` : 
*Example:*
`/hello/` : Matches the exact word "hello";<br>
- `[]` : Character class, matches any single character within the brackets;
`/gr[ae]y/` : matches 'gray' and grey' words;<br>
*Range [A-z] contains '[', ']', '\', '^', '_', '`';*<br>
- `{}` : Exact number of occurancy of given characters;
*Example:*
`/w{3}\.\w+\.\w{2,3}/g` : matches exactly 'www' at the beggining on the domain, and 2-3 characters at the end
- `?` : Matches zero or one of the preceding character/group;
*Example:*
`/colou?r/` : Matches both "color" and "colour" (the 'u' is optional);
`/caller?{0,2}/` : Matches both "call" and "caller"
- `+` : Matches one or more of the preceding character/group;
*Example:*
`/[0-9]+/` : matches few digit numbers (for example, '42');
`/[0-9]+{3,}/` : matches exactly three or more digit numbers (for example, '777', '3333');
- `*` : Matches zero or more of the preceding character/group;
*Example:*
`/[1-9]0*/` : matches the digit from range [1-9] followed by none or multiple '0's (for example, '1', '300', '900000');
`/[1-9]0*{5,}/` : matches the digit from range [1-9] followed by five '0's (for example, '800000', 2000000);
- `()` : Groups characters together, can be used with quantifiers or for capturing;
- `|` : Alternation, matches either the expression before or after it;
*Example:*
`/cat|dog/` : Matches either "cat" or "dog".
- `\` : Escapes a metacharacter, allowing you to match it literally.
*Example:*
`/` : Matches .
- `^` : Matches the start of a line;
- `$` : Matches the end of a line;

**Shorthand characters class:**
- `\d`, `\D` : ANY ONE digit/non-digit character. Digits are [0-9];
- `\w`, `\W` : ANY ONE word/non-word character. For ASCII, word characters are [a-zA-Z0-9_];
- `\s`, `\S` : ANY ONE space/non-space character. For ASCII, whitespace characters are [ \n\r\t\f]: new line, character returnes, TAB, form-feed.


**Operators:**
- `^` : NOT operator, match any character out the given range;
*Example:*
`/[^02468]/g` : Returns only odd digits;



## <div align="center"> Flags</div>
**Flags** optional special characters that can change the search behavior or a regular expression.<br>
*Flags are added after the closing slash of your regular expression, like `/pattern/flags`.*<br>
- `i`: Case-insensitive matching. No difference between 'A' and 'a';<br>
*Example:*
`/hello/i` : matches "hello", "Hello", "HELLO", etc.
- `g` : Global search. Find all matches rather than stopping after the first match;<br>
*Example:*
`/a/g` : applied to "banana" finds three matches;
- `m`: Multiline mode. Changes the behavior of `^` and `$` so they match the start/end of a line instead of the whole string;<br>
*Example:*
`/^hello$/m` : matches only the line with "hello" in it.
- `s`: Dotall mode. Allows `.` to match any character, including newline;<br>
*Example:*
`/a.c/s` : matches "abc" even if there's a newline between 'b' and 'c'.
- `u`: Unicode mode. Enables correct handling of Unicode characters beyond the Basic Multilingual Plane;<br>
*Example:*
`\u03A0` : matches 'П'. Essential when working with emojis or certain non-English characters;
`/[\u{2200}-\u{22FF}\u{2190}-\u{21FF}]/gu` : matches range of math operator and arrows;
- `y`: Sticky mode. Starts matching from the lastIndex property of the regex and continues on from there.<br>
*Example:*
Useful for tokenization or parsing large texts in chunks.


## <div align="center"> Patterns</div>
**Matching specific words:**<br>



**Matching character classes:**<br>



**Using quantifiers:**<br>
<br>
- `/\d+/` : Matches one or more digits (e.g., "123", "42");<br>
- `/\w*/` : Matches zero or more word characters (can match an empty string).


**Phone number:**<br>
- `/(\d{3})-(\d{3})-(\d{4})/` : Matches US phone numbers with area code (e.g., "555-123-4567");<br>


**Email:**<br>
- `/\w+@\w+\.\w{2,3}` : Match an email address (simplified);<br>
- `/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x41-\x5A\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7E]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x41-\x5A\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7E]))*\` : Match an email address (advanced);<br>


**Credit card number:**<br>
- `/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|6(?:011|5[0-9][0-9])[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/` : Credit card numbers (with Luhn algorithm check - simplified);<br>


**Dates:**<br>
- `\d{4}-\d{2}-\d{2}` : Match a date in the format YYYY-MM-DD;<br>
- `/(?:\d{2}[./-]){2}\d{4}|\d{4}[./-](?:\d{2}[./-]){2}/` : Handles both DD/MM/YYYY and YYYY/MM/DD date formats, allowing for different separators;<br>


**Passwords:**<br>
- `/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/` : Password validation that requires at least one lowercase letter, one uppercase letter, one digit, one special character, and a minimum length of 8;<br>


**IP and MAC addresses:**<br>
- `/(\d{1,3}\.){3}\d{1,3}/` : Matches IPv4 addresses;<br>
- `/(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9` : Matches IPv4 addresses;<br> 
- `/([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})/` : Matches the common formats of MAC addresses (colon or hyphen separated);<br>


**URLs and file paths:**
- **Extraction of specific URL components:**<br>
    - `/^(https?):\/\//` : Protocol;<br>
    - `/^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/\n]+)/im` : Domain;<br>
    - `/\?([^#]+)/` : Query parameters;<br>
    - `/(\w+)=([^&]+)/g` : Captures key-value pairs from URL query parameters;<br>
- `/https?:\/\/\w+\.\w+/` : Matches basic URLs, starting with http or https;<br>
- `/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i` : Matches URLs within text;<br>
- `/<\s*([a-zA-Z][\w-]*)\s*(?:[^>]*?\s*([a-zA-Z][\w-]*)=(["'])(.*?)\3)?\s*\/?>/` : Breaks down an HTML tag, capturing its name and any attributes;<br>
- `/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/iu` : URL Validation (more robust). A more thorough URL validation pattern, handling various protocols, domains, ports, and paths, while also excluding private IP ranges;<br>
- `/^(?:\/[^\/]+)*\/[^\/]+\.[a-zA-Z]+$/` :  File paths with varying levels of depth. Ensures the path starts with a slash, has any number of subdirectories, ends with a filename and extension;<br>
- `/\(((?:[^()]++|(?R))*+)\)/` : Nested structures (e.g., matching balanced parentheses). Uses recursion (?R) to handle nested parentheses within parentheses;<br>


**Text analysis and lookarounds:**<br>
- `/ERROR: \w+/` : Matches lines in a log file starting with "ERROR: " followed by a word;<br>
- `/(\w+)\s\1/` : Finds repeated words (e.g., "the the");<br>
- `/\w+(?=,)/` : Match a word only if it's followed by a comma (without including the comma in the match);<br>
- `/(?<!\s)\d+/` : Match a number only if it's not preceded by whitespace;<br>
- `/^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/` : Semantic Versioning;<br>


**API Endpoint Matching:**<br>
- `/^\/api\/v\d+\/\w+(?:\/\w+)*\/?$/` : Matches API endpoints starting with /api/, followed by a version number (e.g., v1, v2), one or more path segments, and optionally ending with a slash;<br>


**Firewall Rule Parsing:**<br>
- `/^(?:allow|deny)\s+(tcp|udp)\s+any\s+any\s+eq\s+(\d+)$/` : Extracts action (allow/deny), protocol, and destination port from a simplified firewall rule format;<br>


**Firewall Block Message Identification:**<br>
- `/blocked\s+by\s+firewall/i` : Helps filter out firewall block events from potentially large log files;<br>


**Access List Entry Matching:**<br>
- `/permit\s+(?:tcp|udp)\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\/(\d{1,2})\s+any$/` : Identifies permitted traffic based on source IP address/subnet and protocol in an access list;<br>


**Policy Keyword Search:**<br>
- `/\b(?:password|encryption|audit|compliance)\b/i` : Helps locate relevant sections or keywords within policy documents;<br>


**Proxy Log Entry Parsing:**<br>
- `/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s-\s-\s\[(.*?)\]\s"(GET|POST|PUT|DELETE)\s(\S+)\sHTTP\/(\d\.\d)"\s(\d{3})\s(\d+)` : Extracts client IP, timestamp, request method, URL, HTTP version, status code, and bytes transferred from a typical proxy log line;<br>


**Configuration file validation:**<br>
- `/^LogLevel\s*=\s*(DEBUG|INFO|WARN|ERROR)$/m` : Checks if a configuration line sets "LogLevel" to one of the allowed values;<br>


**Network Security Specifics:**<br>
- `/(?:\b(?:SELECT|UPDATE|INSERT|DELETE|DROP|CREATE|ALTER)\b|\bUNION\s+(?:ALL|SELECT)\b)/i` : Detecting potential SQL injection attempts;<br>


**UUIDs in URLs or API Responses:**<br>
- `/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/i` : Matches the standard UUID (Universally Unique Identifier) format;<br>


**Token and Authentication Patterns:**<br>
- `/^Bearer\s+([a-zA-Z0-9\-._~+/]+=*)/` : Extracts the actual token value from the "Authorization: Bearer ..." header
- `/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/` : Basic structural validation of a JWT (JSON Web Tokens), checking for three dot-separated segments
- **API Keys (various formats):**
    - `/^[a-zA-Z0-9]{32}$/` : 32-character alphanumeric key
    - `/^([a-zA-Z0-9]{8}-){3}[a-zA-Z0-9]{8}$/` : UUID-like format
    - `/(?<=api_key=)[a-zA-Z0-9]+/` : Extracts API key from a query parameter
- `/^[a-zA-Z0-9\-._~+/]{32,128}$/` : Validates the typical format of authorization codes (OAuth 2.0);<br>




Special characters




`grep "error" <filename>.txt` : Find all lines in a given text file that contain the word "error";<br>

`sed 's/<old_value>/<new_value>/g' <input_filename>.txt > <output_filename>.txt` : Replace all occurrences of "old_value" with "new_value" in a file;<br>




[Regular Expressions 101](https://regex101.com)<br>