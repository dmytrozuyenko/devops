**CONTENTS**

[[_TOC_]]


# **<div align="center">Object Oriented Programming</div>**
**Object** is a group of related data (variables) in the form of fields (properties) and code (functions) in the form of procedures (methods).<br>


**Class** is like an object constructor, or a "blueprint" for creating objects.


- **Encapsulation** - grouped related functions and variables together, instead of spaghetti-code, to reduce complexity. So this object could be reusable in different parts of a program;
- **Abstraction** - hide the details and the complexity. Isolate the impact of change. Changes in object would not impacts on the outside;
- **Inheritance** - eliminate redundant code, there’s no need to write object that have something in common. Anything can be grouped into class and be used where it needed;
- **Polymorphism** - in each intance of using an object it can have unique way to use it's methods and parameters.   


**Philosophy:**
- Beautiful is better than ugly;
- Explicit is better than implicit;
- Simple is better than complex;
- Complex is better than complicated;
- Readability counts.


## **<div align="center">Web-application architecture (scaling):</div>**
**Three-tier architecture:**
- Presentation layer (Session State);
- Application layer (Application State);
- Data access layer (Resource State).


**Optimization (on each tier):**
- Load balancer;
- Multiple application instances;
- Database structure.


**CAP theorem:**
- **Consistency:** Every read receives the most recent write or an error;
- **Availability:** Every request receives a (non-error) response, without the guarantee that it contains the most recent write;
- **Partition tolerance:** The system continues to operate despite an arbitrary number of messages being dropped (or delayed) by the network between nodes.


## **<div align="center">SOLID</div>**
1. **Single Responsibility Principle:** Each class has a single purpose. All its methods should relate to function.<br>
<ins>Reasoning:</ins> Each responsibility could be a reason to change a class in the future. Fewer responsibilities -> fewer opportunities to introduce bugs during changes.<br>
<ins>Example:</ins> Split formatting & calculating of a report into different classes.
2. **Open / Closed Principle** Classes (or methods) should be open for extension and closed for modification. Once written they should only be touched to fix errors. New functionality should go into new classes that are derived. This is popularly interpreted to advocate inheriting from an abstract base class.<br>
<ins>Reasoning:</ins> Again you lower the odds of breaking existing code.
3. **Liskov Substitution Principle** You should be able to replace an object with any of its derived classes. Your code should never have to check which subtype it’s dealing with.<br>
<ins>Reasoning:</ins> Prevents awkward type checking and weird side-effects.
4. **Interface Segregation Principle:** Define subsets of functionality as interfaces.<br>
<ins>Reasoning:</ins> Small, specific interfaces lead to a more decoupled system than a big general-purpose one.<br>
<ins>Example:</ins> A Persistence Manager implements DBReader & DBWriter.<br>
5. **Dependency Inversion Principle:** High-level modules should not depend on low-level modules. Instead, both should depend on abstractions. Abstractions should not depend on details. Details should depend upon abstractions.<br>
<ins>Reasoning:</ins> High-level modules become more reusable if they are ignorant of low-level module implementation details.<br>
<ins>Examples:</ins> 1) Dependency Injection; 2) Putting high-level modules in different packages than the low-level modules it uses.

<details>
  <summary><i>References:</i></summary>
[SOLID Principles (1-Pager)](https://wall-skills.com/2013/solid-principles-for-maintainable-oo-code/)<br>
[**S**ingle Responsibility Principle (video, RU)](https://www.youtube.com/watch?v=O4uhPCEDzSo)<br>
[**O**pen / Closed Principle (video, RU)](https://www.youtube.com/watch?v=x5OtQiKOG-Q)<br>
[**L**iskov Substitution Principle (video, RU)](https://www.youtube.com/watch?v=NqvwYcjrwdw)<br>
[**I**nterface Segregation Principle (video, RU)](https://www.youtube.com/watch?v=d9RJqf2o_Sw)<br>
[**D**ependency Inversion Principle (video, RU)](https://www.youtube.com/watch?v=Bw6RvCSsETI)<br>
</details>

## **<div align="center">The Twelve-factor app:</div>**
1. **Code base:** One codebase tracked in revision control, many deploys;
2. **Dependencies:** Explicitly declare and isolate dependencies;
3. **Config:** Strict separation of config from code. Store config in the environment;
4. **Backing services:** Treat backing services as attached resources;
5. **Build, release, run:** Strictly separate between build, release, run stages;
6. **Processes:** Execute the app as one or more stateless and share-nothing processes;
7. **Port binding:** Export services via port binding;
8. **Concurrency:** Scale out via the process model;
9. **Disposability:** Maximize robustness with fast startup and graceful shutdown;
10. **Dev/prod parity:** Keep development, staging, and production as similar as possible;
11. **Logs:** App never concerns itself with routing or storage of its output stream;
12. **Admin processes:** Run admin/management tasks alongside the application.

>in progress
Application program interface (API)
Abstracted application program interface that will respond to requests.

Software development kit (SDK)
SDK will make a request and recieve and use data in needed form.

https://youtu.be/kG-fLp9BTRo?si=y5sXhxgnv1aSP7HW


# **<div align="center">Java</div>**
The primary distinction between **Software Development Kit (SDK)** and **Runtime Environment (JRE)** (a subset of the SDK) involves the JRE's lack of the compiler, utility programs, and header files.<br>
The **Java Development Kit (JDK)** is a superset of the JRE, which includes development tools such as the Java compiler, Javadoc, Jar, and debugger.
