**CONTENTS**

[[_TOC_]]


# <div align="center"> General topics:</div>
Semicolon vs Colon, Comma, and Em Dash (; : , —)

# <div align="center"> Gerund (`-ing`):</div>
The gerund looks exactly the same as the present participle but the gerund always has the same function as a noun (although it looks like a verb). The gerund is a noun made from a verb by adding `-ing`. Not a verb, but a name of some process.<br>
_Example:_<br>
**`Cooking`**` at home can help you save a lot of money.`<br>
`The best thing for your health is `**`not smoking`**`.`<br>
`I'm really afraid of swimming in the ocean.`<br>
`I'm not good at volleyball, I keep on dropping the ball.`<br>
`Cheerleading is not something I like doing.`<br>
`He was sorry for shouting at his wife the dat before.`<br>
`Are you interested in going to concert with me?`<br>
`I am tired of getting your package all the time.`<br>
`No, people shouldn't be giving up their dreams.`<br>
`I am good at making people laugh.`<br>
`I can't live without traveling,`<br>


**`-e`+`-ing`:**<br>
`make` - `making`, `write` - `writing`;<br>
**Vowel + cons => double cons + `ing`:**<br>
`knit` - `knitting`, `swim` - `swimming`:<br>
**`-ie` => `-y` + `-ing`:**<br>
`lie` - `lying`, `die` - `dying`;


The gerund can function as:<br>
- the subject of the sentence:<br>
**`Smoking`**` causes lung cancer.`<br>
`*(to eat sweets) before breakfast is a bad habit.`<br>
vs.<br>
**`Eating sweets before breakfast`**` is a bad habit.`<br>
- the complement of the verb `TO BE`:<br>
`The hardest thing about learning French is `**`memorizing the verbs of movement`**`.`<br
`The first thing you need to do to overcome loneliness (to get a hobby).`<br>
vs.<br>
`The first thing you need to do to overcome loneliness `**`is getting a hobby`**`.`<br>

The gerund can be used:
- after prepositions or as part of certain expressions (`there's no point in`, `in spite of`, etc):<br>
`Can your brother count to ten `**`without looking`**` at his fingers?`<br>
`She ruined the party by (to get drunk)`<br>
vs.<br>
`She ruined the party `**`by getting drunk`**`.`<br>
- after phrasal verbs. They are composed of a verb + preposition/adverb.<br>
`I `**`ended up buying`**` a new computer.`<br>
`Rachel `**`gave up drinking`**` sugar drinks.`<br>
`My sister always (to put off/go) to the doctor.`<br>
vs.<br>
`My sister always `**`puts off going`**` to the doctor.`<br>




Infinitive

V1
V2
V3
V-ing

Common personal pronouns in English are: “I,” “you,” “he,” “she,” “it,” “we,” “they,” “me,” “you,” “him,” “her,” “it,” “us” and “them.”
AM IS ARE BE WERE WAS

https://test-english.com/grammar-points/a1/have-got/




# <div align="center"> Tenses:</div>
| Tenses | Descriptions | Examples | 
| ---      | ---     | ---     |
| ***Present Simple***<br> `<SUBJECT>`+`<V1>`(+`-S`/-`ES`)<br> `<SUBJECT>`+`DO`/`DOES NOT` + `<V1>`<br> `DO`/`DOES`+`<SUBJECT>`+`<V1>` | General Statements, Facts, and Habits;<br> Regular actions; Schedules. | _"I eat chocolate."_,<br> _"I work on Mondays."_, _"I don't eat lunch at 2:00."_,<br> _"Mary meets her friends on Friday evenings."_,<br> _"The train to London leaves at 8 pm."_ |
| ***Present Continuous***<br> `<SUBJECT>`+`BE`/`AM`/`IS`/`ARE`+`<V-ing>`<br> `<SUBJECT>`+`BE`/`AM`/`IS`/`ARE`+`NOT`+`<V-ing>`<br> `BE`/`AM`/`IS`/`ARE`+`<SUBJECT>`+`<V-ing>` | Actions that are happening right now;<br> One time actions; Trends. | _"I am eating chocolate."_,<br>  _"I'm working this Saturday."_,<br> _"It is snowing."_, _"We are driving home."_ |
| ***Present Perfect***<br> `<SUBJECT>`+`HAVE`/`HAS`+`<V3>`<br> `<SUBJECT>`+`HAVE`/`HAS`+`NOT`+`<V3>`<br> `HAVE`/`HAS`+`<SUBJECT>`+`V3` | A past event that has present consequences;<br> General life experience (or lack thereof). | _"I have eaten chocolate."_,<br>  _"He has never been in Spain."_,<br> _"William and I have been married for three months."_,<br> _"She has been to Canada three time."_,<br> _"Mike has lost his keys so he is going to be late for work."_ |
| ***Present Perfect Continuous***<br> `<SUBJECT>`+`HAVE`/`HAS`+`BEEN`+`<V-ing>`<br> `<SUBJECT>`+`HAVE`/`HAS`+`NOT BEEN`+`<V-ing>`<br> `HAVE`/`HAS`+`<SUBJECT>`+`BEEN`+`<V-ing>` | An uninterrupted actions that started in the past,<br> and haven't finished in the present. | _"I have been eating chocolate"_,<br> _"I've been studying English for 2 years."_,<br> _"Ellen has been replying to emails for two hours already."_,<br> _"I have been practicing my football skills a lot recently"_,<br> _"They have been trying to call you all day."_|
| ***Past Simple***<br> `<SUBJECT>`+`<V2>`<br> `<SUBJECT>`+`DID NOT`+`<V1>`<br> `DID`+`<SUBJECT>`+`<V1>` | Started and finished in the past actions. | _"I ate chocolate."_,<br>  _"I worked all night"_,<br> _"They didn't come to the party."_,<br> _"My husband bought me flowers last week."_ |
| ***Past Continuous***<br> `<SUBJECT>`+`WAS`/`WERE`+`<V-ing>`<br> `<SUBJECT>`+`WAS`/`WERE`+`NOT`+`<V-ing>`<br> `WAS`/`WERE`+`<SUBJECT>`+`<V-ing>` | Continuing in the past actions. | _"I was eating chocolate."_,<br>  _"We were listening the music at 8 pm."_<br> (Respont on: What you were doing in the past?)<br> _"It was snowing when I went outside."_,<br> _"Will was speaking to me whilst I was recording a video"_,<br> _"We were eating dinner when he arrived."_ |
| ***Past Perfect***<br> `<SUBJECT>`+`HAD`+`<V3>`<br> `<SUBJECT>`+`HAD`+`NOT`+`<V3>`<br> `HAD`+`<SUBJECT>`+`<V3>`| Completed actions at a nonspecific point in the past;<br> Incomplete actions before the past simple happened<br> `<PAST SIMPLE>`+`BEFORE`+`<PAST PERFECT>` <br> | _"I had eaten chocolate."_,<br> _"They hadn't departed yet."_,<br> _"I had taken my lunch break, when I saw the delivery man came."_,<br> _"I had worked there for two years before I got my promotion."_,<br> _"When I arrived at the bus stop the bus had already left"_,<br> _"You had travelled to ten countries before you second birthday."_,<br> _"We had already eaten so we weren't hundry when dinner was served."_,<br> _"They left before I'd spoken to them."_<br> _"Sadly, the author died before he'd finished the series."_ |
| ***Past Perfect Continuous***<br> `<SUBJECT>`+`HAD`+`BEEN`+`<V-ing>`<br> `<SUBJECT>`+`HAD`+`NOT`+`BEEN`+`<V-ing>`<br> `HAD`+`<SUBJECT>`+`BEEN`+`<V-ing>` | Started in the past and continued actions | _"I had been eaten chocolate."_,<br> _"They had been waiting since 3 PM."_,<br> _"They had been walking for hours before they decided they were lost."_,<br> _"The orchestra had been practising for months before the concert happened."_ |
| ***Future Simple***<br> `<SUBJECT>`+`WILL`/`SHALL`+`<V1>`<br> `<SUBJECT>`+`WILL`/`SHALL`+ `NOT`+`<V1>`<br> `WILL`+`<SUBJECT>`+`<V1>` | Events or actions you think will (or won't) happen in the future | _"I will eat chocolate."_,<br> _"I'll have a glass of wine."_,<br> _"He's going to cook dinner."_,<br> _"I'm sure you'll love this book."_,<br> _"I will help you carry those boxes."_ |
| ***Future Continuous***<br> `<SUBJECT>`+`WILL`+`BE`+`<V-ing>`<br> `<SUBJECT>`+`WILL`+`NOT`+`BE`+`<V-ing>`<br> `WILL`+`<SUBJECT>`+`BE`+`<V-ing>` | An actions you think will (or won't) be continuing<br> to happen at a specific time in the future. | _"I will be eating chocolate."_,<br> _"I'm not going to be working on company A."_ |
| ***Future Perfect***<br> `<SUBJECT>`+`WILL`+`HAVE`+`<V3>`<br> `<SUBJECT>`+`WILL`+`NOT`+`HAVE`+`<V3>`<br> `WILL`+`<SUBJECT>`+`HAVE`+`<V3>` | Planned or expected to happen before<br> certain time in the future actions. | _"I will have eaten chocolate."_,<br> _"I will have lived in China for 2 years."_ |
| ***Future Perfect Continuous***<br> `<SUBJECT>`+`WILL`+`HAVE`+`BEEN`+`<V-ing>`<br> `<SUBJECT>`+`WILL`+`NOT`+`HAVE`+`BEEN`+`<V-ing>`<br> `WILL`+`<SUBJECT>`+`HAVE`+`BEEN`+`<V-ing>` | Actions that will continue up to a certain<br> point or actions in the future. | _"I will have been eating chocolate."_,<br> _"I won't have been eating meat for 3 months."_ |

 
## <div align="left">Present Simple</div>
Used for general truths, habits, and permanent situations.<br>
~~~
          |
<-X-X-X-X-X-X-X-X-X-X->
past     now     future
~~~
**_Examples:_**<br>
_"You study English."_<br>
_"She studies English."_<br>


**To talk about facts and statements that are always true:**<br>
_"Most people prefer to shop online."_<br>
_The supermarket is busy on Saturdays mornings."_<br>


**Statements that are true in the present:**<br>
_"You are my best friend, Jasmine!"_<br>
_"I can't bake to save my life - look at this cake!"_<br>


**Routine actions or habits in the present, often used with an adverb of frequency:**<br>
_"Do you always bike to work?"_<br>
_"I never see her in yoga class these days."_<br>


**To refer to scheduled events in the future:**<br>
_"Carmine's holiday officially starts on the 5th of October."_<br>
_"My stag do kicks off at 10 p.m. at Boots and Brews!"_<br>


**To give directions or instuctions:**<br>
_"Mix the flour and water together to create a thick paste."_<br>
_"To get to the bookshop, walk down to the corner and take a left."_<br>


## <div align="left">Present Continuous</div>
Used for actions happening now or around the present moment.<br>
~~~
        ==|==>
<-------X-+---->
past     now
~~~
**_Examples:_**<br>
_"You are studying English right now."_<br>
_""_<br>


**Describe things happening right now:**<br>
_"Debra is whipping up some delicious banana pancakes."_<br>
_""_<br>


**To talk about activities that are not permanent:**<br>
_"I'm giving this workout plan a shot for a month to see how it makes me feel."_<br>


**To discuss plans or events that will happen in the future.**<br>
_"Mark isn't going to that conference next months. He changed his mind."_<br>


**Actions happening around a specific time:**<br>
_"I'm working this morning, but i'll be free this afternoon."_<br>


**To describe things that are gradually changing or improving:**<br>
_"The city is slowly becoming more eco-friendly with new recycling programs and bike lanes."_<br>


## <div align="left"> Present Perfect</div>
Used for past actions with a connection to the present or for experiences.<br>
**Past recent events:**<br>
We don't say when. Or with time expressions including now.<br>
~~~
       <===>|
<--------X--+--->
past       now
~~~
**_Examples:_**<br>
_"You have studied English at some time in the past"_<br>
_"She's had an accident"._<br>
_"I've passed the exam._"<br>
`JUST, ALREADY, YET` - _"Have you finished yet? Yes, I've already finished."_, _"Tony has just called.", _"My dad has just pulled up to the house."_<br>
`RECENTLY` - _"Have you seen any good films recently?"_, _"They‘ve recently bought a new car."_<br>
`TODAY, THIS WEEK, THIS MONTH, etc` - _"I haven't seen Ted today. Maybe he isn't feeling well."_, _"I‘ve seen John today."_ (Today has not finished.), _"Has she called you this week?"_<br>


**Past experiences:** 
We don't say when these events happened.<br>
~~~
            |
<---X--X----+--->
past       now
~~~
**_Examples:_**<br>
_"You have been studying English for three years and you may continue studying English."_<br>
_"We've been to Rome and Florence."_<br>
_"I haven't read that book."_<br>
_"Yeah. we've tried that recipe!"_<br>
_"I’ve passed the exam."_<br>
_"She’s found a job."_<br>
_"He has been to the moon. He’s an astronaut."_<br>
_"I haven’t been to India."_<br>
_"I’ve lost my keys."_<br>
_"They have travelled around Asia and most of Europe."_<br>
_"We‘ve been to a very nice restaurant."_<br>
`NEVER, EVER, BEFORE` - _"Have you ever read it?" "I have never read it."_, _"I‘ve never read this book."_, _"Have you ever seen a John Wayne film?"_, _"I haven’t experienced anything like this before."_<br>
`Superlative + EVER` -_This is the best food I've ever tried."_, _"This is the best meal I’ve ever had."_, _"It’s the most amazing place we’ve ever travelled to."_<br>
`Number of times until now` - _"I've seen this film three times."_, _"I’ve watched this film three times."_, _"We’ve been to Paris twice."_<br>


**Unfinished situations, states or actions:**
Situations that started in the past and have not finished.<br>
~~~
      ===>|
<-----X---+--->
past     now     
~~~
**_Examples:_**<br>
_"How long have you been here?"_<br>
_"I have been here all day"_<br>
`HOW LONG, FOR, SINCE` - _"We have been married for 20 years."_, _"I‘ve had this watch since I was a kid."_, _"How long have you been friends?"_, _"I’ve been here for hours."_, _"She’s been my teacher for three years."_, _"I’ve been unemployed since May."_, _"I’ve lived in this town since I was born."_, _"He's been in his new position for about two weeks."_<br>
`ALL + time expression` - _"I've live in this house all my life"_<br>
`LATELY` - _"We have been very busy lately."_, _"She’s been with me all day."_, _"I’ve been very busy lately."_



Don’t use `AGO` with the present perfect:<br>
_"I’ve had my watch since two years ago."_<br>
_"I’ve had my watch for two years."_


[**Present Perfect vs Past Simple:**](https://test-english.com/grammar-points/b1/past-simple-present-perfect/)
| Present Perfect | Both | Past Simple | 
| ---      | ---     | ---     |
| No exact time |`THIS MORNING` | With the exact time |
| Action that started in the past<br> and continues up to this moment | `FOR SEVERAL YEARS` | Simple completed action and<br> when we ask about or give details  |
| Announce news | `TODAY` | Past state or habit |
| Past action connected with the present | `FOR SOME TIME` | Past actions not connected to the present |
| Describe experience and<br> speak about all life | `THIS WEEK` | About dead people |


| Present Perfect Simple | Past Simple | 
| ---      | ---     |
| _"We‘ve been married for 20 years."_<br> (We are married now) | "_We were married for 20 years. Then we divorced."_<br> (We are not married now) |
| _"I've finished my essay."_<br> (It's done now.)<br> _"He has left."_<br> (He's not here now.) | _"When did you finish your essay?"_<br> _"He left after the game."_<br> _"I didn't go to work yesterday."_ |
| _"She had won two gold medals."_<br> _"I've broken my arm."_<br> _"They have travelled a lot."_ | _"She has won two gold medals."_<br> _"She got the first one in Tokyo and .."_<br> _"I've broken my arm."_ _"How did it happen?"_ "_I fell in the park."_ |
| _"He has lived in Japan for years"_<br> (He still lives in Japan.)<br> _"How long have you been married?"_ <br> (You are still married.) | _"He lived in Japan for years."_<br> (he does not live in Japan now.)<br> _"How long were you both married?"_<br> (You aren't married anymore.) |
| A: _"I‘ve been to the cinema."_<br> (To introduce a past or experience.) | B: _"What did you see?"_<br> A: _"I saw a very good film by…"_<br> (Continue talking and give details.) |
| A: _"Oh, you‘ve broken your arm."_ | B: _"Yes, I have."_<br> A: _"How did it happen?"_ |


## <div align="left"> Present Perfect Continuous</div>
Used for actions that started in the past and are ongoing up to the present.<br>


**Duration from part till now:**<br>
(With action verbs) Situations that have not finished or have just finished.<br>
~~~
       ====|->
<----------+--->
past      now
~~~
**_Examples:_**<br>
_"You have been studying English."_<br>
_"Have you been crying?"_<br>
_"I have been painting the bedroom."_<br>


**Repeated actions from past until now:**<br>
~~~
           |
<--X--X--X-+----
past      now
~~~
**_Examples:_**<br>
_"You have been studying English twice a week."_<br>
_"She has been calling you for days."_ (She has repeated this action several times over the last few days.)<br>
`FOR, SINCE, HOW LONG` - _"We have been studying since you left this morning."_<br>
`ALL + time expression` - _"I've been trying to talk to you all evening."_<br>
`LATELY` - "_She hasn't been sleeping very well lately."_<br>


[**Present Perfect Simple vs Present Perfect Continuous:**](https://test-english.com/grammar-points/b1-b2/present-perfect-simple-continuous/)
| Present Perfect Simple | Present Perfect Continuous | 
| ---      | ---     |
| _"Who has eaten my cookies?"_<br> (There are no cookies left) | _"Who has been eating my cookies?"_<br> (There are some cookies left) |
| _"I’ve watched the series you recommended."_<br> (Watched the last episode yesterday.) | _"I’ve been watching the series you recommended."_<br>  (Still watching it.) |
| _"Look how nice my car looks. I’ve washed it."_<br> (The present results come from having finished the action) | _"Sorry, I’m so sweaty. I’ve been washing my car."_<br> (The present results come from the process of performing<br> the action, which may or may not have finished.) |

**_With stative verbs between Present Perfect Simple and Present Perfect Continuous  always choose Perfect Perfect Simple (`have been V3`)._**<br>
Be, Know, Live, Believe, Cost, Have, Like, Love, Hate, Understand, Want, Teach, Exist, Alledge, Report

Be: "I have been here for two hours." (Correct - although "be" is stative, here it implies a continuous state of being in a location)
Know: "She has known him since childhood." (Correct)
Live: "They have lived in London for five years." (Correct)
Believe: "He has believed in Santa Claus all his life." (Correct)
Cost: "This car has cost a fortune." (Correct)
Have: "We have had this dog for ten years." (Correct - "have" here indicates possession, a state)
Like: "I have liked her since we met." (Correct)
Love: "They have loved each other for a long time." (Correct)
Hate: "She has hated spiders since she was a child." (Correct)
Understand: "I have understood the problem all along." (Correct)
Want: "He has wanted to be a doctor for years." (Correct)
Teach: "She has taught at this school for a decade." (Correct - although teaching is an activity, here the focus is on the completed span of time)
Exist: "Dinosaurs have existed for millions of years." (Correct)
Allege: "The newspaper has alleged that the mayor is corrupt." (Correct)
Report: "The news has reported on the incident." (Correct)

Mental States:

agree: "They have agreed to the terms."
assume: "I have assumed he was coming."
believe: (already mentioned)
consider: "She has considered all the options."
doubt: (already mentioned)
expect: "We have expected this for weeks."
feel (when expressing an opinion): "I have felt this way for a long time."
forget: "I have forgotten his name."
imagine: "Have you ever imagined winning the lottery?"
know: (already mentioned)
mean: "What have you meant by that?"
realize: "I have realized my mistake."
recognize: "Have you recognized her?"
remember: "I have remembered the answer."
suppose: "I have supposed that was the case."
think (when expressing an opinion): (already mentioned)
understand: (already mentioned)
Emotional States:

adore: "She has adored him since they met."
appreciate: "I have appreciated your help."
care: "He has cared for her deeply."
despise: "I have despised him for years."
dislike: "She has disliked him from the start."
envy: "Have you ever envied someone?"
fear: "They have feared the dark since childhood."
hate: (already mentioned)
like: (already mentioned)
love: (already mentioned)
mind: "Have you minded waiting?"
need: (already mentioned)
prefer: (already mentioned)
respect: "I have always respected my elders."
want: (already mentioned)
wish: "I have wished for this day to come."
Possession and Relationships:

belong: (already mentioned)
concern: "This has concerned me for some time."
consist: "The team has consisted of five members."
contain: (already mentioned)
depend: "It has depended on the weather."
deserve: "He has deserved a reward."
have: (already mentioned)
include: "The price has included tax."
involve: "The job has involved a lot of travel."
lack: "They have lacked the necessary funds."
matter: (already mentioned)
measure: (already mentioned)
owe: "I have owed him money for months."
own: (already mentioned)
possess: "She has possessed that painting for years."
weigh: (already mentioned)
Sensory Perceptions:

appear: (already mentioned)
hear: (already mentioned)
see: (already mentioned)
seem: (already mentioned)
smell: (already mentioned)
sound: "The music has sounded beautiful."
taste: (already mentioned)


 
## <div align="left">Past Simple</div>
Used to talk about finished actions in the past. We often say when they happened.<br>
~~~
            |
<-----X-----+--->
past       now
~~~
**_Examples:_**<br>
_"You studied English yesterday."_<br>

_"I went to bed at 10 pm last night."_<br>
_"Dennis are a box of chocolates for breakfast."_<br>
_"My husband bought me flowers last week"_<br>


**Past completed actions, usually it's known when these actions happened:**<br>
_"We went to the zoo last week."_<br>
_"Sorry, what did you say?"_<br>
_"I visited Greece last summer with my girlfriend."_<br>

**Past habits or states:**<br>
_"We often went to the pub after work."_<br>
_"He really liked sport, and was very fit."_<br>
_"We were so tired after our long hike yesterday."_<br>
_"Every morning, Ben watered his plants and fed his dog."_<br>


**Situations that started and finished in the past (duration):**<br>
_"I played football for 20 years."_<br>
_"How long did you live in Brussels?"_<br>
_"I loved her since the day we met."_<br>


**To narrate a series of past events of the story in chronological order:**<br>
_"She opened the door, looked at us and went to her room."_<br>
_"Yolanda woke up, brushed her teeth and headed off to the office."_<br>


## <div align="left">Past Continuous</div>
Used for actions that were in progress at a specific time in the past.<br>

~~~
    ===-->  |
<-----X-----+--->
past       now
~~~
**_Examples:_**<br>
_"You were studying English when the telephone rang."_<br>

**Used to talk about what was happening at a certain time in the past or a certain point in a story:**<br>
~~~
      ===>   |
<------X-----+--->
past        now
~~~
_"Tom was cooking dinner at 7 pm yesterday."_<br>
_"Yesterday at 10 p.m. I was sleeping. When Natasha opened the door, we were talking about her."_<br>
_"What were you doing at 9? I was studying."_<br>
_"When I saw them yesterday, they were arguing."_<br>
_"They were swimming at 7 in the morning."_<br>
_"At midday, they were still working."_<br>
_"They were swimming when I saw them."_<br>
_"When she arrived, they were still working."_<br>


**Used to give background information to set the scene at the beginning of a story:**<br>
~~~
     ====>  |
<----==X=>--+--->
past       now
~~~
_"Will was speaking to me whilst I was recording a video."_<br>
_"It was snowing when I went outside."_<br>
_"It was getting dark, and I was walking to the pub when.."_<br>
_"It was getting dark, and I was walking fast. Suddenly..."_<br>


**Used to say that an ongoing action was interrupted by another actions:**
~~~
    ===>    |
<------X----+--->
past       now
~~~
_"We were eating dinner when he arrived."_<br>
_"They were chatting about their holiday when Ben called them."_<br>
_"He was playing football when he broke his arm."_<br> 
_"When I went to bed, it was raining."_<br>


[**Past Simple vs Past Continuous:**](https://test-english.com/grammar-points/a2/past-continuous-past-simple)<br>
The past continuous describes a longer action or situation, and the past simple describes a shorter action or event.<br>
_"When I met Susan, she was having a drink at a terrace with a friend."_<br>
_"We didn’t go out because it was raining."_<br>


## <div align="left">Past Perfect</div>
Used for actions completed before a specific time in the past.<br>
**Used to talk about events that happened before another event, or up to a certain point in the past:**
~~~
    ====>   |
<-----X--x--+--->
past       now
~~~
**_Examples:_**<br>
_"You had studied English at some point in time before you came to class."_<br>
_"When I arrived at the stop, the bus had already left."_<br>
_"She had worked at the company for five years before it closed."_<br>
_"We had already eaten so we weren't hungry when dinner was served."_<br>
_"When I met her, I had never been in a serious relationship."_<br>
_"He noticed that I had cleaned the car. It was clean and shiny."_<br>


**Duration from earlier in the past (stative verbs):**
_"When she died, they had been married for 48 years."_<br>
`FOR, SINCE, HOW LONG` - _"The day Anne died, they had been married for 48 years."_, _"The day I left, I had been in England for exactly four years."_, _"She told me she had always hated her sister."_<br>
_"We had driven 500 miles, and we needed some rest."_<br> 
_"How many hours had he slept when you woke him up?"_<br>


**Past Perfect vs Past Simple:**
**A past event and then about another event that happened earlier than that past event.**<br>
_"I **showed** him a letter that I **had written**_". (First, I wrote a letter. Second, I showed him the letter.)<br>


**The events are introduced in chronological order.**<br>
_"I **wrote** a letter and a few days later I **showed** it to him."_ (First, I wrote a letter. Second, I showed him the letter.)<br>


**NARRATIVE TENSES:**<br>
- **PAST SIMPLE** to describe the main events of a story in chronological order.<br>
- **PAST CONTINUOUS** to set the scene and to describe actions or situations that were in progress (not finished) at certain point in the story.<br>
- **PAST PERFECT** to describe events that happened earlier in the past.<br>


## <div align="left">Past Perfect Continuous</div>
Used for actions that started in the past, comtinued and were completed before another past event.<br>
~~~
 ========> | 
<-==X=>--x-+--->
past      now
~~~
**_Examples:_**<br>
_"You had been studying English for two years before you came to class."_<br>


**Used to talk about an action in the past that continued up to a certain time and contiuned after it:**<br>
_"They had been walking for hours before they decided they were lost."_<br>


**Used to talk about a repeated action in the past up to a certain point:**<br>
_"The orchestra had been practising for months before the concert happened."_<br>


**Past Perfect and Past Perfect Continuous are often used interchangeably:**<br>
_"I had worked there for five years when the company closed."_<br>
**or**<br>
_"I had been working there for five years when the company closed."_<br>


[Narrative tenses – all past tenses](https://test-english.com/grammar-points/b1-b2/narrative-tenses)
We use the past continuous for actions in progress in the past or longer actions interrupted by shorter actions in past simple.<br>
_"The plane in which the football team were travelling crashed and none of them survived."_<br>

We use the past perfect continuous with dynamic verbs to talk about longer continuous actions that started earlier in the past than the main events of the story.<br>
_"We had been flying for about 20 minutes when the plane was hit by turbulence."_<br>


## <div align="left">Future Simple</div>
Used for actions that will happen in the future.<br>
~~~
         |  
<--------+-X-X-X-X->
past    now    future
~~~
**_Examples:_**<br>
_"You will study English in the future."_<br>
_"You are going to study English in the future."_<br>


**Spontaneous decisions:**
_"I'm prety hundgry; I will order some food."_<br>


**Things we think will happen in the future, predictions:**
_"I think it will rain tomorrow."_<br>
_"I'm sure you'll love this book."_<br>
_"The weather forecas says it will rain tomorrow."_<br>


**Make offers or promises:**<br>
_"I will help you carry those boxes._"<br>
_"Richard will call you tomorrow."_<br>
_"I will get to the offics 20 minutes early to get everything set up."_<br>
_"Mum will be happy to help you with project if you need it."_<br>


**Facts in the future:**<br>
_"It will be our 1st wedding anniversary next year."_<br>
_"Our house will be 300 years old soon."_<br>


**Requests:**<br>
_"Will you pass me the salt?"_<br>


## <div align="left">Future Continuous</div>
Used to talk about actions that will be in progress at a certain time in the future. There actions will start and end in the future but we don't know when.<br>
~~~
          |   ====>
<---------+---X---X->
past     now     future
~~~
**_Examples:_**<br>
_"You will be studing English for the next two years."_<br>
_"You are going to be studying English for the next two years."_<br>

_"I will be eating dinner at 8 pm tomorrow."_ (I don't know when I finish.)<br>
_"I'll be playing tennis tomorrow."_ (Considerable amount of time, 30 mins or up to an hour. Not a second.)



## <div align="left">Future Perfect</div>
Used to talk about actions or states that will be completed before a certain point in the future.<br>
~~~
         |  <===>
<--------+--X---X->
past    now    future
~~~
**_Examples:_**<br>
_"You will have studied English for two years at some time in 2012."_<br>
_"You are going to have studied English for two years at some time in 2012"_<br>


_"I will have retired by the time I'm 65."_<br>
_"You will have finished that popcorn before the film starts!"_<br>
_"Come over at 9 pm. my parents will have left by then."_<br>
_""_<br>


Future continuous and future perfect
https://test-english.com/grammar-points/b1-b2/future-continuous-and-future-perfect/

## <div align="left">Future Perfect Continuous</div>
Used to describe actions that will start in the future, continue before another future event.<br>
~~~
                |
<---------X--X--+----
past           now
~~~
**_Examples:_**<br>
_"You will have been studying English for two years next Monday."_<br>
_"You are going to have been studying English for two years next Monday."_<br>

_"She will have been living in Cardiff for three months in August."_<br>
_"When I retire next month, I will have been working here for 3 years."_<br>

No state verbs.
_"I will have had my cat for five years this Christmas."_<br>
NOT _"I will have been having my cat for five years this Christmas."_<br>



**Ways to express future:**<br>
`BE ABOUT TO`+`<INFINITIVE>`:<br>
Something will happen very soon.<br>
_"The new app for electronic payments **is about to** be launched."_<br>
_"Some apps are about to disappear from the market."_<br>
_"Scientists say they are about to find a vaccine."_<br>
`BE ON THE BRINK/POINT/VERGE OF`+`<V-ing>`<br>
Something will happen very soon.<br>
_"This new technology is on the brink/verge of revolutionising the world."_<br>
_"Our country’s economy is on the brink of collapse. "_<br>
_"This historical museum is on the brink of losing half its masterpieces."_<br>
_"They are on the verge of becoming the team to win the most finals in history."_<br>
_"The two historical enemies are on the point of reaching an agreement. "_<br>
`BE DUE TO`+`<INFINITIVE>`<br>
Future events that are planned or expected:<br>
_"They are due to leave the country next week when their visas expire."_<br>
_"Greece is due to repay around £6 billion to its creditors next semester."_<br>
_"The secretary is due to arrive in Montreal tomorrow morning. "_<br>
`BE TO`+`<INFINITIVE>`<br>
Official arrangements and events planned or expected:<br>
_"The president is to sign the agreement before he leaves office."_<br>
_"Prince William is to visit Paris for the first time since his mother died."_ (It is expected that Prince William will…)<br>
_"Nine care homes for the elderly are to close by the end of March."_<br>
Official instructions and orders:<br>
_"All employees are to attend a health and safety orientation on Friday."_<br>
_"All employees are to attend a health and safety orientation at the end of the week."_<br>
_"You are not to leave this room until I say so."_<br> 
Used in `IF`-clauses to describe the result desired in a conditional:<br>
_"If he is to succeed, he will need to learn many things."_<br>
_"We need to be open to everybody’s opinion if we are to avoid repeating the mistakes of the past."_<br>
_"If he is to succeed, he will need to learn to represent the interests of all Americans."_<br> 
`BE (UN)LIKELY TO/THAT`<br>
Something will (not) probably happen:
_"He is unlikely to win the match"_<br>
_"It's unlikely that he will win the match."_<br>
 _"The government is likely to pass new regulations very soon."_<br>
_"It’s likely that the company will have to pay for the damages."_<br>
_"He is unlikely to win this match."_<br>
_"It’s unlikely that the weather will change over the next few days."_<br>
`BE BOUND TO`+`<INFINITIVE>`<br>
Something is certain or very likely to happen:<br>
_"His new film is bound to win the hearts of every romantic out there."_<br>
_"They are bound to like him. He is such a sweet guy."_<br>
_"His new film is bound to win to win the hearts of every romantic out there"_<br>


[Ways to express future](https://test-english.com/grammar-points/b2/ways-express-future/)

^add
https://test-english.com/grammar-points/b1-b2/likely-unlikely-bound-definitely-probably/?authuser=0




# <div align="left">Conditionals</div>
**Conditionals are sentences with two clauses (conditions) - a main clause and an `IF` clause. Conditionals state that the action in the main clause can only take place if a certian condition in the `IF` clause is fulfilled.**<br>


**Comma:**<br>
As with all conditional types, we use a comma after the if–clause when it goes at the beginning of the sentence, but we don’t use a comma when the if-clause goes at the end.<br>
_"If I hadn’t overslept, I wouldn’t have been late."_<br>
_"I wouldn’t have been late if I hadn’t overslept."_<br>


**Unless:**<br>
We can also use unless in conditional sentences to mean `IF` … (`NOT`).<br>
_"I wouldn’t have arrived on time unless I had taken a taxi."_<br>
=<br>
_"I wouldn’t have arrived on time if I hadn’t taken a taxi."_<br>



0							1 				2
			If present, will/might/may+v1	 	if past simple (continues), would/might
If you have time, can you go and pick up 	If the weather is good, I may go	Were I to have travel, I would

3					Mixed 2-3					Mixed 3-2
If past perfect, would have V3	
If I hadn’t gone to the mountains,	
I wouldn’t have broken my leg	

If I hadn’t dropped off from university, I would probably not have started learning everything like crazy afterwards.
If I hadn’t applied to the worst job in my life, I wouldn’t have met my wife.

Unreal
2. If I had a possibility, I would take a vacation now (PRESENT)
3. If I had not worked hard on my previous projects, I wouldn’t have earned good reputation. (PAST)
Mixed. If Terentieva hadn’t called me, I wouldn’t work for SoftServce now (MIXED 1, PAST+PRESENT)
Mixed. If there wasn’t Covid19, we wouldn’t have started to work from home. (MIXED 2, PAST+)


## <div align="left">Type 0</div>

If/unless `<PRESENT>`, `<PRESENT>`.<br>
_"If you have time, can you go and pick up the order."_<br>
_""_<br>


## <div align="left">Type 1</div>
Present or future POSSIBLE situations.<br>
If/unless `<PRESENT>`, `<FUTURE>`/`<IMPERATIVE>`/`<MODAL VERB>`+`<V1>`.<br>
_"If I find her, I’ll tell her that I love her."_<br>
_"If you study, you will pass the exam."_<br>
_"If he doesn't call, you should tell me immediately."_<br>
_"If your room is tidy, you can leave."_<br>
_"If we win, we will celebrate soon."_<br>
_"If I see Sara, I’ll tell her to call you."_<br>


## <div align="left">Future Time Clauses</div>
To talk about the future:<br>
`WHEN`/`AS SOON AS`/`UNTIL`/`ONCE`/`BEFORE`/`AFTER`/`WHILE`+`<PRESENT>`, `<FUTURE>`/`<IMPERATIVE>`/`<MODAL VERB>`.<br>
_"When I am 69, I will retire."_<br>
_"As soon as you arrive, call me."_<br>
_"Until you all finish, nobody can leave."_<br>
_"Before you come, you should text me."_<br>
_"Once you are there, you might need help."_<br>


[Future Time Clauses](https://test-english.com/grammar-points/b1/first-conditional-future-time-clauses/)



Present simple and present perfect in future time clauses
https://test-english.com/grammar-points/b1-b2/when-i-do-vs-when-i-have-done/

When I’m 70, I’ll retire. 
When I will be 70, I’ll retire. 
We’ll call you once we’ve arrived. 
We’ll call you once we will arrive. 


## <div align="left">Type 2</div>
Present or future UNREAL situations.<br>
If/unless `<PAST>`, `WOULD`/`COULD`/`MIGHT`+`<INFINITIVE>`.<br>
_"If I found her, I’d tell her that I love her."_<br>
_"If I won the lottery, I would buy a yacht."_<br>
_"If I had a better salary, I could travel more."_<br>
_"If I were you, I might wait before making a decision."_<br>
_"If I won the lottery, I’d buy a new house."_<br>


[First and second conditionals](https://test-english.com/grammar-points/b1/first-and-second-conditionals)<br>


## <div align="left">Type 3</div>
PAST hypothetical or UNREAL situations.<br>
If/unless `<PAST PERFECT>`, `WOULD`/`COULD`/`MIGHT`+`HAVE`+`<V3>`.<br>
_"If you have time, can you go and pick up."_<br>
_"If you had come to class, you would have passed the exam."_<br>
_"If he hadn't had his helmet on, he might have died."_<br>
_"He might have died, if he hadn't had his helmet on."_<br>
_"If the jacket had been a bit cheaper, I might have bought it."_<br>

[Third conditional](https://test-english.com/grammar-points/b1/third-conditional-past-unreal-situations/)

First conditional, future time clauses
https://test-english.com/grammar-points/b1/first-conditional-future-time-clauses/3/




## <div align="left">Conditional tenses</div>
| Conditional Simple |  | _"I would eat chocolate."_ |
| Conditional Continuous |  | _"I would be eating chocolate."_ |
| Conditional Perfect |  | _"I would have eaten chocolate."_ |
| Conditional Perfect Continuous |  | _"I would have been eating chocolate."_ |


I wish/if only

_"If only I could be there for you tomorrow."_<br>

^
https://test-english.com/grammar-points/b1-b2/wishes-regrets/?authuser=0


# <div align="left">Articles</div>
**`A`/`AN`:**<br>
First mention:<br>
_"I aw an old woman with a dog."_<br>
_""_<br>
Jobs and descriptions:<br>
_"Paula is a techer."_<br>
_"That's a chimpanzee"_<br>
_"When I was a teenager, I enjoyed sleeping."_<br>
Meaning `ONE`:<br>
_"Can I have an orange?"_<br>
_""_<br>
Frequency rates and speeds (meaning `PER`):<br>
_"I work 7 hours a day"_<br>
_"It's $500 a month."_<br>
_"We were driving at 70 km an hour."_<br>
After `WHAT A` and `SUCH A`:<br>
_"What a great idea!"_<br>
_"It's such a nice place."_<br>
_"This is such a difficult problem!"_<br>
_"Thanks, you are such a good friend."_<br>


`[NOTE]` that you cannot use singular countable nouns alone (without a/the/my/etc.)<br>
_"I don’t have driving license."_<br>
_"I don’t have a driving license."_<br>
_"I have car."_<br>
_"I have a car."_<br>
_"When I was teenager…"_<br> 
_"When I was a teenager…"_<br>


**`THE`:**<br>
Already known or mentioned (meaning `THIS`/`THAT`/`THOSE`):<br>
_"The kids are in the garden."_<br>
_"There's a woman with a dog. The dog's huge."_<br>
_"A man and a woman sat in front of me. The man was British, but I think the woman wasn’t."_<br>
We specify which one:<br>
_"I sat on the chair in the corner."_<br>
_""_<br>
There's only one:<br>
_"Can I see the manager?"_<br>
_"The moon in huge."_<br>
_"I’d like to live in this country, but not in the capital."_<br>
Common places in town:<br>
_"I'm going to the bank."_<br>
_"I'm at the library."_<br>
_"I found Peter at the station."_<br>
Superlatives:<br>
_"This is the best restaurant in town."_<br>
_"I'm at the library."_<br>
Oceans, seas, rivers, canals:<br>
_"The Mediterranean, the Nile, the Suez Canal."_<br>


**`<NO ARTICLE>`:**<br>
To describe something in general (with plural or uncountable nouns):<br>
_"Love and health are more important than money."_<br>
_"Women drive more cautiously than men."_<br>
_"Mushrooms are delicious."_<br>
`[COMPARE]`:<br>
_"I love music."_ (Music in general.)<br>
vs.<br>
_"The party was great. I loved **the** music."_ (Specific music.)<br>
Home, work, school, bed, hospital, army, prison:<br>
_"I go to bed."_<br>
_"You should be at school."_<br>
_"They are going to send him to prison."_ (To be imprisoned.)<br>
`[BUT]`:<br>
use the article if we refer to these places just as places or buildings – when they are not used for their main purpose.<br>
_"I found the keys under the bed."_<br>
_"I’m going to the school to pick up my children."_<br>
_"Yesterday I went to the hospital to visit my grandmother."_<br>
Meals:<br>
_"Dinner is ready."_<br>
_"Let's have breakfast."_<br>
_"I always have supper with my children."_<br>
Days, months, years:<br>
_"Friday is the best day."_<br>
_"I met you in 2020."_<br>
`<NOUN>`+`<NUMBER>`:<br>
_"He's in room 15."_<br>
_"Go to page 105."_<br>
TV meaning programming:<br>
_"I saw it on TV."_<br>
_"I don't watch TV."_<br>
`[BUT]`:<br>
_"Turn off the TV."_<br>
_"I’ve bought a new TV."_<br>
I saw him last week.
`NEXT`/`LAST`+`<TIME EXPRESSION>`:<br>
_"The meeting is next Thursday."_<br>
_"I saw him last week."_<br>
`[BUT]`:<br>
_"Last year we spent three weeks in London. The last week in London was one of the best in my life."_ (It does not mean _"the week before `<NOW>`"_).<br>
Continents, counties, cities, mountains:<br>
_"Asia, Spain, Rome, mount Everest."_<br>
`[BUT]`:<br>
Names of countries that are plural or that include words such as **State**, **Republic**, or **Kingdom** are used with the:<br>
_"the US (the United States)"_<br>
_"the UK (the United Kingdom)"_<br>
_"the Philippines, the Netherlands, the Czech Republic."_<br


[A/AN, THE, <no article>](https://test-english.com/grammar-points/b1/an-the-no-article)


https://www.english-grammar.at/online_exercises/articles/article2.htm

https://www.youtube.com/watch?v=SKZSvtf9D_I






Modals

Past modal verbs of deduction
https://test-english.com/grammar-points/b1-b2/past-modal-verbs/



# <div align="center"> Expressions for speculation</div>
`BOUND` / `SURE` - certain or very likely to happen;<br>
_Examples:_<br>
_"He is bound/sure to win the final."_<br>
_"The final is bound/sure to be intense._"<br>



Made of Made from Made with Made out of
https://www.youtube.com/watch?v=_Ff59hghTzM






# <div align="center"> Vocabulary</div>
## <div align="left"> Indirect questions</div>
- _Could you tell me … ?_
- _Do you know … ?_
- _Can I ask you … ?_
- _Do you mind me asking … ?_
- _Have you any idea … ?_
- _I would like to ask … ?_
- _What do you think … ?_
- _Would you mind telling me … ?_
- _I was wondering … ?_
- _I wonder … ?_
- _I would be interested to know … ?_


## <div align="left"> Questions with intensifiers</div>
- _Is it cold outside? - Yes, it is totally freezing._
- _Is his office big? - Yes, it is insanely enormous._
- _Was your teacher angry? - Yes, he is practically furious._
- _Are you sure? - Yes, I am positive._
- _Are you hungry? - No, I am slightly hungry._
- _Were you surprised? - Yes, I was absolutely amazed._
- _Is the bedroom small? - Yes, it is dreadfully tiny._
- _Was the movie funny? - No, it was a bit dull._


**Experiences and feelings:**<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **I felt scared out of my fits** | extremely frightened, so much so that you felt like you might lose control or faint |
| **Reliefed** | feeling of happiness and relaxation when a difficult situation ends |
| **Made my stomach turn** | disgusted or nauseous |
| **Awkward** | uncomfortable or embarrassing |
| **Impressed** | admire someone or something, or be impressed by someone |
| **Fascinated** | extremely interested |
| **Over the moon** | extremely happy and excited about something |
| **I was shaking like a leaf** | trembling with fear, nervousness, or cold |
| **Frustrated** | annoyed or stuck because you can't achieve what you want |
| **I wished the earth would swallow me up** | embarrassed or ashamed that you wanted to disappear |
| **Exhausted** | absolutely tired, often to the point of feeling drained and unable to function properly |
| **hilarious** | very funny |
| **thrilled** | really happy and excited |


| Type | Intensifier | Adjective | 
| ---      | ---     | ---     |
| **Usual** | _VERY_ <br> _EXTREMELY_<br> _RATHER_<br> _SLIGHTLY_<br> _REASONABLY_<br> _A BIT_<br> _DREADFULLY_ | _tired_<br> _angry_<br> _hot_<br> _cold_<br> _big_<br> _small_<br> _funny_<br> _happy_<br> _bad_<br> _good_ |
| **Both** | _FAIRLY_<br> _REALLY_<br> _PRETTY_<br> _ABSOLUTELY_<br> _COMPLETELY_<br> _ENTIRELY_ <br> _PRACTICALLY_<br>_TOTALLY_ | _exhausted_<br> _furious_<br> _boiling_<br> _freezing_<br> _enormous_<br> _tiny_<br> _hilarious_<br> _thrilled_<br> _terrible_<br> _excellent_ |


**Express an opinion (functional language):**<br>
_"I am really against it.."_ 
_"I am in favor of it.."_
_"The way i see it.."_
_"I suppose so.."_
_"I see what you mean.."_
_"You’ve got a point there, but.."_
_"I agree to some/a certain extent, but.."_
_"I totally disagree.."_
_"I am not so sure.."_
_"I am still not convinced.."_
_"I see/take your point, but.."_
_"It seems to me that.."_
_"I don’t agree with you.."_
_"Fair enough, but.."_
_"Let’s agree to disagree.."_


Giving opinions:
_"I think that.."_
_"I believe.."_
_"I guess.."_
_"In my opinion.."_
_"It’s my opinion that.."_
_"As I see it….."_
_"As far as I am concerned.."_
_"From my point of view.."_
_"It seems to me that.."_
_"I would say that.."_
_"As far as I am able to judge.."_
_"I think it would be fair to say.."_

Strong opinions:
_"I firmly believe that.."_
_"I am absolutely convinced that.."_
_"It is my belief that.."_
_"There’s no doubt in my mind.."_
_"It’s quite clear that.."_
_"I am certain that.."_
_"It’s my considered opinion that.."_
_"I agree up to a point.."_
_"I suppose so.."_
_"I agree to a certain extent.."_
_"I take your point, but.."_
_"I couldn’t agree more!"_
_"I can’t really go along with you there.."_
_"There is no doubt about that.."_
_"That makes sense.."_


## <div align="left">Quizlet FLashcards</div>
[Lesson 1](https://quizlet.com/916476455/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **(to) go beyond**   | to exceed |
| **(to) be at one’s best** | to feel the best |
| **not-judgemental** | accepting people as they are, objective |
| **a people person** | someone who likes being with and works well with other people |
| **(to) keep oneself to oneself** | live quietly, privately |
| **witty** | showing or characterized by quick and inventive verbal humor |
| **down-to-earth** | practical realistic |
| **a good laugh** | someone who is fun to be with |
| **hesitation** | unwillingness to do something, or a delay in doing it, because you are uncertain, worried, or embarrassed about it |
| **cautious** | avoiding unnecessary risks or mistakes |
| **eccentric** | unconventional and slightly strange |
| **genuine** | real |
| **mean** | cruel or not kind |
| **moody** | person who is happy one moment and sad the next |
| **sympathetic** | compassionate |
| **trustworthy** | reliable |
| **outgoing** | sociable; eager to mix socially with others |
| **(to) make up one’s mind** | to decide | 
| **sincere** | honest |
| **odd** | strange or unusual |
| **reserved** | keeping one’s thoughts to oneself, withdrawn |
| **taciturn** | silent; not talkative |
| **prudent** | wise, careful, cautious |
| **hectic** | characterized by intense activity |
| **to take a calculated risk** | to do something risky after thinking carefully about what might happen |
| **to crack a joke** | to tell a joke |
| **It slipped my mind** | I forgot |
| **I‘ve lost the train of thought** | I forgot what I was about to say
| **I can relate to that** | I can understand that |
| **How do I put it?** | How can I say this? |


[Lesson 2](https://quizlet.com/917714568/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **part and parcel** | an essential or integral component |
| **be particular about** | think something is very important |
| **procrastinate on** | to keep delaying something that must be done, ofter because it is unpleasant or boring |
| **engage in thoughts** | actively think about |
| **(to) endeavor** | to try |
| **overly** | excessively |
| **be reluctant to** | to be unwilling to |
| **laid-back** | very relaxed, not seeming to be worried about anything |
| **golden mean** | the ideal moderate position between two extremes |
| **solid knowledge** | good knowledge |
| **faux pas** | a socially awkward or tactless act |
| **(to) elaborate on** | explain in more detail |
| **deliberately** | precisely focusing on particular aspect of something |
| **consent** | permission |
| **an evasive answer** | elusive, ambiguous answer |
| **a leading question** | a leading question |


[Lesson 3](https://quizlet.com/917714568/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **(to) put pressure on** | to make attempts to make someone do something by threatening them or making them believe that they should do it |
| **pivotal** | vitally important, essential |
| **to spark interest** | to create interest |
| **to strive** | to try hard |
| **marginal** | insignificant |
| **to get jitters** | to get feelings to extreme nervousness |
| **get around to doing something** | find the time to do something |
| **get a buzz out of** | to enjoy |
| **scared out of my wits** | frightened very badly, petrified |
| **no longer worried** | relieved |
| **(to) make one’s stomach turn** | to disgust someone |
| **(to) throw up** | to vomit |
| **awkward** | making you feel embarrassed so that you are not sure what to do or say |
| **(to) be over the moon** | to be very happy |
| **(to) shake like a leaf** | to shake a lot because you are nervous or frightened |
| **(to) stare** | look at with fixed eyes |
| **frustrated** | to feel disappointed by being kept from doing something |
| **I wished the earth would swallow me up** | felt embarrassed |
| **thrilled** | very excited |
| **petrified** | paralyzed with fear |
| **firstly** | for one thing |
| **to dig into** | research something thoroughly |
| **outside the comfort zone** | in a situation which is not comfortable for you |
| **anxious** | worried |
| **amused** | pleasurably entertained |


[Lesson 4](https://quizlet.com/475932013/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **to break the news** | to announce something or tell someone about something especially if it is unpleasant and unexpected |
| **to cost an arm and a leg** | to be very expensive |
| **top dollar** | the highest end of a price range; a lot of money |
| **to give smb the creeps** | if a person or place gives you the creeps, they make you feel nervous and a little frightened, especially because they are strange |
| **to be about to do something** | to be going to do something very soon |
| **to go belly up** | to go bankrupt |
| **to save the day** | to do something that solves a serious problem |
| **let’s face it** | accept a difficult reality |
| **a real flop** | real failure |
| **to go back to the drawing board** | start again from the beginning |
| **to give somebody the ax** | if someone gets the ax or is given the ax, they are suddenly dismissed from their job because the company wants to reduce costs. If a plan, project, or service gets the ax, it is stopped in order to reduce costs |
| **to be sharp as a tack** | to be very intelligent |
| **to talk something over** | to discuss |
| **after all** | used to say that something should be remembered or considered, because it helps to explain what you have just said |
| **there’s no point in (smth/doing smth)** | it is not worth doing |
| **to make up one’s mind** | to decide |
| **to put up with** | to tolerate |
| **a dead-end job** | a job with no promotional opportunities |
| **to lose one’s temper** | to suddenly became angry |
| **atleast** | minimum |


[Lesson 5](https://quizlet.com/837516296/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **a setback** | something that interferes with progress |
| **a predicament** | a difficult situation |
| **an obstacle/hurdle** | something that gets in the way and stops action or progress |
| **a controversy** | dispute; debate; quarrel |
| **a major hurdle** | the biggest obstacle |
| **minor difficulties** | not very serious difficulties |
| **to overcome hurdles** | to successfully remove obstacles |
| **to get out of predicament** | to get out of a difficult situation |
| **to tackle the problem** | to deal with a problem |
| **to remedy the situation** | correct or improve a situation |
| **to solve a problem** | to resolve the issue |
| **an acute problem** | a serious problem |
| **a thorny question** | a question etc that is complicated and difficult |
| **a refugee** | a person who has been forced to leave their country in order to escape war, persecution, or natural disaster |
| **famine** | an extreme shortage of food | 
| **disease outbreak** | occurs when a number of cases of a disease are reported in a short period of time |
| **pickpocket** | a thief who steal from the pockets or purses of others in public places |
| **it is not related to** | it has nothing to do |
| **to influence** | to affect |
| **flood** | an overflowing of water in a normally dry area |
| **eruption** | bursting out |
| **earthquake** | the shaking that results from the movement of rock beneath Earth’s surface |
| **drought** | a long period without rain |
| **landslide** | a slide of a large mass of dirt and rock down a mountain or cliff |
| **domestic violence** | act of violence involving family members |
| **debt** | something, typically money, that is owed or due |
| **obesity** | extreme fatness |
| **sustainable** | able to meet the current demand for a resource without depleting the future supply |
| **avalanche of unfortunate events** |  |


[Lesson 6](https://quizlet.com/667832255/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **to make an effort** | to try hard |
| **do one’s best** | try as hard as possible |
| **to do more harm than good** | to be damaging and not helpful |
| **to take stock of..** | spend some time thinking about the situation you are in before you decide what to do next |
| **to take measures** | do something to solve a problem |
| **to take notice of** | to pay attention to |
| **an acute problem** | a serious problem |
| **a thorny question** | a question etc that is complicated and difficult |
| **to present a problem** | to pose a problem |
| **to overblow** | to exaggerate |
| **with hindsight** | looking back (at a situation) |
| **viable solution** | a solution that has capability to be successful |
| **unsolicited advice** | unneeded advice; advice which isn’t asked for |


[Lesson 7](https://quizlet.com/625771235/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **a deterrent (to crime)** | something that discourages or hinders |
| **the authorities** | a group of people who have the right to enforce laws |
| **to keep track of** | to continue to be informed or know about someone or something |
| **to identify** | to recognize |
| **to be aware of** | to know about |
| **surveillance** | close watch over a person, group, or area; supervision |
| **violation (of law, for example)** | a breaking of or failing to keep something like a law or a promise |
| **law-abiding (citizens)** | people who obey and respect the law |
| **to commit (a crime)** | to do something illegal |
| **invasion of privacy** | revealing personal information about an individual without his or her consent |
| **accountable for** | responsible for |
| **to hand over** | pass responsibility to someone else |
| **headquarters** | main office |
| **to spy** | to watch secretly |
|**to confide (in somebody)** | to trust (another) with information or secret |
| **persuade** | to convince |
| **plausible** | believable |
| **martial arts** | sports that involve combat and self-defense |
| **abuse of power** | the misuse of authority for harmful, unethical, or illegal ends |
| **to be aware about** | to know about |
| **to commit (a crime)** | to do something illegal |
| **law-abiding (citizens)** | people who obey and respect the law |
| **groundbreaking** | introducing new ideas or methods |
| **to withdraw cash** | to take money out of your account |
| **to emerge** | To come into view |



[Lesson 8](https://quizlet.com/414216333/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **speck** | a very small spot |
| **to abandon** | to leave behind |
| **to choke (on smth)** | to be unable to breathe properly because something is in your throat or there is not enough air |
| **affluent** | rich, wealthy |
| **appalling** | causing dismay or horror |
| **congested** | overcrowded |
| **cracked** | informal slightly crazy |
| **crumbling** | falling to pieces |
| **derelict** | abandoned |
| **grim** | forbidding or uninviting |
| **posh** | elegant and fashionable |
| **run-down** | in very bad condition |
| **spotless** | very clean |
| **sprawling** | spread out |
| **vibrant** | full of life |
| **well-run** | well managed |
| **unbearably** | extremely |
| **mind you** | used for making something that you have already said less strong or ess general |
| **cars crawl along bumper-to-buper** | car move slowly one after the other |
| **to take smb by surprise** | to surprise smb |
| **to honk the horn** | to make the beeping sound |
| **you are dripping in sweat** | you are very sweaty |
| **it has a real buzz** | there is a lot of activity and excitement |
| **don’t get me wrong** | understand me in the correct way |
| **lasting effect** | effect which last long |


[Lesson 9](https://quizlet.com/927694930/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **Major event** | an important incident that occurs |
| **to be convicted of something** | to be proven or officially announced to be guilty of a crime after a trial in a law court |
| **bribe** | payment made to a person in a position of trust to corrupt his judgement |
| **to struggle** | to fight |
| **to bother** | if you don’t bother to do something, you deliberately do not do it because you do not think it is worth spending any effort on it |
| **straightforward** | direct and clear |
| **mortgage (to take out a mortgage)** | a specific type of loan that is used to buy real estate |
| **tough (it was really tough for her)** | difficult |
| **to be fed up with** | annoyed or bored, and wanting something to change |
| **to retrain as (a counselor)** | to learn or to teach someone the skills that are needed to do a different job |
| **to do through a rough patch** | to experience a lot of problems in a period of your life |
| **call it a day** | to decide to stop working, especially because you have done enough or you are tired |
| **fancy somebody/something** | have a particular liking or desire for |
| **to split up** | to separate |
| **to be on a terrible run** | a good or bad run is a period of time when things go well or badly for you |
| **apparently** | obviously | 
| **to take up** | to start |
| **to take into your family** | to adopt a child |
| **to come across as** | if someone comes across in a particular way, that is the impression you have of them (e.g. He comes across as a nice person) |
| **to commute** | travel some distance between one's home and place of work on a regular basis |
| **upbringing** | the care and training a child gets while growing up |
| **to lose the train of thought** | to forget what one was talking or thinking about |
| **to get side-tracked** | start talking about something less important |
| **decent** | proper |


[Lesson 10](https://quizlet.com/928117655/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **hilarious** | very funny |
| **remarkable (e.g. achievement)** | unusual, extraordinary |
| **moving** | causing strong feelings of sadness or sympathy |
| **poignant** | evoking a keen sense of sadness or regret |
| **intense (e.g. intense heat)** | showing great concentration or determination |
| **to overwhelm** | if someone is overwhelmed by an emotion, they feel it so strongly that they cannot think clearly |
| **current affairs** | events of political or social importance that are happening now |
| **to lose one’s temper** | to suddenly become angry |
| **to take up** | start (a hobby) |
| **to run out** | have none left |
| **to turn up** | to arrive |
| **to set up** | to start a business |
| **to settle down** | to give up the single life and start a family |
| **to give up** | to stop (doing something) |
| **to pick up** | to learn something by watching or listening to other people |
| **to look up to** | to respect |
| **to pass away** | to die |
| **to do on** | to continue |
| **to go by** | to pass |
| **to take on** | to employ |
| **to stand for** | to represent |
| **to drop out** | quit a class, school etc |
| **to bring up** | raise a child |
| **to refuse** | to turn somebody down |
| **to put smb off** | to make someone dislike something or someone, or to discourage someone from doing something |
| **to give in** | submit or yield to another’s wish or opinion |
| **to look the words up** | if you look up information in a book, on a computer etc, you try to find it there |


[Lesson 13](https://quizlet.com/928523927/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **it slipped my mind** | I forgot |
| **knee-jerk reaction** | quick automatic response |
| **assumption** | a thing that is accepted as true or as certain to happen, without proof |
| **to elaborate on something** | to explain in greater detail |
| **to side with sb** | to support someone |
| **to rack one’s brain** | to think very hard |
| **to come at a price** | to have a negative or unpleasant consequence due to some action |
| **to perk up** | to cheer up |
| **golden mean** | the ideal moderate position between two extremes |
| **in a row** | one after another |
| **by any means** | in any way |
| **play it by ear** | to act without preparation |
| **the bottom line** | the most important thing |
| **to touch base** | to make contact with someone |


[Lesson 14](https://quizlet.com/929562136/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **to chill out** | to relax |
| **to recharge** | to get your energy back |
| **to wind smb up** | to deliberately say or to do something that will annoy or worry someone, as a joke > tease |
| **to unwind** | to stop worrying or thinking about problems and start to relax |
| **overwhelming** | very great in amount |
| **to blame** | to say or think a person or thing is responsible for something bad that has happened |
| **contrary to** | in opposition to |
| **appeal** | attraction |
| **to devote** | to dedicate |
| **leisure time** | time free from work or duties |
| **in terms of** | with regard to, with respect to |
| **get carried away** | to become too excited and lose control |
| **to fall apart** | to start having problems that you cannot deal with |
| **to mess up** | to spoil something or to do it badly |
| **to mull over** | think carefully about something over a period of time |
| **put a spanner in the works** | cause problems and prevent something from happening as planned |
| **to zone out** | not pay attention / dissociate yourself from a situation |
| **my mind was wondering** | I was thinking about smth else |
| **mumbo jumbo** | nonsense |
| **to disrupt** | to break up; to cause confusion |


[Lesson 15](https://quizlet.com/929964508/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **to envy** | to have a feeling of jealousy |
| **envious** | jealous |
| **change of air/scenery** | a different place from where one usually is |
| **to take a calculated risk** | to do something risky after thinking carefully about what might happen |
| **to screw up** | to make a mistake |
| **put it down to experience** | something bad has happened, but you decide to learn from it instead of being upset by it |
| **to give up on someone** | stop trying to help or change someone because you’ve stopped believing they will improve / change |
| **to give the benefit of the doubt** | Believe someone without proof |


[Lesson 16](https://quizlet.com/930599013/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **against all odds** | despite the difficulties |
| **call it a day** | stop working |
| **up to one’s neck** | having too much of something; overwhelmed by an excessive amount of something |
| **to follow suit** | to do the same thing as someone else |
| **in a nutshell** | briefly, in a few words |
| **to blow off steam** | to get rid of negative or bad feelings |
| **to tighten one’s belt** | to live on a smaller budget |
| **a slim chance** | a small chance |
| **to put a picture** | to give the latest information |
| **to suffer the consequences** | to be punished for what one has done |


[Lesson 18](https://quizlet.com/ua/931428279/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **to appeal** | to attract |
| **destination** | the place to which something or someone is going |
| **delightful** | very pleasing, wonderful |
| **breathtaking** | beautiful, gorgeous, stunning |
| **travel on a tight budget** | travel with little money |
| **to ensure** | to make sure or certain |
| **to run smoothly** | Work without problems |
| **to round up** | to find and gather together a group of people, animals, or things |
| **superb** | excellent |
| **yacht** | a small ship for pleasure cruises or racing |
| **board** | food or meals |
| **lodging** | a temporary place to stay |
| **a cliff** | steep, high wall of rock, earth, or ice |
| **rolling hills** | (of land) rising and falling in long gentle slopes |
| **barren** | not productive, bare |
| **winding streams** | not straight, curvy |
| **dense woodland** | thick forest |
| **dry** | arid land |
| **lush** | produced or growing in extreme abundance |
| **fertile** | capable of reproducing |
| **dirt track** | dirt road |
| **breathtaking view** | an extremely beautiful view |
| **crystal clear** | very clear |
| **scenery** | landscape |
| **vineyard** | a place where grapevines are planted |
| **take your breath away** | to be extremely beautiful or surprising |
| **a retreat** | a place you can go to that is quiet or safe |


[Lesson 19](https://quizlet.com/493765386/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **to go to great lengths** | to try very hard to achieve something |
| **under the weather** | not feeling well |
| **off the top of one’s head** | to think of something without looking it up or trying hard to figure it out |
| **start from scratch** | start from the beginning |
| **to bend over backwards** | to try very hard to help someone |


[Lesson 20](https://quizlet.com/932400503/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **aware of** | knowing that something exists, or having knowledge of a particular thing |
| **fed up with** | tired of |
| **thankful** | grateful for |
| **intimidated by** | frightened or nervous because you are not confident in situation |
| **leery of** | suspicious |
| **reliant on** | dependent on |
| **suspicious of** | having or showing a cautious distrust |
| **breakthrough** | dramatic important discovery |
| **resistant** | having the ability to fight against |
| **savvy** | well-informed; sharp; experienced |
| **invasion of privacy** | violating a person’s right not to have his or her name, photo, or private affairs exposed or made public without giving consent |
| **a deterrent to a crime** | a way to stop people from doing something illegal |
| **to alter** | change |
| **to resort to** | to do something that you do not want to do because you cannot find any other way of achieving something |
| **Implication** | a possible future effect or result of an action, event, decision etc |
| **beneficial effect** | positive effect |
| **irreversible changes** | changes that can’t be changed back |
| **to surpass** | to exceed or go beyond |
| **cutting-edge** | extremely modern and advanced |
| **to disrupt** | to break up, disturb |
| **to distort** | to twist out of shape |


[Lesson 21](https://quizlet.com/932807276/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **ultimate** | surpassing all others; definitive |
| **to estimate** | to try to judge the value, size, speed, cost etc of something, without calculating it exactly |
| **consumption** | using good and services |
| **to wake up to** | to become aware of sth |
| **to file a lawsuit** | officially make a complaint in a court of law |
| **obese** | very fat |
| **to accuse smb of doing smth** | to say that someone has done something wrong |
| **offender** | a person who commits a crime |
| **to make a debut** | do smth for the first time |
| **to devote smth to smth** | to give one’s time, effort, or attention earnestly |
| **to boom** | to show fast growth |
| **to take over** | to take control of something |
| **carbon footprint** | the total carbon dioxide emissions produced by an individual, group, or location |
| **devastating effect** | destructive effect |
| **fossil fuel** | a natural fuel such as coal or gas, formed in the geological past from the remains of living organisms |
| **subsidies** | a sum of money granted by the government or public body to assist an industry or business so that the price of a commodity or service may remain low competitive |
| **tumor** | mass of rapidly dividing cells that can damage surrounding tissue |
| **you name it** | anything you say or choose |
| **to go bang** | to explode |
| **bodily harm** | physical pain or injury |
| **to adjust** | to adapt |
| **equality** | as a political value, the idea that all people are of equal worth |


[Lesson 22](https://quizlet.com/933464862/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **a blessing in disguise** | something good that isn’t recognized at first |
| **bite off more than you can chew** | to try to do too much or more than you can handle |
| **a change of heart** | to change your opinion or the way you feel about something |
| **a downward spiral** | when a situation is getting worse and is difficult to control because one bad event causes another |
| **a close shave** | a situation in which you come very close to a dangerous situation |
| **call it a day** | stop working |
| **get out of hand** | to get out of control |
| **under the weather** | not feeling well |
| **cut corners** | do something the cheapest or easiest way |
| **see eye to eye** | to agree |
| **a piece of cake** | very easy |
| **miss the boat** | used to say that someone missed his or her chance |


[Lesson 24](https://quizlet.com/654792261/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **prospect** | the possibility that something will happen |
| **cynical** | unwilling to believe that people have good, honest, or sincere reasons for doing something |
| **upbeat** | positive and making you feel that good things will happen OPP downbeat |
| **cautiously** | in a cautious way |
| **to dread** | to feel anxious or worried about something that is going to happen or may happen |
| **to conform** | to behave in the way that most other people in your group or society behave |
| **to settle down** | to start giving all of your attention to a job or activity to |
| **to sort smb/smth out** | to successfully deal with a problem or difficult situation |
| **to turn out** | to happen in a particular way, or to have a particular result, especially one that you did not expect |
| **to have ups and downs** | informal - the mixture of good and bad experiences that happen in any situation or relationship |
| **you are nowhere (to go nowhere)** | to have no success or make no progress |
| **to look forward to** | to be excited and pleased about something that is going to happen |
| **to have mixed feelings** | if you have mixed feelings or emotions about something, you are not sure where you like, agree with, or feel happy about it |
| **to feel like doing smth** | to want to have something or to do something |
| **to look on the bright side** | to see the good points in a situation that seems to be bad |
| **to attend** | to do to an event such as a meeting or a class
that’s the way the cookie crumbles** | that’s life |
| **in no time** | very quickly |
| **cut something short** | to stop doing something earlier than you had planned |
| **to drag one’s heels (feet)** | postpone doing what one should be doing |
| **to make up for** | compensate for |
| **in the nick of time** | at the last possible moment |
| **take your time** | don’t hurry |
| **any time now** | very soon |


[Lesson 25](https://quizlet.com/652524772/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **a toddler** | a child who had only recently learned to walk |
| **cheeky** | result bold |
| **to answer back** | reply rudely |
| **affectionate** | loving |
| **to go off the rails** | start behaving in a way that is not socially acceptable |
| **to lose faculties** | lose a natural ability, such as the ability to see, hear, or think clearly |
| **to establish a career** | to get a good career |
| **commitment** | a promise or pledge to do something |
| **to settle down** | to give up the single life and start a family |
| **bold** | audacious, courageous, dauntless |
| **self-conscious** | excessively aware of your appearance or behavior |
| **not lift a finger** | to not make any effort |
| **to be in one’s prime** | be in the period in your life when you are most active or successful |
| **mature (immature)** | fully developed |
| **maturity** | the state of being fully grown or developed |
| **to slap** | hit quickly with the flat part of the hand |
| **to smack** | to hit someone, especially a child, with your open hand in order to punish them |
| **to throw a fit** | become very or unreasonably angry or upset; to have an outburst of rage, frustration, or ill temper |
| **come of age** | reach the age when legally an adult |
| **mediocrity** | the state or quality of being average; of moderate to low quality |
| **bias** | prejudice in favor of or against one thing, person, or group compared with another, usually in a way considered to be unfair |
| **milestone** | an important event |
| **peer** | a person who is equal to another in social standing or age |


[Lesson 26](https://quizlet.com/653641429/)<br>
| Word or phrase | Desciption |
| ---      | ---      |
| **the bottom line** | the most important thing |
| **calculated risk** | a chance a person takes after carefully considering all possible outcomes |
| **to jump in** | enter a conversation |
| **to digress** | to go off the subject |
| **I have much time on my hands** | I have a lot of time |
| **it’s out of my character** | it’s not for me |
| **well-rounded personality** | a well-rounded person has a range of interests and skills and a variety of experience |
| **misbehave** | to act out badly or in the wrong way |
| **misinterpret** | to understand incorrectly |
to come across as** | make a particular impression |
| **to hit it off** | to quickly become good friends with |
| **to slack off** | be or become lazy; to procrastinate or avoid work or one’s duty |


<details>
  <summary><i>**Videos to discuss:**</i></summary>
    https://www.youtube.com/watch?v=R3f6U5X4sMo<br>
    https://www.youtube.com/watch?v=KqaGdcQh5jA<br>
    https://www.youtube.com/watch?v=4BZuWrdC-9Q<br>
    https://www.youtube.com/watch?v=TQMbvJNRpLE<br>
    https://www.youtube.com/watch?v=R1vskiVDwl4<br>
    https://www.youtube.com/watch?v=6Pm0Mn0-jYU<br>
    https://www.youtube.com/watch?v=rWxVwTVU15Q<br>
    https://www.youtube.com/watch?v=arj7oStGLkU<br>
    https://www.youtube.com/watch?v=9jv4hIsNR1k<br>
    https://www.youtube.com/watch?v=LCHPSo79rB4<br>
    https://www.youtube.com/watch?v=arj7oStGLkU<br>
    https://www.youtube.com/watch?v=MjdpR-TY6QU<br>
    https://www.youtube.com/watch?v=2SORukNAI0o<br>
    https://www.youtube.com/watch?v=o6tRewiT1I8<br>
    https://www.ted.com/talks/julian_treasure_how_to_speak_so_that_people_want_to_listen<br>
</details>


C1-C2 is like one-liners in programming. Really short and effective. But insanely hard to read, because it requires the reader to make an additional effort.



